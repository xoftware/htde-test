#include "../../../htde.h"
using namespace htde;
extern "C" {



	void Assign(node_p self, int mode) {
		
		auto &x = self->V["x"].Assignment;
		
		switch	(x.index()) {
		 case 3: x = (double)0.0; 			break;	//string	D->C.Engine.Prop(N,"x");
		 case 2: x = (double)std::get<2>(x);break;	//float	//double
		 case 1: x = (double)std::get<1>(x);break;	//int	//double
		 case 0: x = (double)0.0; 			break;	//bool		D->C.Engine.Prop(N,"x");
		}
		
		//[!]calc	//update matrix	//update frontend [?] if exists
		//[!]calc	//if mode == 0 (from matrix): do not update matrix, 1 (from frontend): update matrix
	/*	
		switch(mode) {
		 case  1: self->V["matrix"].Assign(self,0); break;	//0 (from x): update from x
		}
	*/	
		
		
		
	};
	
	void Calculate(node_p self, int mode) {	//[!]calc	//use vmethod
	//	Property *P = self->V["x"];
	//	int val = P->Assignment;		//get<1>

	
		cout << "calculate from 3D.x" <<endl;
	};
	
}

/*

	Script::AssignProperty(node_p target, float value);


	//can variant parameter accept types?		//yes
	//can node_p/View have destructor?			//yes
	//can vector store polymorphic Properties?	//yes
	//can polymorphic Properties destruct?		//yes
	//is it worth it?							//no

	
	struct matrix_x : constructor::Property {	
		
		
		void Assign(varprop value, int mode) {
			
			switch (value.index() {
			 case 2: this->Assignment = (float)std::get<2>(value); break;
			}
			
			switch (mode) {
			 case 1: this->N->V["matrix"]->Assign(NULL,0);
			}
			
		};
		
	};
	
	constructor::Property *Constructor() {
		return new matrix_x();
	}
	
	
	
	unordered_map<string, (constructor::Property*)()> Properties;
	
	N->View.Properties["x"] = new constructor::Properties["x"];
	
	for(constructor::Property* p : N->View.Properties) delete p;
	
*/
/*

#include <iostream>
// Example program
#include <iostream>
#include <string>
using std::cout; using std::endl; using std::string;

#include <vector>
#include <unordered_map>
#include <variant>	//c++17
using std::vector; using std::unordered_map; using std::variant;
typedef variant<bool,int,float,string> varprop;

#include <memory>
using std::shared_ptr; using std::weak_ptr; using std::make_shared; using std::static_pointer_cast;

struct node; typedef shared_ptr<node> node_p; typedef weak_ptr<node> node_w;

struct Property {
    node_w N;   //is this a problem? will it remain valid?
    virtual void Assign(varprop value){ cout << "default Assign"; return; }
};
struct View {
 
 unordered_map<string, Property*> Properties;
 
 ~View() {
   //  delete this->Properties["x"];    //segfault - use loop/shared pointer?
   
    for(std::pair<string,Property*> p: this->Properties) {
        cout << "Property " << p.first << " found" <<endl;
         delete p.second;
         cout << "Property " << p.first << " deleted" <<endl;
    }
     cout << "view destroyed" <<endl;  
 }
};
struct node {
    
    string name;
    View V;
    
    ~node() {
        
        cout << "node destroyed" <<endl;
    }
}; 

namespace constructor {
    
    unordered_map<string, Property*(*)()> Properties;
}

struct matrix_x : Property {
    
    void Assign(varprop value) { 
        
        switch(value.index()) {
         case 0: cout << "bool" <<endl; break;
         case 1: cout << "int" <<endl; break;
         case 2: cout << "float" <<endl; break;
         case 3: cout << "string" <<endl; break;
        }
        
        this->N.lock()->V.Properties["y"]->Assign(true);
        cout << "matrix_x Assign" <<endl; return; 
    
    }
};
    Property* Constructor_x() {
        return new matrix_x();
    }
    
struct matrix_y : Property {
    
    void Assign(varprop value) { 
        
        switch(value.index()) {
         case 0: cout << "bool" <<endl; break;
         case 1: cout << "int" <<endl; break;
         case 2: cout << "float" <<endl; break;
         case 3: cout << "string" <<endl; break;
        }
        
        auto self = this->N.lock();
        
        self->name = "test lock";
        cout << self->name <<endl;
        cout << "matrix_y Assign" <<endl; return; 
    }
};
    Property* Constructor_y() {
        return new matrix_y();
    }

node template_node;
vector<node_p> DOM;
node_p new_node() {
	return make_shared<node>(template_node);
};


void construct_node() {
    
    auto ELEMENT = new_node();
         ELEMENT->V.Properties["x"] = constructor::Properties["x"]();
         ELEMENT->V.Properties["x"]->N = ELEMENT;
         ELEMENT->V.Properties["y"] = constructor::Properties["y"]();
         ELEMENT->V.Properties["y"]->N = ELEMENT;
        cout << "node created" <<endl;
        cout << "view created" <<endl;
        cout << "Property x created" <<endl;
        cout << "Property y created" <<endl;
        
    DOM.push_back(ELEMENT);
    cout << "element added to dom" <<endl;
};

void destruct_node() {
  
  DOM.back()->V.Properties["x"]->Assign(false);
  DOM.back()->V.Properties["y"]->Assign(false);
  DOM.pop_back();
    
  cout << "going out of scope" <<endl;
};


int main()
{
   cout << "Hello World" << endl; 
   
   constructor::Properties["x"] = Constructor_x;
   constructor::Properties["y"] = Constructor_y;
   
   cout << "x defined" <<endl;
   cout << "y defined" <<endl;
   
    construct_node();   cout <<"node constructed" <<endl;
    destruct_node();    cout <<"node destructed" <<endl;
        
   
   return 0;
}



















*/
	
	
	
	
	
