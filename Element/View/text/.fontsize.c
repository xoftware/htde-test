#include "../../../htde.h"
using namespace htde;
extern "C" {



	void Assign(node_p self, int mode) {
		
		auto &size = self->V["fontsize"].Assignment;
		
		switch	(size.index()) {
		 case 2: size=(float)std::get<2>(size); 
		 	break;								//float
		 case 1: size=(float)std::get<1>(size); 
		 	break;								//int
		 default:size=(float)12;
		 	break;								//NaN
		}
		
	};
	
	void Calculate(node_p self, int mode) {	//[!]calc	//use vmethod
	
	
		cout << "calculate from text.fontsize" <<endl;
	};
	
}
