#include "../../../htde.h"
using namespace htde;
extern "C" {



	void Assign(node_p self, int mode) {
		
		auto &color = self->V["fontcolor"].Assignment;
		string name; uint32_t hex;
		
		switch	(color.index()) {
		 case 3: name = std::get<3>(color);
			if ( name == "red" )	color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);	else
			if ( name == "orange" )	color = glm::vec4(1.0f, 0.5f, 0.0f, 1.0f);	else
			if ( name == "yellow" )	color = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);	else
			if ( name == "green" )	color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);	else
			if ( name == "blue"	)	color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);	else
			if ( name == "indigo" )	color = glm::vec4(0.5f, 0.0f, 0.5f, 1.0f);	else
			if ( name == "violet" )	color = glm::vec4(1.0f, 0.0f, 0.5f, 1.0f);	else
			if ( name == "black" )	color = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);	else
			if ( name == "grey"	)	color = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);	else
			if ( name == "white" )	color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);	else
									color = glm::vec4(0.5f, 0.5f, 0.5f, 0.5f);
			break;																//string	D->C.Engine.Prop(N,"color");
		 case 2: hex = (uint32_t)std::get<2>(color);
			
		 	color = glm::vec4
		 	(	(float)((hex >> 16) & 0xff)	/ 255.0f	//
		 	,	(float)((hex >> 8) & 0xff)	/ 255.0f	//
		 	,	(float)(hex & 0xff)			/ 255.0f	//
		 	,	(float)((hex >> 24) & 0xff)	/ 255.0f	// alpha: 00-FF (uint32)
		 	);

		 	break;
		  case 1: hex = (uint32_t)std::get<1>(color);

		 	color = glm::vec4
		 	(	(float)((hex >> 16) & 0xff)	/ 255.0f	//
		 	,	(float)((hex >> 8) & 0xff)	/ 255.0f	//
		 	,	(float)(hex & 0xff)			/ 255.0f	//
		 	,	(float)((hex >> 24) & 0xff)	/ 127.0f	//alpha: 00-7F (int32)
		 	);
		 	
		 	break;
		 default: 					color = glm::vec4(0.5f, 0.5f, 0.5f, 0.5f);	
			break;	//b,i,f
		}
	};
	
	void Calculate(node_p self, int mode) {	//[!]calc	//use vmethod

	
		cout << "calculate from 3D.fontcolor" <<endl;
	};
	
}
