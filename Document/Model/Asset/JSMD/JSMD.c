#include "../../../../htde.h"
#include "re2/re2.h"
#include <cstdlib>
using namespace htde;
using namespace htde::constructor;

extern "C" {

	#define JSMD_TARGET_NONE	0	//none
	#define JSMD_TARGET_HEADER	1	//header,footer,break
	#define JSMD_TARGET_BULLET	2	//bullet,number,min,max,radio,check
	#define JSMD_TARGET_LINK	4	//link
	#define JSMD_TARGET_MEDIA	8	//media
	#define JSMD_TARGET_FONT	16	//font family, size, color fg, bg
	#define JSMD_TARGET_BLOCK	32	//code, note, quote
	#define JSMD_TARGET_NODE	64	//element, class, id

	#define JSMD_PARENT_NONE	127	//targets: all
	#define JSMD_PARENT_HEADER	0	//targets: none
	#define JSMD_PARENT_BULLET	30	//targets: bullet + link + media + font
	#define JSMD_PARENT_LINK	0	//targets: none
	#define JSMD_PARENT_MEDIA	0	//targets: none
	#define JSMD_PARENT_FONT	30	//targets: bullet + link + media + font
	#define JSMD_PARENT_BLOCK	0	//targets: none
	#define JSMD_PARENT_NODE	127	//targets: all

/*
	#define	JSMD_MODIFY_NONE	0	//default
	#define	JSMD_MODIFY_HEADER	0	//title
	#define JSMD_MODIFY_FOOTER	1	//title
	#define JSMD_MODIFY_BULLET	0	//bullet
	#define	JSMD_MODIFY_NUMBER	1	//bullet
	#define JSMD_MODIFY_EXPAND	2	//bullet
	#define	JSMD_MODIFY_RADIO	3	//bullet
	#define JSMD_MODIFY_CHECK	4	//bullet
	#define JSMD_MODIFY_CODE	0	//block
	#define JSMD_MODIFY_QUOTE	1	//block
	#define JSMD_MODIFY_NOTE	2	//block
*/

	uint	JSMD_PARENT_TYPE(node_p P) { return P->attribs.i["JSMD_PARENT_TYPE"]; };
	uint	JSMD_TARGET_TYPE(node_p N) { return N->attribs.i["JSMD_TARGET_TYPE"]; };
	node_p	JSMD_ELEMENT(uint type) {
		node_p N = NULL;
		switch(type) {
			case JSMD_TARGET_HEADER:	N=Element("header");	N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_HEADER;	break;	// (|) (&) (\)
			case JSMD_TARGET_BULLET:	N=Element("bullet");	N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_BULLET;	break;	// (*) (+) (-) (0) (o) (O) (x) (X)
			case JSMD_TARGET_FONT:		N=Element("font");		N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_FONT;	break;	// (=) (_) (~) (^)
			case JSMD_TARGET_BLOCK:		N=Element("block");		N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_BLOCK;	break;	// (`) (>) (<)
			case JSMD_TARGET_LINK:		N=Element("link");		N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_LINK;	break;	// (?) (!) .action(url)
			default:					N=Element("textbody");	N->attribs.i["JSMD_PARENT_TYPE"]=JSMD_PARENT_NONE;	break;	// (:) (#) (@)
		}
		N->attribs.i["JSMD_TARGET_TYPE"]=type;
		return N;
	};

	struct
		JSMD{
		JSMD(string code_s) {
			 code = code_s.c_str();
			
			RE2 RE_NEWLINE(R"(^(\n(\t*)\(([*oOxX\-\+\\\|&]|[0-9]+)(:[^\)]+)?\)))"); //bullet,header
			RE2 RE_INLINE(R"(^(\(([\?\!=_~\^><`:#@])(;|:[^\)]+)?\)))"); //link, media, font, block, element
			string TAG, TAB, KEY, VAL; //1=tab 2=key 3=val //1=key 2=val
			
			RE2 RE_LINK(R"(^(\.([a-z]+)\(([^\)]*)\)))");	
			string DOT, ACT, URL; //.action(url)
			
			M =JSMD_ELEMENT(JSMD_TARGET_NONE);	
			P = M;		p = JSMD_PARENT_NONE;	//text/root?
			N = NULL;	n = JSMD_TARGET_NONE;	//text/root?
			
			for(i=0; i<strlen(code); ++i) {												//cout<< code[i] <<" | "<< i <<endl;
				
				if ('\n' == code[i]) {													//cout<<"newline | "<<newlines <<endl;
					if(++newlines >= 2) { j=i;
						
						if((JSMD_TARGET_HEADER == n)
						|| (JSMD_TARGET_BULLET == n)) {
							Exit(n,false);	
						}
						newlines = 0;
					}
					
					//1 = tab; 2 = key; 3 = val;
					if (RE2::PartialMatch(code + i, RE_NEWLINE, &TAG, &TAB, &KEY, &VAL)) {
						j=i; i+= TAG.length()-1;
						
						if("|" == KEY) {
							if(!Init(JSMD_TARGET_HEADER,-1))
								continue;
						}else
						if("&" == KEY) {
							if(!Init(JSMD_TARGET_HEADER,-1))
								continue;
						}else
						if("\\"== KEY) {
						}else
						if("*" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("+" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("-" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("o" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("O" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("x" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else
						if("X" == KEY) {
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}else{	//number
							if(!Init(JSMD_TARGET_BULLET,TAB.length()))
								continue;
						}
					}
				}else 
				if((' ' == code[i])
				||('\t' == code[i])) {	
					newlines = newlines;
				}else
				if('(' == code[i]) {
					newlines = 0;

					//1 =  key; 2 = val;
					if (RE2::PartialMatch(code + i, RE_INLINE, &TAG, &KEY, &VAL)) {
						j=i; i+= TAG.length()-1;
						
						if("_" == KEY) {
						
							if(";" == VAL) {	
								if(!Exit(JSMD_TARGET_FONT,true))continue;
							}else {
								if(!Init(JSMD_TARGET_FONT,-1))continue;
								
								for(auto s: Utils::split(VAL.substr(1),' ')) {
									if((s[0]=='0')&&(s[1]=='x')) {
										N->V["fontcolor"].Assignment=stof(s);//(float)strtoul(s.c_str(),nullptr,16);
										N->V["fontcolor"].Inline=true; 
									//	N->V["fontcolor"].Assign(N,1);	//[prop]
									}else if(isdigit(s[0])){
										N->V["fontsize"].Assignment=stof(s);
										N->V["fontsize"].Inline=true; 
									//	N->V["fontsize"].Assign(N,1);	//[prop]
									}else{
										N->V["font"].Assignment=s;
										N->V["font"].Inline=true; 
									//	N->V["font"].Assign(N,1);		//[prop]
									}
								}
							}
						
						}else
						if("?" == KEY) {
						
							if(!(p & JSMD_TARGET_LINK))continue;
								
							TextElement();
							N = JSMD_ELEMENT(JSMD_TARGET_LINK);
							N->M.parent = P;
							P->M.children.push_back(N);
							
							N->attribs.s["value"] = VAL;
							while(RE2::PartialMatch(code + i+1, RE_LINK, &DOT, &ACT, &URL)) {
								i+=DOT.length(); textopen =i;
							//	N->actions.push_back(ACT);
								N->attribs.s[ACT] = URL;
							}
						
						}else
						if("`" == KEY) {
							
							if(";" == VAL) {
								if(!Exit(JSMD_TARGET_BLOCK,true))continue;
							}else {
								if(!Init(JSMD_TARGET_BLOCK,-1))continue;
								N->attribs.s["language"] = VAL;
							}
							
						}else
						if(":" == KEY) {	if(!(p & JSMD_TARGET_NODE))continue;
						}else{
						}
					}
				}else {
					newlines = 0;
				}

			}
			//final text
			j=strlen(code)-1;
			TextElement();
		};
		
		void TextElement() { 
			uint k = textopen+1;
			textopen = i;
			
			if(j <= k)	//no text
				return;	
				
			auto v = string(code + k, code + j);
			if(n != JSMD_TARGET_BLOCK) {
				RE2::GlobalReplace(&v, "\\s+", " ");
				if(v==" ")	//no text
					return;
			}
			
			node_p N = Element("textnode");
			N->attribs.s["value"] = v;
			N->M.parent = P;
			P->M.children.push_back(N);
		};
		void TabDepth(uint newdepth) {

			if (newdepth < tabstart) {				//minimum
				newdepth = tabstart;
			}	//1		= 	4		-	3
				newdepth = newdepth+1 - tabstart;	//normalize
				//1		=	1		-	0
			int taboffset = newdepth - tabdepth;	//relative

			if (taboffset > 0) {
				newdepth = tabdepth+1;				//maximum
				//create element as child of parent
			}else {
				//create element as child of parent->parent->parent...
				while(++taboffset <= 1)
					P = P->M.parent;
			}
			N->attribs.i["depth"] = tabdepth = newdepth;
			N->M.parent = P;
			P->M.children.push_back(N);
		};
		
		bool Init(uint type, int d) {
			if(!(p & type))
				return false;
				
			TextElement();
			N = JSMD_ELEMENT(type);
			
			if(d < 0) {
				N->M.parent = P;
				P->M.children.push_back(N);
			}else {
				if (type != n) {
					tabstart=d;
				}	TabDepth(d);
			}
			P = N;				p = JSMD_PARENT_TYPE(P);
								n = JSMD_TARGET_TYPE(P);
			return true;
		};
		bool Exit(uint type, bool Inline) {
			if(Inline) {
				if(n != type)
					return false;
					
				TextElement();
				P=P->M.parent;	p = JSMD_PARENT_TYPE(P);
								n = JSMD_TARGET_TYPE(P);
				return true;
			} else {
				TextElement();	tabdepth=0; tabstart=0;
				P = M;			p = JSMD_PARENT_TYPE(P);
								n = JSMD_TARGET_TYPE(P);				
				return true;
			}
		};
		
		const char* code;
		uint newlines=0; uint tabdepth=0; uint tabstart=0; uint textopen=0; uint i=0; uint j=0;
		node_p P;	uint p;
		node_p N;	uint n;	
		node_p M;
	};
	
	
	node_p Parse(string code) {
	
		return JSMD(code).M;
	};
}



/*
	
	[ ] - Style Properties/Shader
	[ ] - Parser/Layout
	
	[ ] - Font Converter/Loader
	[ ] - Symbols/Regular Fonts
	[ ] - Containers/Media
	
	[ ] - Key code/Mouse selection
	[ ] - Front-end API/Pack Flag
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
*/
