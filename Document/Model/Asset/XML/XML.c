#include "../../../../htde.h"
#include "pugixml.hpp"
using namespace htde;
using namespace htde::constructor;

extern "C" {
	
	node_p htde_parse_xml( pugi::xml_node target) {
	
		if(strlen(target.name())<=0) {
			node_p N = M::Parsers["JSMD"](target.value()); 
		//	N->attribs.s["value"] = target.value();	//[!] node_proto //attribs
			N->attribs.i["txtid"] = Thread.D->V.Window->Text_Init(N);
			return N;
		}
	
		//element ~= xml_node
		node_p N = Element(target.name());
		
		//for each attribute
		for (pugi::xml_attribute attr : target.attributes()) {
			//test for known types
			//test for int types?
			N->attribs.s[attr.name()] = attr.value();	//[!] node_proto //attribs
		}
		
		Utils::split(N->attribs.s["class"],' ',std::back_inserter(N->classes));	//[!] node_proto //attribs
		N->attribs.s["class"] = "";	//[!] node_proto //attribs	//[?]need to properly delete this
		
		//for each child
		for (pugi::xml_node child : target.children()) {
			
			node_p C = htde_parse_xml(child);
			
			C->M.parent = N;
			N->M.children.push_back( C );
		}
		
		return N;
	};
	
	node_p Parse( string code ) {
		
		node_p model = Element("model");
		
		pugi::xml_document asset;
		if(asset.load_string(code.c_str())) {
			//for each child
			for (pugi::xml_node child : asset.children()) {
				
				node_p C = htde_parse_xml(child);
		
				C->M.parent = model;
				model->M.children.push_back( C );
			}
		}
		
		return model;
	};
}