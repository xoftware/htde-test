#include "../../../../htde.h"
using namespace htde; using namespace htde::constructor;
#include "cmark.h"
//#include "node.h"
extern "C" {
	
	node_p Parse(string code)
	{	auto code_c = code.c_str();
		cmark_node *asset = cmark_parse_document(code_c, strlen(code_c), CMARK_OPT_DEFAULT);
		cmark_node *target;
		
		node_p M = NULL;	//model
		node_p P = NULL;	//parent
		
		cmark_iter *iter = cmark_iter_new(asset);
		cmark_event_type ev_type;
		
		while((ev_type = cmark_iter_next(iter)) != CMARK_EVENT_DONE) {
			target = cmark_iter_get_node(iter); bool literal = false;
			 
			if (ev_type == CMARK_EVENT_ENTER) {
				node_p N = NULL;	//node
				auto type = cmark_node_get_type_string(target);
				//------------------------------------------
				if(CMARK_NODE_DOCUMENT == target->type) {
					cout << "document ";
						
					P = N = M = Element("model");
					continue;
					//[ ] - create model node("model")
					//[ ] - target node = model node
					//[ ] - parent node = target node
					
				}else
				//------------------------------------------
				if((CMARK_NODE_TEXT 		== target->type)
				|| (CMARK_NODE_CODE 		== target->type)
				|| (CMARK_NODE_HTML_BLOCK 	== target->type)
				|| (CMARK_NODE_HTML_INLINE 	== target->type)) {
					cout << "<text />";
						
					N = Element("text");
					//[ ] - create target node(type)
					N->attribs.s["value"] = string((char*)target->as.literal.data, (int)target->as.literal.len);	//[!] node_proto //attribs
					//[ ] - set "value" attribute
					literal = true;
					//[ ] - parent node = same
					
				}else
				//-----------------------------------------
				if(CMARK_NODE_LIST == target->type) {
					cout << "list ";
					
					N = Element("list");
					//[ ] - create a target node(type)
					
					auto list_type =  cmark_node_get_list_type(target);
					if ( list_type == CMARK_ORDERED_LIST) {
						
						N->attribs.s["ordered"] = "true";	//[!] node_proto //attribs
						//[ ] - set "type" attribute to "ordered"
						cmark_delim_type delim = cmark_node_get_list_delim(target);
						if(CMARK_PAREN_DELIM == delim) {
							N->attribs.s["delim"] = "paren";	//[!] node_proto //attribs		
							//[ ] - set "delim" attribute to "paren"
						}else 
						if (CMARK_PERIOD_DELIM == delim) {
							N->attribs.s["delim"] = "period";	//[!] node_proto //attribs
							//[ ] - set "delim" attribute to "period"
						}
						
					}else
					if ( list_type == CMARK_BULLET_LIST) {
						
						N->attribs.s["ordered"] = "false";	//[!] node_proto //attribs
						//[ ] - set "type" attribute to "bullet"
					}
					
				}else
				//-----------------------------------------
				if(CMARK_NODE_HEADING == target->type) {
					cout << "heading ";
					
					N = Element("heading");
					//[ ] - create a target node(h1)
					N->attribs.i["level"] = (int)target->as.heading.level;	//[!] node_proto //attribs
					//[ ] - set "level" attribute
					
				}else
				//-----------------------------------------
				if(CMARK_NODE_CODE_BLOCK == target->type) {
					cout << "</code >";
					
					N = Element("code");
					//[ ] - create a target node(code)
					if (target->as.code.info.len > 0)
						N->attribs.s["info"] = string((char*)target->as.code.info.data, (int)target->as.code.info.len);	//[!] node_proto //attribs
						//[ ] - set "info" attribute
					N->attribs.s["value"] = string((char*)target->as.code.literal.data, (int)target->as.code.literal.len);	//[!] node_proto //attribs
					//[ ] - set "value" attribute
					literal = true;
					//[ ] - parent node = same
					
				}else
				//-----------------------------------------
				if(CMARK_NODE_LINK == target->type) {
					cout<<"link ";
					
					N = Element("element");
					//[ ] - create a target element
					N->attribs.s["link"] = string((char*)target->as.link.url.data, (int)target->as.link.url.len);	//[!] node_proto //attribs
					//[ ] - set "link" attribute
					N->attribs.s["title"] = string((char*)target->as.link.title.data, (int)target->as.link.title.len);	//[!] node_proto //attribs
					//[ ] - set "title" attribute
					
				}else
				//-----------------------------------------
				if(CMARK_NODE_IMAGE == target->type) {
					cout<<"image ";
					
					N = Element("element");
					//[ ] - create a target element
					N->attribs.s["image"] = string((char*)target->as.link.url.data, (int)target->as.link.url.len);	//[!] node_proto //attribs
					//[ ] - set "image" attribute
					N->attribs.s["title"] = string((char*)target->as.link.title.data, (int)target->as.link.title.len);	//[!] node_proto //attribs
					//[ ] - set "title" attribute
					
				} else { //--------------------------------
					cout<<"other ";
				
					N = Element(string(type, strlen(type)));
					//[ ] - create a target node(type)
				}
				//-----------------------------------------
				
				N->M.parent = P;
				P->M.children.push_back(N);
				
				if (!literal) {
					P = N;
				}
				
			} else if (target->first_child) {

				if (P != M) {
					P = P->M.parent;
				}
			}
		}
		cmark_iter_free(iter);
		//--------------------------------------------------------
		cout << cmark_render_xml(asset, CMARK_OPT_DEFAULT) <<endl;
		return M;
	};
}
