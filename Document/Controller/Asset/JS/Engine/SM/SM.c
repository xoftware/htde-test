#include "../../../../../../htde.h"
#include "jsapi.h"
#include "js/Initialization.h"
#include "js/Proxy.h"
using namespace htde;
using namespace htde::Utils;
extern "C" {
	//Component
	void trace_node_js(JSTracer* tracer, void* data) {
		//JS_CallObjectTracer(tracer, (JS::Heap<JSObject*>*)data, "node_js");
		JS::TraceEdge(tracer, (JS::Heap<JSObject*>*)data, "node_js");
	};
	struct FrontEnd {
		   FrontEnd(JSContext *cx, JS::HandleObject el) {
			   this->cx = cx;
			   this->node = el;
			   if((this->node != NULL)
			   && (this->node.get()))
				   JS_AddExtraGCRootsTracer(this->cx, trace_node_js, &this->node);
		   };
		  ~FrontEnd() {
			   if((this->node != NULL)
			   && (this->node.get()))
				   JS_RemoveExtraGCRootsTracer(this->cx, trace_node_js, &this->node);
		   };
		
		JSContext *cx;
		JS::Heap<JSObject*> node;
	};
	struct BackEnd {
		   BackEnd(node_p node) {
			 this->node = node;
		   }	   node_p node;
	};
	struct runtime_t {
		constructor::Document*			D;
		JSContext*						cx;
		JS::PersistentRooted<JSObject*> gl;
		bool STATE_SET=0; //[prop]
	};	typedef shared_ptr<runtime_t> 	runtime;
	//Utils
	namespace htde::Utils::Script {
		int def_prop_new(runtime_t *rt, JS::HandleObject el, string prop, JSObject* obj) {
			
			JS::RootedValue o(rt->cx); o.setObject(*obj);
			if (!JS_DefineProperty(rt->cx, el, prop.c_str(), o, 
				JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT, NULL, NULL 
			))
				return 1;
			return 0;
		};
		int def_prop_obj(runtime_t *rt, JS::HandleObject el, string prop, JS::HandleObject ao) {
			
			JS::RootedValue a(rt->cx); a.setObject(*ao.get());
			if (!JS_DefineProperty(rt->cx, el, prop.c_str(), a, 
				JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT, NULL, NULL 
			))
				return 1;
			return 0;
		};
		int def_prop(runtime_t *rt, JS::HandleObject el, string prop, string value) {
			
			JS::RootedValue s(rt->cx); s.setString(JS_NewStringCopyZ(rt->cx, value.c_str()));
			if (!JS_DefineProperty(rt->cx, el, prop.c_str(), s, 
				JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT, NULL, NULL 
			))
				return 1;	
			return 0;
		};
		int set_prop(runtime_t *rt, JS::HandleObject el, string prop, string value) {
			
			JS::RootedValue s(rt->cx); s.setString(JS_NewStringCopyZ(rt->cx, value.c_str()));
			if (!JS_SetProperty(rt->cx, el, prop.c_str(), s))
				return 1;
				
			return 0;
		};
		int set_prop_tof(runtime_t *rt, JS::HandleObject el, string prop, bool value) {
			
			JS::RootedValue b(rt->cx); b.setBoolean(value);
			if (!JS_SetProperty(rt->cx, el, prop.c_str(), b))
				return 1;
				
			return 0;
		};
		int set_prop_int(runtime_t *rt, JS::HandleObject el, string prop, int value) {
			
			JS::RootedValue i(rt->cx); i.setInt32(value);
			if (!JS_SetProperty(rt->cx, el, prop.c_str(), i))
				return 1;
				
			return 0;
		};
		int set_prop_num(runtime_t *rt, JS::HandleObject el, string prop, double value) {
			
			JS::RootedValue n(rt->cx); n.setNumber(value);
			if (!JS_SetProperty(rt->cx, el, prop.c_str(), n))
				return 1;
				
			return 0;
		};
		int set_prop_obj(runtime_t *rt, JS::HandleObject el, string prop, JSObject* obj) {
			
			JS::RootedValue o(rt->cx); o.setObject(*obj);
			if (!JS_SetProperty(rt->cx, el, prop.c_str(), o))
				return 1;
				
			return 0;
		};
		string get_prop(runtime_t *rt, JS::HandleObject el, string prop) {
			
			JS::RootedValue s(rt->cx);
			if (!JS_GetProperty(rt->cx, el, prop.c_str(), &s))
				return "";
			
			return JS_EncodeString(rt->cx, s.toString());
		};
		string cx_get_prop(JSContext *cx, JS::HandleObject o, string prop) {
			
			JS::RootedValue s(cx);
			if (!JS_GetProperty(cx, o, prop.c_str(), &s))
				return "";
			
			return JS_EncodeString(cx, s.toString());
		};		
		bool def_prop_proxy(runtime_t *rt, JS::HandleObject el, string prop, JS::HandleObject o, string proxy) {
			
			JS::RootedValue argv(rt->cx); argv.setObject(*o.get());
			JS::RootedValue rval(rt->cx);
			JS::HandleValueArray args(argv);
			
			if(!JS_CallFunctionName(rt->cx, rt->gl, proxy.c_str(), args,&rval))
				return false;
			
			if (!JS_DefineProperty(rt->cx, el, prop.c_str(), rval, 
				JSPROP_READONLY | JSPROP_ENUMERATE | JSPROP_PERMANENT, NULL, NULL 
			))
				return false;
				
			return true;
		};
		bool def_prop_proto(runtime_t *rt, JS::HandleObject el, string type) {
			
			JS::RootedValue Ec(rt->cx);
			JS_GetProperty(rt->cx, rt->gl, type.c_str(), &Ec);
			JS::RootedObject Eo(rt->cx, &Ec.toObject());
			
			JS::RootedValue Ep(rt->cx);
			JS_GetProperty(rt->cx, Eo, "prototype", &Ep);
			JS::RootedObject element_c_proto(rt->cx, &Ep.toObject());
			
			JS_SetPrototype(rt->cx, el, element_c_proto);
		};
		bool set_priv_proto(runtime_t *rt, string type, node_p model) {
			JS::RootedValue Ec(rt->cx);
			JS_GetProperty(rt->cx, rt->gl, type.c_str(), &Ec);
			JS::RootedObject Eo(rt->cx, &Ec.toObject());
					
			JS::RootedValue Ep(rt->cx);
			JS_GetProperty(rt->cx, Eo, "prototype", &Ep);
			JS::RootedObject element_c_proto(rt->cx, &Ep.toObject());
					
			//JS_SetPrivate(Eo, new BackEnd(nullptr));
			JS_SetPrivate(element_c_proto, new BackEnd(model));
			return true;
		};
		
		void link_frontend_recurse(runtime_t *rt, JS::HandleObject el_t) {
			
			node_p target = ((BackEnd*)JS_GetPrivate(el_t))->node;
			delete (FrontEnd*)target->frontend;
			target->frontend = new FrontEnd(rt->cx, el_t);
			
			cout<<"[JS] LINK_FRONTEND_RECURSE : "<<target->attribs.s["id"]<<endl;	//[!] node_proto //attribs
			
			JS::RootedValue av_t(rt->cx);
			if(!JS_GetProperty(rt->cx, el_t, "children",&av_t)) {
				cout<<"[JS][ERROR][link_frontend_recurse] : children is not Elements Proxy"<<endl;
				return;
			}
			JS::RootedObject ao_t(rt->cx, &av_t.toObject());
			
			JS::Rooted<JS::IdVector> ids(rt->cx, JS::IdVector(rt->cx));
			JS_Enumerate(rt->cx, ao_t, &ids); JS::RootedId id(rt->cx);
			for (jsid idVal : ids) {
				id = idVal;
				
				JS::RootedValue v_c(rt->cx);
				JS_GetPropertyById(rt->cx, ao_t, id, &v_c);
				JS::RootedObject el_c(rt->cx, &v_c.toObject());
				
				link_frontend_recurse(rt, el_c);
			}
		};
		void unlink_frontend_recurse(runtime_t *rt, node_p target) {
			
			delete (FrontEnd*)target->frontend;
			target->frontend = new FrontEnd(rt->cx, nullptr);
		
			cout<<"[JS] UNLINK_FRONTEND_RECURSE : "<<target->attribs.s["id"]<<endl;	//[!] node_proto //attribs
			
			for(auto child : target->M.children) {
				unlink_frontend_recurse(rt, child);
			}
		};
		
		string toString(runtime_t *rt, JS::HandleValue s) {
			return string(JS_EncodeString(rt->cx, s.toString()));
		};
		node_p getElementById(node_p target, string id) {
			
			if ( id == target->attribs.s["id"] )	//[!] node_proto //attribs
				return target;
			
			for(auto child : target->M.children) {
				auto found = getElementById(child, id);
				if ( found!= NULL )
					return found;
			}
			return NULL;
		};
		JSObject* Node_Recurse(runtime_t *rt, node_p target);
	}
	namespace htde::Utils::Script {
		bool texttype(runtime_t *rt, node_p target, JS::HandleObject el_t, string val) {
			//delete all children from target
			JS::RootedValue t(rt->cx); t.setObject(*el_t.get());
			JS::RootedValue a(rt->cx); JS_GetProperty(rt->cx, el_t, "children",&a);
			JS::RootedObject ao(rt->cx, &a.toObject()); int i= target->M.children.size();
			while(i--) {
				JS_DeleteElement(rt->cx, ao, i);
			}i=0;
			//insert all children from textbody
			node_p model = constructor::M::Parsers["JSMD"]("\n"+val+"\n");	cout<<"INSERTING TEXTTYPES/TEXTNODES"<<endl;
			for(auto child:model->M.children) {
				JS::RootedObject el_c(rt->cx, Script::Node_Recurse(rt, child));
				JS_SetPrivate(el_c, new BackEnd(child));	
				child->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
				JS_SetProperty(rt->cx, el_c, "parent", t);	
				JS_SetElement(rt->cx, ao, i++, el_c);
			}
			return true;
		};
		bool textbody(runtime_t *rt, node_p target, JS::HandleObject el_t, string val) {
			//
			JS::RootedValue t(rt->cx); t.setObject(*el_t.get());
			JS::RootedValue a(rt->cx); JS_GetProperty(rt->cx, el_t, "children",&a);
			JS::RootedObject ao(rt->cx, &a.toObject());	int i= target->M.children.size();
			//update old textbody
			JS::Rooted<JS::IdVector> ids(rt->cx, JS::IdVector(rt->cx));
			JS_Enumerate(rt->cx, ao,&ids); JS::RootedId id(rt->cx);
			for(jsid idVal: ids) {
				  id=idVal;
				JS::RootedValue c(rt->cx); JS_GetPropertyById(rt->cx, ao, id, &c);
				JS::RootedObject el_c(rt->cx, &c.toObject());
				node_p child = ((BackEnd*)JS_GetPrivate(el_c))->node;
				if(child->V.Flags[0]==4) {
					texttype(rt, child, el_c, val);
					return true;
				}
			}
			//create new textbody
			node_p model = constructor::M::Parsers["JSMD"]("\n"+val+"\n");	cout<<"INSERTING TEXTBODY"<<endl;
			model->attribs.i["txtid"] = rt->D->V.Window->Text_Init(model);
			JS::RootedObject el_m(rt->cx, Script::Node_Recurse(rt, model));
			JS_SetPrivate(el_m, new BackEnd(model));	
			model->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
			//insert textbody into target
			JS_SetProperty(rt->cx, el_m, "parent", t);
			JS_SetElement(rt->cx, ao, i, el_m);
			return true;
		};
	}
	//Document
	bool global_construct(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		if(args[0].isString()) {
			cout << "[js] CALLED: new Document(URL://manifest) { OPEN }" <<endl;
			new constructor::Document(JS_EncodeString(cx, args[0].toString()));
			cout << "[js] DOCUMENTS SIZE: " << Documents.size() -1 <<endl;
		//	args.rval().setNumber((uint32_t)(Documents.size() -1));
		//	return true;
		}
		
		args.rval().setNull();
		return true;
	};
	bool global_call(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		cout << "[js] CALLED: Document(URL://manifest) { NAVIGATE / REFRESH }" <<endl;
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		
			se->D->Manifest = "";
		//	JS_DestroyContext(cx);
		
		//Document(manifest);
			//save Document configuration
			//D_OLD->Manifest = "";
			//D_NEW = new Document(args[0]);
			//configure [new] Document => window/tab, history
		
		args.rval().setNull();
		return true;
	};
	void global_finalize(JSFreeOp *fop, JSObject *obj) {
	
		cout<<"finalize global"<<endl;
		
		auto backend = (BackEnd*)JS_GetPrivate(obj);
		auto frontend = (FrontEnd*)(backend->node->frontend);
		delete frontend;
		delete backend;
	};
	static JSClassOps global_ops = {
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		global_finalize,
		global_call,
		nullptr,
		global_construct,
		JS_GlobalObjectTraceHook
	};
	static JSClass global_class = {
		"global",
		JSCLASS_GLOBAL_FLAGS,
		&global_ops
	};
	//Element(s)
	unordered_map<string, JSObject*> element_c_byid;
	void element_c_finalize(JSFreeOp *fop, JSObject *obj) {
		auto backend = (BackEnd*)JS_GetPrivate(obj);
		if(backend->node != NULL) {
			cout<<"finalize element - backend not null : "<<backend->node->attribs.s["id"]<<endl;	//[!] node_proto //attribs
			auto frontend = (FrontEnd*)(backend->node->frontend);
			delete frontend;
		} else {
			cout<<"finalize element - backend IS null (prototype)"<<endl;
		}
		delete backend;
	};
	bool element_c_set(JSContext *cx, JS::HandleObject el_t, JS::HandleId id, JS::MutableHandleValue vp, JS::ObjectOpResult& result) {
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if (JS_GetPrivate(el_t) == NULL) {
			cout << "[element_c_set] not linked yet, backend is null!" <<endl;
			result.succeed(); return true;
		}
		node_p target = ((BackEnd*)JS_GetPrivate(el_t))->node;
		
		JS::RootedValue vk(rt->cx);	JS_IdToValue(rt->cx,id,&vk);
		if(vk.isString()) {	auto key = Script::toString(rt, vk);
			
			/*
			if(vp.isBoolean()) {
				cout << "[Element_c_set] boolean set|"<<key<<" on "<<"something" <<endl; 
				target->attribs.i[key]=(int)vp.toBoolean();
			}else
			if(vp.isNumber()) {
				cout << "[Element_c_set] number set |"<<key<<" on "<<"something" <<endl; 
				target->attribs.f[key]=(double)vp.toNumber();
			}else
			*/
			if(vp.isString()) { auto val = Script::toString(rt, vp); 
				cout << "[Element_c_set] string set |"<<key<<" on "<<"something" <<endl; 
				
				if(("value" == key)
				&& (510&target->V.Flags[0])) {		//
					if (target->V.Flags[0] == 256) {	//textnode
						target->attribs.s[key]=val;
					}else
					if (target->V.Flags[0] == 2) {		//rectangle
						Script::textbody(rt, target, el_t, val);
					}else {								//textbody/texttype		//[?] parser/default
						Script::texttype(rt, target, el_t, val);
					}	
					rt->D->V.Window->DrawFlag = true;
				}else {
					target->attribs.s[key]=val;
					
					if (rt->D->V.Attrs.end() != std::find(rt->D->V.Attrs.begin(), rt->D->V.Attrs.end(), key)) {	//[1st class]
						rt->D->V.Assign(target);
						rt->D->V.Window->DrawFlag = true;
					}
				}
				
			}else {
				cout << "[Element_c_set] thing set  |"<<key<<" on "<<target->M.Type <<endl;
			}
			
		}
		
		result.succeed();
		return true;
	};	//[element]
	static JSClassOps element_c_ops = {
		nullptr,
		nullptr,
		nullptr,
		element_c_set,
		nullptr,
		nullptr,
		nullptr,
		element_c_finalize,
		nullptr,
		nullptr,
		nullptr,
		nullptr
	};
	static JSClass element_c = {
		"Element",
		JSCLASS_HAS_PRIVATE,
		&element_c_ops
	};
	bool element_c_construct(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		auto l=(int)args.length(); string type=""; string id=""; 
		 
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		{
			JSAutoRequest ar(rt->cx);
			JSAutoCompartment ac(rt->cx, rt->gl);
			
			if(!args.isConstructing()) {
				if(args[0].isString()) {
					string id = Script::toString(rt, args[0]);
					auto Node = Script::getElementById(rt->D->M.Root, id);
					if ( Node != NULL ) {
					//	JS::RootedObject el(cx, (JSObject*) Node->frontend);	//[heap]
						JS::RootedObject el(cx, ((FrontEnd*)Node->frontend)->node);
						args.rval().setObject(*el.get());
						return true;
					}
				}
				args.rval().setNull();
				return true;
			}
			
			if((l>0) && args[0].isObject()) {
				
				JS::RootedObject el_t(rt->cx, &args[0].toObject());
				if(!JS_InstanceOf(rt->cx, el_t, &element_c, nullptr)) {
					args.rval().setNull();
					return true;
				}
				
				if((l>1) && args[1].isString())	
					id = Script::toString(rt, args[1]);
				type   = Script::get_prop(rt, el_t, "type");
				
				cout<<"[JS][CLONE ELEMENT] type : " << type <<endl;
				
				node_p model = constructor::Element(type);
				JS::RootedObject el_m(rt->cx, Script::Node_Recurse(rt, model));
				
				JS_SetPrivate(el_m, new BackEnd(model));	
				//model->frontend = el_m.get();			//[heap]
				//model->frontend = new FrontEnd(rt->cx, el_m);	//[frontend]
				model->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
				
				model->attribs.s["id"] = id;	//[!] node_proto //attribs
				Script::set_prop(rt, el_m, "id", id);	//[textapi] [!] SHOULD BE SET PROP
				//------------------------------------------------------------
				//node_p templ = ((BackEnd*)JS_GetPrivate( el_t ))->node;
				// copy templ->local to model->local		//[?]
				// copy event listeners //.on.mouseup		//[ ]
				// copy properties		//.any				//[?]
				//------------------------------------------------------------
				args.rval().setObject(*el_m.get());
			}
			else {	
				if((l>0) && args[0].isString())	type = Script::toString(rt, args[0]);	
				if((l>1) && args[1].isString())	id   = Script::toString(rt, args[1]);
			
				node_p model = constructor::Element(type);
				JS::RootedObject el_m(rt->cx, Script::Node_Recurse(rt, model));
				
				JS_SetPrivate(el_m, new BackEnd(model));
			//	model->frontend = el_m.get();			//[heap]
				//model->frontend = new FrontEnd(rt->cx, el_m);	//[frontend]
				model->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
				
				model->attribs.s["id"] = id;	//[!] node_proto //attribs
				Script::set_prop(rt, el_m, "id", id);	//[textapi] [!] SHOULD BE SET PROP
				
				args.rval().setObject(*el_m.get());
			}
		}
		return true;
	};
	string Elements_Proxy_script = R"(
		Elements_proxy = function(children) { var _me_ =
			new Proxy
			(	children
			,	{	'apply' 
				:	function(targ, self, args) 
					{	
						return self[targ].apply(this, args);
					}
				,	'set'				
				:	function(targ, prop, node, call)
					{	
						if(prop/1 == prop) 
						{	if("[object Element]" === Object.prototype.toString.call(node))
							{
								if (prop/1 > targ.length) {
									prop   = targ.length
								}
								if (prop/1 < 0) {
									return false;	//[?] unshift
								}
								
								//------------------------------------
									if(!Elements_set(targ, prop/1, node)) {
										return false;
									}
								//	if(assign) Elements_assign(node);
								//------------------------------------
							
								if (node.parent!= targ.target) {
									node.parent = targ.target;
								}
							
							} else { return false; }
						}
						
						targ[prop] = node; 
						
						console.log('Set '+prop+' to '+node+' on '+targ.target);
						return true;
					}
				,	'deleteProperty'
				:	function(targ, prop) 
					{	
						console.log('Deleted '+prop);
						//------------------------------------
							Elements_del(targ, prop/1);
						//------------------------------------
						return true;
					}		
				}
			);
			/*
			var assign = true;
			
			["push","pop","shift","unshift","splice"].forEach
			(	function(method)		
				{	var _method_ 		= _me_[method];
					Object.defineProperty(_me_,method, 
					{	enumerable		: false
					,	configurable 	: false
					,	writable	 	: false
					,	value			: function()	//[?]
						{	
							console.log(method+" pre-hook"); assign = false; var rval = _method_.apply(_me_,arguments);
							console.log(method+" post-hook"); assign = true; return rval; //Elements_assign(children.target); 
						}
					});
				}
			);
			*/
			return _me_;
		};
	)";
	/*
	bool Elements_assign(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(!args[0].isObject()) {
			cout<<"[JS][ERROR][Elements_set] : node is not [object Element]"<<endl;
			return true;
		}
		JS::RootedObject el_c(cx, &args[2].toObject());
		if(!JS_InstanceOf(cx, el_c, &element_c, nullptr)) {
			cout<<"[JS][ERROR][Elements_set] : node is not [object Element]"<<endl;
			return true;
		}
		node_p child  = ((BackEnd*)JS_GetPrivate( el_c ))->node;			//[private]
		
		cout << "Elements_assign called: "<< child->attribs.s["id"] <<endl;
		rt->D->V.Assign(child); //[!]calc
		
		args.rval().setBoolean(true);
		return true;
	};
	*/
	bool Elements_set(JSContext *cx, unsigned argc, JS::Value *vp) {	cout<<"Elements_set called"<<endl;
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(!args[0].isObject()) {
			cout<<"[JS][ERROR][Elements_set] : targ is not Elements Proxy"<<endl;
			args.rval().setBoolean(false); return true;
		}
		if(!args[1].isNumber()) {
			cout<<"[JS][ERROR][Elements_set] : prop is not Number"<<endl;
			args.rval().setBoolean(false); return true;
		}
		if(!args[2].isObject()) {
			cout<<"[JS][ERROR][Elements_set] : node is not [object Element]"<<endl;
			args.rval().setBoolean(false); return true;
		}
	//	if(!args[3].isBoolean()) { 
	//	//	cout<<"[JS][ERROR][Elements_set] : assign is not Boolean"<<endl;
	//		return true;
	//	}
		
		JS::RootedObject el_c(cx, &args[2].toObject());
		if(!JS_InstanceOf(cx, el_c, &element_c, nullptr)) {
			cout<<"[JS][ERROR][Elements_set] : node is not [object Element]"<<endl;
			args.rval().setBoolean(false); return true;
		}
		JS::RootedObject ao_t(cx, &args[0].toObject());
		JS::RootedValue t(rt->cx);
		if(!JS_GetProperty(rt->cx, ao_t, "target",&t)) {
			cout<<"[JS][ERROR][Elements_set] : targ is not Elements Proxy"<<endl;
			args.rval().setBoolean(false); return true;
		}
		JS::RootedObject el_t(cx, &t.toObject());	//[private] @
		//-----------------------------------------------------------------
		
		//node_p target = ((BackEnd*)JS_GetInstancePrivate(cx, el_t, &element_c, NULL))->node;	//[private]
		//node_p child  = ((BackEnd*)JS_GetInstancePrivate(cx, el_c, &element_c, NULL))->node;	//[private]
		node_p target = ((BackEnd*)JS_GetPrivate( &t.toObject() ))->node;	//[private]
		node_p child  = ((BackEnd*)JS_GetPrivate( el_c ))->node;			//[private]
		auto   index  = (int)args[1].toNumber();
		auto   size   = (int)target->M.children.size();
		
		//[textapi]	//[?] return if not:
		//textbody into rectangle
		//texttype into textbody, texttype
		//textnode into texttype
		if(!(target->V.Flags[1] & child->V.Flags[0])) {
			cout<<"[JS][WARNING][Elements_set] : not texttype into texttype [DOM MISMATCH] | Flags= "
				<<	target->V.Flags[1]	<<"("<<target->M.Type<<")"	<<" & "
				<<	child->V.Flags[0]	<<"("<<child->M.Type<<")"	<<endl;
			args.rval().setBoolean(false); return true;
		}
		
		cout<<"[JS][Elements_set] : prop = "<<index
								<<", max = "<<size-1<<endl;
		
		child->M.parent = target;
		
		//3			//3
		if(index == size) {
			target->M.children.push_back(child);
		} else 
		if(index < size) {
			if(index >= 0) {
				node_p child_old = target->M.children[index];
								   target->M.children[index] = child;
								   
				if((((FrontEnd*)target->frontend)->node != NULL)	//[frontend]
				&& (target->M.children.end() == std::find(target->M.children.begin(), target->M.children.end(), child_old)))
					Script::unlink_frontend_recurse(rt, child_old);	
			} else {
				cout<<"[JS][ERROR][Elements_set] : index is less than 0 [FATAL DOM MISMATCH]"<<endl;
				args.rval().setBoolean(false);
				return false;
			}
		} else {
			cout<<"[JS][ERROR][Elements_set] : index is greater than size [FATAL DOM MISMATCH]"<<endl;
			args.rval().setBoolean(false);
			return false;
		}
		
		if((((FrontEnd*)target->frontend)->node != NULL)	//[frontend]
		&& (((FrontEnd*)child->frontend)->node == NULL))
			Script::link_frontend_recurse(rt, el_c);
		
		
		//if param 4 == true
		rt->D->V.Assign(child); //[!]calc
		rt->D->V.Window->DrawFlag = true;
		
		//-----------------------------------------------------------------
		cout <<"[JS][Elements_set] : "<<child->M.parent->attribs.s["id"]	//[!] node_proto //attribs
			 <<" > "<<child->M.parent->M.children[index]->attribs.s["id"]<<endl;	//[!] node_proto //attribs
		
		args.rval().setBoolean(true);
		return true;
	};
	bool Elements_del(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(!args[0].isObject()) {
		//	cout<<"[JS][ERROR][Elements_del] : targ is not Elements Proxy"<<endl;
			return true;
		}
		if(!args[1].isNumber()) {
		//	cout<<"[JS][ERROR][Elements_del] : prop is not Number"<<endl;
			return true;
		}
		
		JS::RootedObject ao_t(cx, &args[0].toObject());
		JS::RootedValue t(rt->cx);
		if(!JS_GetProperty(rt->cx, ao_t, "target",&t)) {
			cout<<"[JS][ERROR][Elements_del] : targ is not Elements Proxy"<<endl;
			return true;
		}
		JS::RootedObject el_t(cx, &t.toObject());	//[private] @
		
		//-----------------------------------------------------------------
		//node_p target = ((BackEnd*)JS_GetInstancePrivate(cx, el_t, &element_c, NULL))->node;	//[private]
		node_p target = ((BackEnd*)JS_GetPrivate( &t.toObject() ))->node;	//[private]
		auto   index  = (int)args[1].toNumber();
		auto   size   = (int)target->M.children.size();
		
		cout<<"[JS][Elements_del] : prop = "<<index
								<<", max = "<<size-1<<endl;
		
		//Delete the Exact Index specified
		//Should always be the last Index
		//[?] - child->M.parent, el_c.parent
		
		//2			//3-1
		if(index == size-1) {
			node_p child_old = target->M.children.back();
							   target->M.children.pop_back();
			
			if((((FrontEnd*)target->frontend)->node != NULL)
			&& (target->M.children.end() == std::find(target->M.children.begin(), target->M.children.end(), child_old)))
				Script::unlink_frontend_recurse(rt, child_old);	//[frontend]
				
		} else {
			cout<<"[JS][ERROR][Elements_del] : index is not size-1 [FATAL DOM MISMATCH]"<<endl;
			args.rval().setBoolean(false);
			return false;
		}
		
		rt->D->V.Window->DrawFlag = true;
		//-----------------------------------------------------------------
		cout<<"[JS][Elements_del] : deleted "<<index<<", size = "<<(int)target->M.children.size()<<endl;
		args.rval().setBoolean(true);
		return true;
	};
	bool Event_sim(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(args[0].isObject()) {
		
			JS::RootedObject e (cx, &args[0].toObject());
			JS::RootedObject el(cx, &args.thisv().toObject());
			if(!JS_InstanceOf(cx, el, &element_c, nullptr)) {
				if(el != rt->gl)
					return true;
			}
			//node_p target = ((BackEnd*) JS_GetInstancePrivate(cx, el, &element_c, NULL))->node;		//[private]
			node_p target = ((BackEnd*) JS_GetPrivate(el))->node;		//[private]
			data_generic Event_Object;
			
			JS::Rooted<JS::IdVector> ids(cx, JS::IdVector(cx));
			if (!JS_Enumerate(cx, e, &ids)) {
				return true;
			}
			for (int i = 0; i < ids.length(); i++) {
				
				string key = "";
				JS::RootedValue k(cx); JS_IdToValue(cx, ids[i], &k);
				JS::RootedValue v(cx); JS_GetPropertyById(cx, e, ids[i], &v); 
				
					 if(k.isNull())   continue;
				else if(k.isString()) key = Script::toString(rt, k);
				else if(k.isNumber()) key = std::to_string((int)k.toNumber());
				else 				  continue;
				
					 if(v.isNull())	  continue;
				else if(v.isString()) Event_Object.s[key] = Script::toString(rt, v);
				else if(v.isInt32())  Event_Object.i[key] = (int)v.toInt32();
				else if(v.isDouble()) Event_Object.f[key] = (float)v.toDouble();
				else if(v.isObject()) {
					
					JS::RootedObject el_x(cx, &v.toObject());
					if(!JS_InstanceOf(cx, el_x, &element_c, nullptr)) {
						if(el_x != rt->gl)
							continue;
					}
					//auto val = ((BackEnd*) JS_GetInstancePrivate(cx, el_x, &element_c, NULL))->node;	//[private]
					auto val = ((BackEnd*) JS_GetPrivate(el_x))->node;	//[private]
					Event_Object.n[key] = val;
					
				}
			}
			Event_Object.s["type"] = Script::get_prop(rt, e, "type");
			Event_Object.n["source"] = 
			Event_Object.n["target"] = target;
			//Event_Object->frontend = el.get();
			se->Dispatch(target, Event_Object); //rt->D->C.Chain(Ev);
		}
		
		args.rval().setNull();
		return true;
	};
	bool style_c_set(JSContext *cx, JS::HandleObject so, JS::HandleId id, JS::MutableHandleValue vp, JS::ObjectOpResult& result) {
	
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(rt->STATE_SET){result.succeed();return true;}	//[prop]
		JS::RootedValue t(rt->cx); JS_GetProperty(rt->cx, so, "target",&t); 
		if(!t.isObject()){result.succeed();return true;}
		//	cout << "[style_c_set]" <<endl;
		JS::RootedObject el_t(cx, &t.toObject());
		node_p target = ((BackEnd*)JS_GetPrivate( &t.toObject() ))->node;
		
		JS::RootedValue vk(rt->cx);
		JS_IdToValue(rt->cx, id, &vk);
		
		if(vk.isString()) {
			
			auto key = Script::toString(rt, vk);
			
			if(target->V.Properties.end() != target->V.Properties.find(key)) {
				
					 if(vp.isBoolean()){ target->V[key].Assignment=(bool)vp.toBoolean(); target->V[key].Inline=true; target->V[key].Assign(target,1); }	//[!] node_proto
				else if(vp.isNumber()) { target->V[key].Assignment=(double)vp.toNumber();target->V[key].Inline=true; target->V[key].Assign(target,1); }	
				else if(vp.isString()) { target->V[key].Assignment=(string)Script::toString(rt, vp); 
																						 target->V[key].Inline=true; target->V[key].Assign(target,1); }	//
				//[!]calc	//use Assign()	//set Inline=true
				rt->D->V.Window->DrawFlag = true;
			}
		}
		
		result.succeed();
		return true;
	};	//[style]
	static JSClassOps style_c_ops = {
		nullptr,
		nullptr,
		nullptr,
		style_c_set,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr,
		nullptr
	};
	static JSClass style_c = {
		"Style",
		JSCLASS_HAS_PRIVATE,
		&style_c_ops
	};
	string Classes_Proxy_script = R"(
		Classes_proxy = function(classes) { return true && 
			new Proxy
			(	classes
			,	{	'apply' 
				:	function(targ, self, args) 
					{	
						return self[targ].apply(this, args);
					}
				,	'set'				
				:	function(targ, prop, node, call)
					{	//------------------------------------
							Classes_set(targ, prop/1, node);
						//------------------------------------
						targ[prop] = node; 
						return true;
					}
				,	'deleteProperty'
				:	function(targ, prop) 
					{	//------------------------------------
							Classes_del(targ, prop/1);
						//------------------------------------
						return true;
					}
				}
			);
		};
	)";
	bool Classes_set(JSContext *cx, unsigned argc, JS::Value *vp) {
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(!args[0].isObject()) {
			return true;
		}
		if(!args[1].isNumber()) {
			return true;
		}
		if(!args[2].isString()) {
			return true;
		}
		JS::RootedObject co_t(cx, &args[0].toObject());
		JS::RootedValue t(rt->cx); JS_GetProperty(rt->cx, co_t, "target",&t);
		JS::RootedObject el_t(cx, &t.toObject());
		node_p target = ((BackEnd*)JS_GetPrivate( &t.toObject() ))->node;
		auto   class_ = Script::toString(rt, args[2]);
		auto   index  = (int)args[1].toNumber();
		auto   size   = (int)target->classes.size();		//[!] node proto //classes
		
		if(index == size) {
			target->classes.push_back(class_);			//
		} else 
		if(index < size) {
			if(index >= 0) {
				target->classes[index] = class_;			//
			} else {
				args.rval().setBoolean(false);
				return false;
			}
		} else {
			args.rval().setBoolean(false);
			return false;
		}
		
		rt->D->V.Assign(target); //[!]calc
		rt->D->V.Window->DrawFlag = true;
		
		args.rval().setBoolean(true);
		return true;
	};
	bool Classes_del(JSContext *cx, unsigned argc, JS::Value *vp) {
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		if(!args[0].isObject()) {
			return true;
		}
		if(!args[1].isNumber()) {
			return true;
		}
		JS::RootedObject co_t(cx, &args[0].toObject());
		JS::RootedValue t(rt->cx); JS_GetProperty(rt->cx, co_t, "target",&t);
		JS::RootedObject el_t(cx, &t.toObject());
		node_p target = ((BackEnd*)JS_GetPrivate( &t.toObject() ))->node;
		auto   index  = (int)args[1].toNumber();
		auto   size   = (int)target->classes.size();		//
		
		if(index == size-1) {
			target->classes.pop_back();					//
		} else {
			args.rval().setBoolean(false);
			return false;
		}
		
		rt->D->V.Assign(target); //[!]calc
		rt->D->V.Window->DrawFlag = true;
		
		args.rval().setBoolean(true);
		return true;
	};
	
	/*
		parse xml;	<- tree
		
		new root;	-> new js document;	//D->C.Engine->CreateNode(node_p root);
										//document API : DOM
		
		new node;	-> new js element;	//D->C.Engine->CreateNode(node_p element);
										//element API : DOM, type, attr
		for each (member):
			member	-> new js event;	//D->C.Engine->CreateMember(event_p mousedn);
										//event API : mousedn, on.mousedn
		
	*/
	namespace htde::Utils::Script {		
		JSObject* Node_Recurse(runtime_t *rt, node_p target) {
			
			auto se = (script_engine *) JS_GetContextPrivate(rt->cx);
			
		//	JS::CallArgs args = JS::CallArgsFromVp(0, NULL);
			
			JS::RootedObject el_t (rt->cx,
				"root" == target->attribs.s["id"]	//[!] node_proto //attribs
				?	rt->gl.get()
				:	JS_NewObject(rt->cx, &element_c)//JS_NewObjectForConstructor(rt->cx, &element_c, args)
			);
			JS::RootedValue t(rt->cx); t.setObject(*el_t.get());
			
			Script::def_prop_proto(rt, el_t, "Element");
			Script::def_prop(rt, el_t, "type", target->M.Type);	//[!] node_proto //type
		//	Script::def_prop_new(rt, el_t, "on", JS_NewObject(rt->cx, NULL));	//[modifiers]	//use element?
			JS::RootedObject so(rt->cx, JS_NewObject(rt->cx,&style_c));	//[style]
			JS::RootedObject co(rt->cx, JS_NewArrayObject(rt->cx, 0)); uint32_t j = 0;//[classes]
			JS::RootedObject ao(rt->cx, JS_NewArrayObject(rt->cx, 0)); uint32_t i = 0;
			
			for(auto class_ : target->classes) {	//[!] node_proto //V.classes -> classes
				JS::RootedValue cv(rt->cx); cv.setString(JS_NewStringCopyZ(rt->cx, class_.c_str()));
				if (!JS_SetElement(rt->cx, co, j++, cv))
					cout << "[js!] ERROR: Node_Recurse -> Classes SetElement" <<endl;
			}
			
			for(auto&P:target->V.Properties) {
				if (!P.second.Inline)	//todo: check for undefined?
					continue;
				//P.first = key
				//P.second = Property;
				switch(P.second.Assignment.index()) {
					case 3: Script::set_prop    (rt,so,P.first,std::get<3>(P.second.Assignment)); break;	//string
					case 2: Script::set_prop_num(rt,so,P.first,std::get<2>(P.second.Assignment)); break;	//float		//double
					case 1: Script::set_prop_num(rt,so,P.first,std::get<1>(P.second.Assignment)); break;	//int		//double
					case 0: Script::set_prop_tof(rt,so,P.first,std::get<0>(P.second.Assignment)); break;	//bool
				}
				P.second.Inline=true; 		//[prop]
				P.second.Assign(target,1);	//[prop]
			}//[!]calc	//[?] default: 0.0f
			
			for(auto& attr : target->attribs.s)	//listeners:	//[!] node_proto //attribs
				Script::set_prop(rt, el_t, attr.first, attr.second);
				
			for(auto& member : target->C->Members) {	//[modifiers] //use proxy (in order to hook flags)
				
				string type = member.first;
				se->Exec("	if(!Document['"+type+"'])"
						+	"	Document['"+type+"'] = function(e){ e=e||{};"
						+		" e.type='"+type+"'; Event_sim.call(this,e)};"
				);
				JS::RootedValue fn(rt->cx);
				if (JS_GetProperty(rt->cx, rt->gl, type.c_str(),&fn))
					JS_SetProperty(rt->cx, el_t  , type.c_str(), fn);
			}
			
			for(auto& child : target->M.children) {
					
				JS::RootedObject el_c(rt->cx, Script::Node_Recurse(rt, child));	
				
				JS_SetPrivate(el_c, new BackEnd(child));
			//	child->frontend = el_c.get();						//[heap]
				//child->frontend = new FrontEnd(rt->cx, el_c);	//[frontend]
				child->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
					
				if (!JS_SetProperty(rt->cx, el_c, "parent", t))
					cout << "[js!] ERROR: Node_Recurse -> SetProperty" <<endl;
			
				if (!JS_SetElement(rt->cx, ao, i, el_c))
					cout << "[js!] ERROR: Node_Recurse -> SetElement" <<endl;
				
				i++;
			}
			//------------------------------------------------------------
			if (!JS_DefineProperty(rt->cx, ao, "target", t, 
				JSPROP_READONLY | JSPROP_PERMANENT, NULL, NULL 
			))
				cout << "[js!] ERROR: Node_Recurse -> target" <<endl;
			
			Script::def_prop_proxy(rt, el_t, "children", ao, "Elements_proxy");
			//------------------------------------------------------------	//[class]
			if (!JS_DefineProperty(rt->cx, co, "target", t, 
				JSPROP_READONLY | JSPROP_PERMANENT, NULL, NULL 
			))
				cout << "[js!] ERROR: Node_Recurse -> target" <<endl;
			
			Script::def_prop_proxy(rt, el_t, "classes", co, "Classes_proxy");
			//------------------------------------------------------------
			if (!JS_DefineProperty(rt->cx, so, "target", t, 
				JSPROP_READONLY | JSPROP_PERMANENT, NULL, NULL 
			))
				cout << "[js!] ERROR: Node_Recurse -> style target" <<endl;
			
			Script::def_prop_obj(rt, el_t, "style", so);
			//------------------------------------------------------------
			
			return el_t.get();
		};
	}

	//API
	bool cursor_c_set(JSContext *cx, JS::HandleObject cur, JS::HandleId id, JS::MutableHandleValue vp, JS::ObjectOpResult& result) {
	
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		auto rt = (runtime_t *) se->local.get();
		
		JS::RootedValue vk(rt->cx);	JS_IdToValue(rt->cx,id,&vk);
		if(vk.isString()) {	auto key = Script::toString(rt, vk);
			
			if (key == "x"){if(vp.isNumber()) {
				rt->D->V.Window->Cursor.x = (int)vp.toNumber();
				rt->D->V.Window->Cursor.Draw(8);
			}}else
			if (key == "y"){if(vp.isNumber()) {
				rt->D->V.Window->Cursor.y = (int)vp.toNumber();
				rt->D->V.Window->Cursor.Draw(8);
			}}else
			if (key == "image"){if(vp.isString()) {
				rt->D->V.Window->Cursor.file = Script::toString(rt, vp); 
				rt->D->V.Window->Cursor.Draw(1);
			}}
		}
		result.succeed();
		return true;
	};
	static JSClassOps cursor_c_ops = { nullptr,nullptr,nullptr,cursor_c_set,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,nullptr	};
	static JSClass cursor_c = {	"Cursor", 0, &cursor_c_ops };
	//
	bool Manual_GC(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		{
			JSAutoRequest ar(cx);
			
			if(args.length() == 0) {
				JS_GC(cx);
			} else {
				JS_MaybeGC(cx);
			}
		}
		
		args.rval().setNull();
		return true;
	};
	bool Console_log(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		
		for(int i=0; i<args.length(); i++) {
			if(args[i].isString()){
			
				cout << JS_EncodeString(cx, args[i].toString()) <<endl;
			
			}
		}

		args.rval().setNull();
		return true;
	};
	bool Request(JSContext *cx, unsigned argc, JS::Value *vp) {
		
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		auto se = (script_engine *) JS_GetContextPrivate(cx);
		
		if(args[0].isString()) {
			
			JS::RootedObject Tx_js(cx, JS_NewObject(cx, NULL));
			
			auto Tx = make_shared<Host::Transfer_Object>();
			Tx->D = se->D;
			Tx->URL = string(JS_EncodeString(cx, args[0].toString())); 
			Tx->Async = false;
			Tx->frontend = Tx_js.get();
			Hosts.Request(Tx);
			
		}else
		if(args[0].isObject()) {
			
			JS::RootedObject Tx_js(cx, &args[0].toObject());
			
			auto Tx = make_shared<Host::Transfer_Object>();
			Tx->D = se->D;
			Tx->URL = Script::cx_get_prop(cx, Tx_js, "URL");
			Tx->Async = false;
			Tx->frontend = Tx_js.get();
			Hosts.Request(Tx);
			
		}
		
		args.rval().setNull();
		return true;
	};
	bool Parse(JSContext *cx, unsigned argc, JS::Value *vp) {
	//[!]Parse(type, code);
		JS::CallArgs args = JS::CallArgsFromVp(argc, vp);
		/*
		
			//[?]
			// M - node_p				//Parse() -> DOM
			// V - ruleset/stylesheet	//Parse() -> .Apply / Rules.Apply
			// C - compiled script		//Parse() -> .Exec	/ Exec 		//Eval
			
			Parse("M", "file.type", "code");
			Parse("V", "file.type", "code");
			Parse("C", "file.type", "code");
		
			Document.Parse("type","code");	//Document.children.push(element);
			Styles.Parse("type","code");	//Styles.push(ruleset);
			Runtime.Parse("type","code");	//Runtime.Evaluate(script);
			
			Runtime.Transpile("type","code");	//Runtime.Evaluate("code");
			Runtime.Compile("type"  ,"code");	//Runtime.Evaluate("script");
			
			Assets[]	//Assets.M[]	//Assets.V[]	//Assets.C[]
			
			if(args[0].isObject())	//Transfer Object
			//	file = get property ("URL")		//[!] - /dir
			//	code = get property ("Data")	//
			
			if(args[0].isString() && args[1].isString())
			//	file = args[0].toString()		//[?] - i.xml
			//	code = args[1].toString()
		
		*/
		if(args[0].isString()) {
			
			auto se = (script_engine *) JS_GetContextPrivate(cx);
			auto rt = (runtime_t *) se->local.get();
			{
				JSAutoRequest ar(rt->cx);
				JSAutoCompartment ac(rt->cx, rt->gl);
				
				auto code = string(JS_EncodeString(cx, args[0].toString()));

				node_p model = rt->D->M.Parse( code );	//
				JS::RootedObject el_m(rt->cx, Script::Node_Recurse(rt, model));
				
				JS_SetPrivate(el_m, new BackEnd(model));
			//	model->frontend = el_m.get();			//[heap]
				//model->frontend = new FrontEnd(rt->cx, el_m);	//[frontend]
				model->frontend = new FrontEnd(rt->cx, nullptr);	//[frontend]
				
				args.rval().setObject(*el_m.get());
			}
			return true;
		} 
		
		args.rval().setNull();
		return false;
	};
	
	/*	TODO:
		[*] - change Tx->success => Tx->status		(int)
		[*] - change Tx->Sync	 => Tx->Async		(bool)
		[?] - change Tx->callback => Tx->callback	(function pointer)
		
		
		[ ] - profile
		[?] - PersistentRooted smart pointer lifetime
		[*] - delete Tx->frontend (pointer)
		[ ] - finalize element (element ops)
		[ ] - clean up conventions / internal API
		
		[ ] - frontend object API (get, set, ...)
		[ ] - convert frontend => Tx->local (in Protocol module: GET, POST)
		
		[*] - global as JS function/constructor
		
		----------------------------------------------
		[ ] - Init(constructor::Document *D)
		[ ] - Parse - BE/FE		|	Parse(Tx_js) / Parse(type, code)
		[ ] - DOM / GC			|	Append, Delete
		[ ] - Members / Events	|	.mousedn / .on.mousedn
		
		
	*/
	
	//Engine
	struct js_sm : script_engine {	
		
		int Dispatch(node_p target, data_generic &Event_Object) {
//			cout << "[JS1] ~=~=~=~ [Ev] : "<<target->attribs.s["id"]<<endl;	//[!] node_proto //attribs
			auto rt = (runtime_t *) this->local.get();
			{
				JSAutoRequest ar(rt->cx);
				JSAutoCompartment ac(rt->cx, rt->gl);
				
				auto type = Event_Object.s["type"];		//[old] string type = "on" + Event_Object.s["type"];
				auto source=Event_Object.n["source"];
				
				if(((FrontEnd*)target->frontend)->node == NULL) {
//					cout << "[JS][EVENT] no element found : "<<type <<endl;
					return 1;
				}
				JS::RootedObject el(rt->cx, ((FrontEnd*)target->frontend)->node);
				JS::RootedObject e (rt->cx, JS_NewObject(rt->cx, NULL));	//&event_c [object Event]
				
				for(auto prop : Event_Object.s)	Script::set_prop(rt, e, prop.first, prop.second);
				for(auto prop : Event_Object.i)	Script::set_prop_num(rt, e, prop.first, (double)prop.second);
				for(auto prop : Event_Object.f)	Script::set_prop_num(rt, e, prop.first, (double)prop.second);
				for(auto prop : Event_Object.n)	Script::set_prop_obj(rt, e, prop.first, ((FrontEnd*)prop.second->frontend)->node.get()); 	//[*]
			/*	
				JS::RootedValue o(rt->cx);
				JS_GetProperty(rt->cx, el, "on", &o); if(!o.isObject()) {	//[modifiers] //use element?
//					cout << "[JS][EVENT] no callbacks found : "<<type <<endl;
					return 1;
				}
				JS::RootedObject on(rt->cx, &o.toObject());
			*/	
			//	JS::RootedValue func(rt->cx); JS_GetProperty(rt->cx, on, type.c_str(), &func);
				JS::RootedValue func(rt->cx); JS_GetProperty(rt->cx, el, ("on"+type).c_str(), &func);
				JS::RootedValue rval(rt->cx);
				JS::RootedValue argv(rt->cx); argv.setObject(*e.get()); 
				JS::HandleValueArray args(argv);
				if(!JS_CallFunctionValue(rt->cx, el, func, args,&rval)) {
//					cout << "[JS][EVENT] no callback found : "<<type <<endl;
					return 1;
				}
				
				//use Event_Object pointer in param
				//update Event_Object->bubble	from e["bubble"]
				//update Event_Object->cancel	from e["cancel"]
				JS::Rooted<JS::IdVector> ids(rt->cx, JS::IdVector(rt->cx)); JS_Enumerate(rt->cx, e,&ids);
				for (int i = 0; i < ids.length(); i++) {
					
					string key = "";
					JS::RootedValue k(rt->cx); JS_IdToValue(rt->cx, ids[i], &k);
					JS::RootedValue v(rt->cx); JS_GetPropertyById(rt->cx, e, ids[i], &v); 
					
						 if(k.isString()) key = Script::toString(rt, k);
					else if(k.isNumber()) key = std::to_string((int)k.toNumber());
					else continue;
					
						 if(v.isBoolean())Event_Object.i[key] = (int)v.toBoolean();
					else if(v.isInt32())  Event_Object.i[key] = (int)v.toInt32();
					else if(v.isDouble()) Event_Object.f[key] = (float)v.toDouble();
					else if(v.isString()) Event_Object.s[key] = Script::toString(rt, v);
					else if(v.isObject()) {
						JS::RootedObject el_x(rt->cx, &v.toObject());
						if(!JS_InstanceOf(rt->cx, el_x, &element_c, nullptr)) {
							if(el_x != rt->gl)
								continue;
						}
						Event_Object.n[key] = ((BackEnd*)JS_GetPrivate(el_x))->node;	//[private];
					}
				}
				Event_Object.s["type"] = type;
				Event_Object.n["source"] = source;
				Event_Object.n["target"] = target;
			}
			return 0;
		};
		
		int Receive(Host::Transfer Tx) {
//			cout << "[JS1] ======= [Tx]" <<endl;
			auto rt = (runtime_t *) this->local.get();
			{
				JSAutoRequest ar(rt->cx);
				JSAutoCompartment ac(rt->cx, rt->gl);
				
				JS::RootedObject Tx_js(rt->cx, (JSObject*) Tx->frontend);	//[heap]
				
				Script::set_prop(rt, Tx_js, "Data", Tx->Data);
				JS::RootedValue Tx_v(rt->cx); Tx_v.setNumber((uint32_t)Tx->status);
				
				JS::RootedValue func(rt->cx);
				JS::RootedValue rval(rt->cx);
				JS::HandleValueArray args(Tx_v);
				
				if(!JS_CallFunctionName(rt->cx, Tx_js , "Receive", args,&rval)) {
					if (JS_GetProperty (rt->cx, rt->gl, "Receive",&func))
						JS_SetProperty (rt->cx, Tx_js , "Receive", func);
					JS_CallFunctionName(rt->cx, Tx_js , "Receive", args,&rval);
				}
			}
			return 0;
		};
		
		//int Prop(node_p model, string prop) {};	//[!]calc
		
		int Root(node_p model) {
			
			auto rt = (runtime_t *) this->local.get();
			{
				JSAutoRequest ar(rt->cx);
				JSAutoCompartment ac(rt->cx, rt->gl);
				
				model->attribs.s["id"] = "root";	//[!] node_proto //attribs
				JS::RootedObject el_m(rt->cx, Script::Node_Recurse(rt, model));
				
				JS_SetPrivate(el_m, new BackEnd(model));
				model->frontend = new FrontEnd(rt->cx, nullptr); //[?] how the hell was this missing?
				//model->frontend = el_m.get();				//[heap]
				//model->frontend = new FrontEnd(rt->cx, el_m);	//[frontend]
				Script::link_frontend_recurse(rt, el_m);	//[frontend]
				
			}
			return 0;
		};
		
		int Prop(node_p target, string key, int ns) {	//[prop]
			
			auto rt = (runtime_t *) this->local.get();
				 rt->STATE_SET = true;
			{
				JSAutoRequest ar(rt->cx);
				JSAutoCompartment ac(rt->cx, rt->gl);
				
				if(((FrontEnd*)target->frontend)->node == NULL) {
					cout << "[JS][PROP] no element found"<<endl;
					return 1;
				}
				JS::RootedObject el(rt->cx, ((FrontEnd*)target->frontend)->node);
				
				if(ns == 1) { //V	(called from Document Assign)	//[?]todo: batch
					JS::RootedValue s(rt->cx); JS_GetProperty(rt->cx,el,"style",&s); 
					JS::RootedObject so(rt->cx, &s.toObject());
					
					auto & val = target->V[key];
					switch(val.Assignment.index()) {
						case 3: Script::set_prop    (rt,so,key,std::get<3>(val.Assignment)); break;	//string
						case 2: Script::set_prop_num(rt,so,key,std::get<2>(val.Assignment)); break;	//float		//double
						case 1: Script::set_prop_num(rt,so,key,std::get<1>(val.Assignment)); break;	//int		//double
						case 0: Script::set_prop_tof(rt,so,key,std::get<0>(val.Assignment)); break;	//bool
					}
					val.Inline=false; 		//[prop]
					val.Assign(target,1);	//[prop]
				}
			/*
				else if(ns == 0) { //M	(called from Document)
					
					//if val is string (attribs.s)
						//set el[key] = val
						//if key is capitalized, call D->V.Assign(target) [?]
					
				}
			*/
				
			}	rt->STATE_SET = false;
			return 0;
		};
		
		int Init(constructor::Document *D) {
			
			auto rt = (runtime_t *) (this->local = make_shared<runtime_t>()).get();
			
			if (!(rt->cx = JS_NewContext(8L * 1024 * 1024)))
				return 1;
			if (!JS::InitSelfHostedCode(rt->cx))
				return 1;
			
			{
				JSAutoRequest ar(rt->cx);

				JS::CompartmentOptions options;
				if(!(rt->gl = JS::PersistentRooted<JSObject*>
					(rt->cx , JS_NewGlobalObject(rt->cx, &global_class, nullptr, JS::FireOnNewGlobalHook, options))
				))
					return 1;
				
				JS_SetContextPrivate(rt->cx, this); this->D = rt->D = D;
				if (!this->Exec("'[js0] '+(Document=this)"))
					return 1;
				
				{
					JSAutoCompartment ac(rt->cx, rt->gl);
					JS_InitStandardClasses(rt->cx, rt->gl);
					
					unsigned flags = JSPROP_READONLY | JSPROP_PERMANENT;
					
					//Document
					Script::def_prop(rt, rt->gl, "manifest", D->Manifest);
					Script::def_prop(rt, rt->gl, "engine", "JS/SM52");
					//Element
					if (!JS_InitClass(rt->cx, rt->gl, NULL, &element_c, element_c_construct, 0, NULL, NULL, NULL, NULL))
						return 1;
					if (!Script::set_priv_proto(rt, "Element", nullptr))
						return 1;
					if (!JS_DefineFunction(rt->cx, rt->gl, "Event_sim", &Event_sim, 1, flags))
						return 1;
					if (!JS_InitClass(rt->cx, rt->gl, NULL, &style_c, NULL, 0, NULL, NULL, NULL, NULL))
						return 1;
					if (!JS_DefineFunction(rt->cx, rt->gl, "Classes_set", &Classes_set, 1, flags))
						return 1;
					if (!JS_DefineFunction(rt->cx, rt->gl, "Classes_del", &Classes_del, 1, flags))
						return 1;
					if(!this->Exec(Classes_Proxy_script))
						cout << "[JS!][WARNING][PROXY_SCRIPT]" <<endl;
					//Elements
					if(!this->Exec(Elements_Proxy_script))
						cout << "[JS!][WARNING][PROXY_SCRIPT]" <<endl;
					
				//	if (!JS_DefineFunction(rt->cx, rt->gl, "Elements_assign", &Elements_assign, 1, flags))
				//		return 1;
					if (!JS_DefineFunction(rt->cx, rt->gl, "Elements_set", &Elements_set, 1, flags))
						return 1;
					if (!JS_DefineFunction(rt->cx, rt->gl, "Elements_del", &Elements_del, 1, flags))
						return 1;
					//Cursor
					if (!JS_InitClass(rt->cx, rt->gl, NULL, &cursor_c, NULL, 0, NULL, NULL, NULL, NULL))
						return 1;
					
					JS::RootedObject Cursor_o(rt->cx, JS_NewObject(rt->cx, &cursor_c));
					Script::def_prop_obj(rt, rt->gl, "Cursor", Cursor_o);
					//Console
					JS::RootedObject Console_o(rt->cx, JS_NewObject(rt->cx, NULL));
					Script::def_prop_obj(rt, rt->gl, "console", Console_o);
					
					if (!JS_DefineFunction(rt->cx, Console_o, "log", &Console_log, 1, flags))
						return 1;
					//Request
					if (!JS_DefineFunction(rt->cx, rt->gl, "Request", &Request, 1, flags))
						return 1;
					//Parse
					if (!JS_DefineFunction(rt->cx, rt->gl, "Parse", &Parse, 1, flags))
						return 1;
					//Test
					if (!JS_DefineFunction(rt->cx, rt->gl, "GC", &Manual_GC, 1, flags))
						return 1;
					
					/*
					
					[*]	Document = global = root;
						
						//Document
						
					[ ]	Document(manifest);		//Document.open(manifest);
						Document(true);			//Document.open(Document.manifest);
						Document(false);		//Document.close();
						Document();
						
					[*]	new Document(manifest)	//new constructor::Document(manifest);
					[ ]	delete Document			//delete Documents[index];
						
						Document[global_var];
						
						Document.position;		//
						Document.size;			//
						Document.move();		//Document.x, Document.y
						Document.resize();		//Document.w, Document.h
						
					[ ]	Document.manifest;
					[ ]	Document.index;
						
					[ ]	Document.id;
						Document.class;
						
					[ ]	Document.assets;
					[ ]	Document.engine;
						
						Document.language;
						Document.author;
						
						Document.loadtime();
						Document.uptime();
						Document.pause();
						
						Document.save();
						Document.print();
						
						Document.history[-1];
						Document.history[ 1];
						
						//Tools
						
						Inspector();
						
					[*]	Console.log(Document.type);
						Console.warn();
						Console.error();
						
						//DOM
					[ ]		.parent
					[*]		.children
					[*]		.push
					[*]		.unshift
					[*]		.shift
					[*]		.pop
					[*]		.splice
					[?]		.getElementById
						
						//Event
					[~]		.mouseup()
					[~]		.on.mouseup = callback;
						
					[~]	Document
						.	on.mouseup = function callback( e )
							{
								e.x;
								e.y;
							}
						;
							
						
						//System
						
					[~]	Document
						.	Request
							({	URL : "http://localhost:ui.urthq.gg/Multiplayer"
							,	POST: {"param1" : true}
							,	Receive: function( e ) 		//Custom CallBack
								{
									this.Data;					//Tx = this;
									Document.Parse(this);
								}
							})
						;
						
					[~]	Document
						.	Request("http://localhost:ui.urthq.gg/Multiplayer");
					[~]	Document
						.	Receive = function callback( e ) 	//Default CallBack
							{
								this.Data;						//Tx = this;
								Document.Parse(this);
							}
						;
						
						
						
						
					[ ]	new Element(type);	-> constructor::Element(type)	//[?] - GC
					
						//DOM
					[ ]		.parent
					[*]		.children
					[*]		.push
					[*]		.unshift
					[*]		.shift
					[*]		.pop
					[*]		.splice
					[*]		.get("namespaceid")
						
					*/
				}
			}

			return 0;			
		};
		
		int Exec(string code) {
			
			auto rt = (runtime_t *) this->local.get();
		
			JSAutoRequest ar(rt->cx);
			JS::RootedValue rval(rt->cx);

			{
				JSAutoCompartment ac(rt->cx, rt->gl);

				const char *script = code.c_str();
				const char *filename = "noname";
				int lineno = 1;
				JS::CompileOptions opts(rt->cx);
				opts.setFileAndLine(filename, lineno);
				if(!JS::Evaluate(rt->cx, opts, script, strlen(script), &rval))
					return 1;
			}
			
			//JSString *str = rval.toString();
			//printf("%s\n", JS_EncodeString(rt->cx, str));
			  
			return 0;
		};
		
		int Exit() {
			
			auto rt = (runtime_t *) this->local.get();
			
			JS_DestroyContext(rt->cx);
			//delete this;	//[!] need virtual destructor
			return 0;
		};
		
	};
	
		script_engine* Constructor() {
			return new js_sm();
		};
		
		void Load() {
			JS_Init();
		//	JS_ShutDown();
		};

}

