#include "../../../../htde.h"
using namespace htde;
using std::boolalpha;
#include "rapidjson/reader.h"
using namespace rapidjson; double stodbl(const char *s){return strtoul(s,nullptr,16);};

extern "C" {

	const int WEIGHT_PARENT		=	  10;
	const int WEIGHT_TYPE		=	   1;	
	const int WEIGHT_ID			=	1000;
	const int WEIGHT_CLASS		=	  10;
	const int WEIGHT_ATTRIBUTE	=	  10;
	const int WEIGHT_ACTION		=	 100;
	
	void test_jss_rule_props(constructor::Rule &R_, std::smatch &M) {
		string type		= "*";
		string attribs 	= M[1]; //
		string actions	= M[4];	//
		for
		(	std::smatch Mx
		;	std::regex_search(attribs, Mx, std::regex(R"((,\s*)?([a-z:#@\$]+):([a-zA-Z0-9_"'\.\-\*]+))"))
		;	attribs = Mx.suffix().str()
		)	{	if(Mx[0]=="") break;
				string key = Mx[2];
				string val = Mx[3];
				if(":" == key) {		type = val;			
				}else
				if("#" == key) {		R_.classes.push_back(val);		R_.weight += WEIGHT_CLASS;
				}else
				if("@" == key) {		R_.id = val;					R_.weight += WEIGHT_ID;
				}else
				if("$" == key) {		//var							//[?] TODO
				}else {					R_.attribs.s[key] = val;		R_.weight += WEIGHT_ATTRIBUTE;
				}
			}							R_.type = type;					R_.weight += WEIGHT_TYPE;
		;
		for
		(	std::smatch MA
		;	std::regex_search(actions, MA, std::regex(R"(\.([a-z]+)\(\))"))
		;	actions = MA.suffix().str()
		)	{	if(MA[0]=="") break;
				string action = MA[1];	R_.actions.push_back(action);	R_.weight += WEIGHT_ACTION;
			}
		;
	};
	bool test_jss(constructor::Rule &R, string selector) {
		bool valid = false;
		/*
			1 - type, id, classes, attribs, vars
			4 - actions
			6 - grandparent
			7 - parent
		*/
		string RE = R"(\((((,\s*)?[a-z:#@\$]+:[a-zA-Z0-9_"'\.\-\*]+)+)\)((\.[a-z]+\(\))*)(\s+(>\s+)?)?)";
		for
		(	std::smatch M
		;	std::regex_search(selector, M, std::regex(RE))
		;	selector = M.suffix().str()
		)	{	if(M[0]=="") break; 
				if(valid) return false;
				
				bool grandparent = (M[6]!="");
				bool parent 	 = (M[7]!="");
				
				if(grandparent) {
					constructor::Rule RP;
					test_jss_rule_props(RP,M);
					RP.isDirectParent = (parent)?true:false;
				//	R.weight += WEIGHT_TYPE;
					R.weight += RP.weight;
					R.Parents.push_back(RP);
				} else {
					test_jss_rule_props(R,M);
					valid = true;
				}
			}
		;
		
		return valid;
	};
	
	
	
	void Parse(constructor::Document *D_, string code) {
		
		struct MyHandler {
			bool Null() 			{ if(RuleDepth == 1){R.properties.s[RuleProp] = "null";} return true; };
			bool Bool(bool b)		{ if(RuleDepth == 1){R.properties.b[RuleProp] =      b;} return true; };
			bool Int(int i) 		{ if(RuleDepth == 1){R.properties.f[RuleProp]=(double)i;} return true; };	//
			bool Uint(unsigned u)	{ if(RuleDepth == 1){R.properties.f[RuleProp]=(double)u;} return true; };	//
			bool Int64(int64_t i)	{ if(RuleDepth == 1){R.properties.f[RuleProp]=(double)i;} return true; };	//
			bool Uint64(uint64_t u) { if(RuleDepth == 1){R.properties.f[RuleProp]=(double)u;} return true; };	//
			bool Double(double d)	{ if(RuleDepth == 1){R.properties.f[RuleProp]=(double)d;} return true; };	//
			bool RawNumber(const char* s, SizeType length, bool copy)
									{ if(RuleDepth == 1){R.properties.s[RuleProp]=string(s);}return true; };
			bool String(const char* s, SizeType length, bool copy)	
									{ if(RuleDepth == 1){if((length>2)&&(s[0]=='0')&&(s[1]=='x'))  
														{R.properties.f[RuleProp]=stodbl(s);}else				//
														{R.properties.s[RuleProp]=string(s);}}return true; };
			bool StartObject() { 
				RuleDepth++;
				return true; 
			};
			bool Key(const char* s, SizeType length, bool copy) {
				if (RuleDepth == 0) {
					constructor::Rule Rule;	R = Rule;
				//	R.stylesheet = SS;
					R.selector = string(s);
					R.weight = 0;
				} else
				if (RuleDepth == 1) {
					RuleProp = s;
				}
				return true;
			};
			bool EndObject(SizeType memberCount) {
					RuleDepth--; 
				if (RuleDepth == 0) {
					if(test_jss(R, R.selector)) {
						D->V.Rules[R.weight].push_back(R);
						for(auto& s: R.attribs.s) {	//[1st class]
							D->V.Attrs.push_back(s.first);
						}
					} else {
					//	R = NULL;
					}
				}
				return true; 
			};
			bool StartArray() { 
				cout << "StartArray()" << endl; 
				return true; 
			};
			bool EndArray(SizeType elementCount) { 
				cout << "EndArray(" << elementCount << ")" << endl; 
				return true; 
			};
			
			constructor::Document *D;
			constructor::Rule R; int RuleDepth = -1; string RuleProp = "";
		};
		
		MyHandler handler; handler.D=D_; rapidjson::Reader reader;
		rapidjson::StringStream ss(code.c_str());
		reader.Parse(ss, handler);
		
	};
	
}