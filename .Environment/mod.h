namespace htde {
	void *handle;
	
	string target;
	string parent;	
	vector<string> parents;
/*
	void load_member(string path, string member) {
		
		if(~member.find("No such file or directory")) {
			cout << "no members here." <<endl; 
			return;
		}
		
		string id = path + "/" + member;
		handle = dlopen(id.c_str(), RTLD_NOW);
		
		node *Target = &constructor::Elements[target];
		member = member.substr(1,-4 +member.length());
		//---
		
			constructor::Member    Member; 						
		if( constructor::Member   *Existing = Target->C[member]) 
						 Member = *Existing;
	
		void *x;	

		if(x=dlsym(handle, "listener"))	Member.listener = (method)x;	//method
		if(x=dlsym(handle, "callback"))	Member.callback = (method)x;	//method
		if(x=dlsym(handle, "hook"))		Member.hook     = *(int *)x;
		
		//---
		Target->C[member] = &(constructor::Members[id] = Member);		//constructor::Members
		cout << "target = " << target << " => " << member << endl;
	}
	void load_element(string path, string element) {
		
		if(~element.find("No such file or directory")) {
			cout << "no elements here." << endl;
			return;
		}
		
		handle = dlopen((path + "/" + element).c_str(), RTLD_NOW);
		//---
		
		node Element; 
		if( parent.size() ) 
			Element = constructor::Elements[parent];
		
		Element.type = target;
		
		void *x;
		
		if(x=dlsym(handle, "description")) Element.local.s["description"] = *(string *)x;
		if(x=dlsym(handle, "constructor")) ((void(*)( node *self ))x)( &Element );
		
		//---
		constructor::Elements[target] = Element;
		cout << "new element = " << element << endl;
	}
*/
	void load_element_proto(string path, string element) {
		
		if(~element.find("No such file or directory")) {
			cout << "no elements here." <<endl; 
			return;
		}
		
		node N = constructor::Elements["element"];
		
		for(string component : System::exec("cd "+path+" && cat "+element)) {
			
			component = "./Element/" + component;
			
			if(constructor::Controllers.find(component) != constructor::Controllers.end()) {
				N.C = &constructor::Controllers[component];
				cout << "assembled component: " << component <<endl;
			}else
			if (constructor::Views.find(component) != constructor::Views.end()) {
				N.V = constructor::Views[component];
				cout << "assembled component: " << component <<endl;
			}else {
				cout << "invalid model entry, using default component" <<endl;
			}
		}
		//[textapi] TODO: apply flags in view loader, make modular from view components
		//0: NONE, 1:DEFAULT, 2:RECTANGLE, 4:TEXTBODY, 256: TEXTNODE
		//8: TITLE, 16:ITEM, 32:STRING, 64: BASIC, 128: BLOCK
			 if(element=="textnode")								{ N.V.Flags[0]=256;	N.V.Flags[1]=0;		}
		else if(element=="textbody")								{ N.V.Flags[0]=4;	N.V.Flags[1]=504;	}
		else if(~N.V.Type.find("./Element/View/text/title"))		{ N.V.Flags[0]=8;	N.V.Flags[1]=256;	}
		else if(~N.V.Type.find("./Element/View/text/item"))			{ N.V.Flags[0]=16;	N.V.Flags[1]=368;	}
		else if(~N.V.Type.find("./Element/View/text/string"))		{ N.V.Flags[0]=32;	N.V.Flags[1]=256;	}
		else if(~N.V.Type.find("./Element/View/text/basic"))		{ N.V.Flags[0]=64;	N.V.Flags[1]=368;	}
		else if(~N.V.Type.find("./Element/View/text/block"))		{ N.V.Flags[0]=128;	N.V.Flags[1]=256;	}
		else if(~N.V.Type.find("./Element/View/3D/2D/rectangle"))	{ N.V.Flags[0]=2;	N.V.Flags[1]=7;		}
		else 														{ N.V.Flags[0]=1;	N.V.Flags[1]=3;		}
		
		constructor::Elements[(N.M.Type = element)] = N;
		cout << "assembled element: " << element <<" | Flags=["<<N.V.Flags[0]<<","<<N.V.Flags[1]<<"]"<<endl;
	}
	void load_element_default() {
		
		cout << "LOAD ELEMENT (DEFAULT)" <<endl;
		
		node
			ELEMENT;
		//	ELEMENT.type = "element";
			ELEMENT.M.Type ="element";	//[!] node_proto //type
			ELEMENT.V = constructor::Views["./Element/View/3D/2D/rectangle"];
			ELEMENT.V.Flags[0]=2;
			ELEMENT.V.Flags[1]=7;
			ELEMENT.C = &(constructor::Controllers["./Element/Controller/element"]);
		constructor::Elements["element"] = ELEMENT;
	}
	void load_property_proto(string path, string property) {
		
		if(~property.find("No such file or directory")) {
			cout << "no properties here." <<endl; 
			return;
		}
		
		handle = dlopen((path + "/" + property).c_str(), RTLD_NOW);
		
		constructor::View *Target = &constructor::Views[path];
		property = property.substr(1,-4 +property.length());
		//---
		
		constructor::Property P_ = Target->Properties[property];	//[?]
		
		void *x;	
		
		if(x=dlsym(handle, "Calculate")) P_.Calculate = (vmethod)x;	//method
		if(x=dlsym(handle, "Assign"))	 P_.Assign    = (vmethod)x;	//method
		//[!]calc	//use vmethod	//load Calculate, Assign
		
		//---
		Target->Properties[property] = P_;
		cout << "target = " << target << " => " << property << endl;
	}
	void load_view_proto(string path) {
		
		constructor::View	V_ = constructor::Views[path.substr(0,-1 -target.length() + path.length())];
		constructor::Views[(V_.Type = path)] = V_;
		cout << "new view = " << target << " @ " << path << endl;
	}
	void load_member_proto(string path, string member) {
		
		if(~member.find("No such file or directory")) {
			cout << "no members here." <<endl; 
			return;
		}
		
		string id = path + "/" + member;
		handle = dlopen(id.c_str(), RTLD_NOW);
		
		constructor::Controller *Target = &constructor::Controllers[path];
		member = member.substr(1,-4 +member.length());
		//---
		
			constructor::Member  M_; 						
		if( constructor::Member	*Existing = Target->Members[member]) 
						 M_	  = *Existing;
		
		void *x;	
		
		if(x=dlsym(handle, "listener"))	M_.listener = (method)x;	//method
		if(x=dlsym(handle, "callback"))	M_.callback = (method)x;	//method
		if(x=dlsym(handle, "hook"))		M_.hook     = *(int *)x;
		
		//---
		Target->Members[member] = &(constructor::Members[id] = M_);
		cout << "target = " << target << " => " << member << endl;
	}
	void load_controller_proto(string path, string controller) {
		
		if(~controller.find("No such file or directory")) {
			cout << "no controllers here." << endl;
			return;
		}
		
		handle = dlopen((path + "/" + controller).c_str(), RTLD_NOW);
		//---
		
		constructor::Controller C_ = constructor::Controllers[path.substr(0,-1 -target.length() + path.length())];	//[?]
		
		void *x;
		
		if(x=dlsym(handle, "description")) C_.description = *(string *)x;
	//	if(x=dlsym(handle, "constructor")) ((void(*)( node *self ))x)( &Element );
		
		//---
		constructor::Controllers[(C_.Type = path)] = C_;	//[?] path
		cout << "new controller = " << controller << " @ " << path << endl;
	}
	
	void load_parser_model(string path, string parser) {
		
		if(~parser.find("No such file or directory")) {
			cout << "no parsers here." <<endl; 
			return;
		}
		
		handle = dlopen((path + "/" + parser).c_str(), RTLD_NOW);
		//---

		void *x;	

		if(x=dlsym(handle, "Parse"))	
			constructor::M::Parsers[target] = (node_p (*)( string code ))x;

		//---
		cout << "constructor::M::Parsers["<<target<<"] = "<<parser <<endl;
	}
	void load_parser_view(string path, string parser) {
		
		if(~parser.find("No such file or directory")) {
			cout << "no parsers here." <<endl; 
			return;
		}
		
		handle = dlopen((path + "/" + parser).c_str(), RTLD_NOW);
		//---
		
		void *x;
		
		if(x=dlsym(handle, "Parse"))	
			constructor::V::Parsers[target] = (vparser)x;
		
		//---
		cout << "constructor::V::Parsers["<<target<<"] = "<<parser <<endl;
	}
	
	void load_controller_engine(string path, string engine) {
		
		if(~engine.find("No such file or directory")) {
			cout << "no engines here." <<endl; 
			return;
		}
		
		string language = parents[4];
		handle = dlopen((path + "/" + engine).c_str(), RTLD_NOW);
		//---
		
		void *x;	

		if(x=dlsym(handle, "Constructor"))	
			constructor::C::Engines[language][target] = (script_engine*(*)())x;
		
		if(x=dlsym(handle, "Load"))
			((void(*)())x)();
		
		//---

		cout << "constructor::C::Engines["<<language<<"]["<<target<<"] = "<<engine <<endl;
	}
	void load_controller_xpiler(string path, string xpiler) {
		
		if(~xpiler.find("No such file or directory")) {
			cout << "no xpilers here." <<endl; 
			return;
		}
		
		cout << "[!] ~~~~~~~> " << path <<endl;
		cout << "[!] ~~~~~~~> " << parents[4] << endl;
		cout << "[!] ~~~~~~~> " << xpiler <<endl;
	}
	
	void load_host_protocol(string path, string protocol) {
		
		if(~protocol.find("No such file or directory")) {
			cout << "no protocols here." <<endl; 
			return;
		}
		
		handle = dlopen((path + "/" + protocol).c_str(), RTLD_NOW);
		//---

		Host::Protocol P;
		
		void *x;	

		if(x=dlsym(handle, "Request"))	P.Request = (Host::Transferrer)x;
		if(x=dlsym(handle, "Receive"))	P.Receive = (Host::Transferrer)x;
		
		//---
		Hosts[target] = P;
		cout << "Hosts["<<target<<"] = "<<protocol <<endl;
	}
	void load_host_portalias(string path, string portalias) {
		
		if(~portalias.find("No such file or directory")) {
			cout << "no port aliases here." <<endl; 
			return;
		}
		if(Hosts.Protocols.find(parent) == Hosts.Protocols.end()) {
			cout << "no port alias protocol ("<<parent<<")"<<endl;
			return;
		}
		
		handle = dlopen((path + "/" + portalias).c_str(), RTLD_NOW);
		//---

		Host::Port A;
		
		if(!handle) {
			cout << "looking for [optional] port alias router..." <<endl;
		} else {
			void *x;	
			if(x=dlsym(handle, "Router"))	A.Router = (Host::Transferrer)x;
			if(x=dlsym(handle, "Bridge"))	A.Routes[""] = (Host::Bridge)x;
		}
		
		//---
		Hosts[parent]["localhost:"+target] = A;
		cout << "Hosts["<<parent<<"][localhost:"<<target<<"] = "<<portalias <<endl;
	}
	void load_host_bridge(string path, string bridge) {
		
		if(~bridge.find("No such file or directory")) {
			cout << "no bridges here." <<endl; 
			return;
		}
		if(Hosts.Protocols.find(parents[2]) == Hosts.Protocols.end()) {
			cout << "no bridge protocol ("<<parents[2]<<")"<<endl;
			return;
		}
		if(Hosts[parents[2]].Ports.find("localhost:"+parents[3]) == Hosts[parents[2]].Ports.end()) {
			cout << "no bridge port alias (localhost:"<<parents[3]<<")"<<endl;
			return;
		}

		handle = dlopen((path + "/" + bridge).c_str(), RTLD_NOW);
		
		string route = target;	//2 = protocol, 3 = portalias, 4 = route
		for(int l=parents.size()-1; l>=4; l--) route = parents[l] + "/" + route;
		//---
		
		void *x;	

		if(x=dlsym(handle, "Bridge"))	
			Hosts[parents[2]]["localhost:"+parents[3]][route] = (Host::Bridge)x;
		
		//---
		cout << "Hosts["<<parents[2]<<"][localhost:"<<parents[3]<<"]["<<route<<"] = "<<bridge <<endl;
	}
	
	void load(string root, string dir) {
		
		if(~dir.find("No such file or directory")) {
			cout << "no subdirs here." << endl;
			return;
		}
		
		string path = root + "/" + dir;
		string cd = "cd " + path + " && ";
		
		htde::target = dir;		
		cout <<endl<<endl<< path <<endl<<endl;
		
		//COMPONENTS-----------------------------------
		
		if(~root.find("./.Engine")) {
			
			//load as static
			//[target].so
		}else
		
		if(~root.find("./Host")) {
			
			switch ((htde::parents = Utils::split(root, '/')).size() - 1) {
			case 1:
				//match protocol [http.so]
				for(string protocol : System::exec(cd + "ls " + htde::target + ".so"))
					load_host_protocol(path, protocol);
			break;
			case 2:
				//match port [ui.urthq.gg/]
					load_host_portalias(path, dir);
					
				//match port [ui.urthq.gg.so]
				for(string portalias : System::exec(cd + "ls " + htde::target + ".so"))
					load_host_portalias(path, portalias);
			break;
			default:
				//match bridge [Multiplayer.so]
				for(string bridge : System::exec(cd + "ls " + htde::target + ".so"))
					load_host_bridge(path, bridge);
			break;
			}
			
		}else
		
		if(~root.find("./Document/Controller/Asset")) {
		
			switch ((htde::parents = Utils::split(root, '/')).size() - 3) {
			case 3:
				if("Engine" == parent)
					for(string engine : System::exec(cd + "ls " + htde::target + ".so"))
						load_controller_engine(path, engine);
				else
				if("Transpiler" == parent)
					for(string xpiler : System::exec(cd + "ls " + htde::target + ".so"))
						load_controller_xpiler(path, xpiler);
			break;
			}
		
		}else
		
		if(~root.find("./Document/View/Asset")) {
		
			//match parser [JSS.so]
			for(string parser : System::exec(cd + "ls " + htde::target + ".so"))
				load_parser_view(path, parser);
		
		}else
		
		if(~root.find("./Document/Model/Asset")) {
		
			//match parser [XML.so]
			for(string parser : System::exec(cd + "ls " + htde::target + ".so"))
				load_parser_model(path, parser);
		
		}else
		
		if(~root.find("./Element/Controller")) {
		
			//match controller [element.so]
			for(string controller : System::exec(cd + "ls " + htde::target + ".so"))
				load_controller_proto(path, controller);
			
			//match members [.mousedn.so]
			for(string member : System::exec(cd + "ls .*.so"))
				load_member_proto(path, member);
		
		}else
		
		if(~root.find("./Element/View")) {
		
			//match view []
			load_view_proto(path);
			
			//match properties [.x.so]
			for(string property : System::exec(cd + "ls .*.so"))
				load_property_proto(path, property);
		
		}else
		
		if(~path.find("./Element/Model")) {	//[!] node_proto
		
			//match default ["element"]
			load_element_default();
		
			//match element ["button"]
			for(string element : System::exec(cd + "ls"))
				load_element_proto(path, element);
		}
		
		//RECURSE--------------------------------------
		
		//match subdirs [button/]
		for(string child : System::exec(cd + "ls -d */ 2>&1 | sed 's#/##'")) {
			htde::parent = dir;
			htde::target = child;			
			load(path, child);
		}
	};
	void init(string application) {		
		cout << "new application handle: " << application <<endl;
	//	htde::load(".",".Engine");
		
		htde::load(".","Element/Controller");
		htde::load(".","Element/View");
		htde::load(".","Element/Model");
		
		htde::load(".","Document");
		htde::load(".","Host");
		
		//System::Async::Pool();	//true
		System::Sync::Input(true);	//true
	};
	void exit(string application) {
		cout << "del application handle: " << application <<endl;
		dlclose(htde::handle);	//[!] -  close for each
		
		System::Sync::Input(false);
		//System::Async::Pool(false);
		
		//delete Documents
	}
}
