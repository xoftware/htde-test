#include "pugixml.hpp"
#include "re2/re2.h"
#include <X11/Xlib.h>
#include <X11/extensions/Xrender.h>
#include <X11/Xlib-xcb.h>
#include <xcb/xcb.h>
#include <X11/XKBlib.h>
#include <X11/Xcursor/Xcursor.h>
#include <GL/glew.h>
#include <GL/glx.h>
#include <GL/gl.h>
#include <glm/glm.hpp>
using namespace glm;
#include <glm/gtc/type_ptr.hpp>
#include "ref/gl/common/shader.hpp"
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
#include <bitset>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fstream>
#include <filesystem>
#include <queue>
namespace std{namespace fs=filesystem;} using std::bitset; using std::queue;
#include "/usr/include/magic.h"
#define STB_IMAGE_IMPLEMENTATION
#include "ref/gl/test/stb_image.h"

namespace htde {	
	namespace Utils {
		//utils
		template<typename Out>
		void split(const std::string &s, char delim, Out result) {
			std::stringstream ss(s);
			std::string item;
			while (std::getline(ss, item, delim)) {
				*(result++) = item;
			}
		};
		std::vector<std::string> split(const std::string &s, char delim) {
			std::vector<std::string> items;
			split(s, delim, std::back_inserter(items));
			return items;
		};
		void erase(vector<string>&v, string s){ auto i=std::find(v.begin(),v.end(),s); if(i!=v.end()) v.erase(i); };
		/*	//[parse]
		void uppercase(string &s) {
			std::transform(s.begin(), s.end(), s.begin(), ::toupper);
		};
		void lowercase(string &s) {
			std::transform(s.begin(), s.end(), s.begin(), ::tolower);
		};
		*/
		struct xml_string_writer: pugi::xml_writer {
			std::string result;

			virtual void write(const void* data, size_t size) {
				result.append(static_cast<const char*>(data), size);
			}
		};
		strmap parse_url_params(string in) {
			strmap PARAMS; re2::StringPiece it(in);
			RE2 RE_PARAMS(R"([&//?]([^=]+)=([^&]+))"); string KEY,VAL; 
			while (RE2::FindAndConsume(&it,RE_PARAMS,&KEY,&VAL)) PARAMS[KEY]=VAL;
			return PARAMS;
		};
		string undelimit(string in, string delimiter) {
			string out = in;
			
			if(out.length() < delimiter.length())
				return out;
			
			int seek = delimiter.length();
			if (delimiter == out.substr(0,seek))
				out = out.substr(seek);
			
			if(out.length() < delimiter.length())
				return out;
			
			/**/seek =-delimiter.length()+out.length();
		    if (delimiter == out.substr(seek, out.length()-1))
				out = out.substr(0, seek);
			
			return out;
		};
	
		namespace V {
			bool match_rule(node_p target, constructor::Rule &R_) {
				if((R_.id != "")
				&& (R_.id != target->attribs.s["id"]))	//[!] node_proto //attribs
					return false;
				
				if((R_.type != "*")
				&& (R_.type != target->M.Type))	//[!] node_proto //type => M.type
					return false;
				
				for(string class_ : R_.classes)
					if(target->classes.end() == std::find(target->classes.begin(), target->classes.end(), class_))
						return false;	//[!] node_proto //V.classes => classes
				
				for(string action : R_.actions)
					if(target->actions.end() == std::find(target->actions.begin(), target->actions.end(), action))
						return false;
				
				for(pair<string,string> s : R_.attribs.s) {
					string attr = s.first;
					string val  = s.second;
					if(val == "") {
						if(target->attribs.s[attr] == "")	//[!] node_proto //attribs
							return false;
					} else {
						if(target->attribs.s[attr] != val)//[!] node_proto //attribs
							return false;
					}
				}
				return true;
			};
			bool match_parent_rules(node_p target, constructor::Rule &R_) {
				node_p NP = target; 
				
				for(int i = R_.Parents.size() -1; i>=0; i--) { 
					constructor::Rule &RP = R_.Parents[i];
					
					if(RP.isDirectParent) {
						
						if(NP->M.parent == NULL)
							return false;	
						NP = NP->M.parent;
						
						if(!match_rule(NP, RP))
							return false;
						
					} else {
						while (true) {
							
							if(NP->M.parent == NULL)
								return false;
							NP = NP->M.parent;
							
							if(match_rule(NP, RP))
								break;
						}
					}
				}
				return true;
			};
			void getElementsByRule(node_p target, constructor::Rule &R_, vector<node_p> &Nodes) {
				
				for(auto child : target->M.children)
					getElementsByRule(child, R_, Nodes);
				
				if(!match_rule(target, R_))
					return;
				if(!match_parent_rules(target, R_))
					return;
				
				Nodes.push_back(target);
			};
			bool supports(node_p target, string property) {
				if(target->V.Properties.end() == target->V.Properties.find(property))
					return false;
				if(target->V[property].Inline==true)	//[!]calc
					return false;
				return true;
			};
			int max3(int &a, int &b, int  c) { if(a>b) return (a>c)?a:c; return (b>c)?b:c; };
			int min2(int  a, int  b) { return (a<b)?a:b; };
			thread_local magic_t gIR_H;
			thread_local string	 gIR_RE = R"(([0-9]{1,4}) ?x ?([0-9]{1,4}))";
			thread_local smatch  gIR_M;
			bool getImageResolution(string file, int &w, int &h, int &l) {	printf("file: %s\n", file.c_str());
				auto info=string(magic_file(gIR_H, file.c_str()));			printf("info: %s\n", info.c_str());
				if (regex_search(info, gIR_M, regex(gIR_RE))) {
					w=stoi(gIR_M[1]);							//raw res
					h=stoi(gIR_M[2]); 							//raw res
					l=max3(w, h, 128);							//min res
					l=min2(1<<(32-__builtin_clz(l-1)), 2048);	//max res
					return true;
				}else{ 
					w=h=l=0; return false; 
				}
			};
			glm::vec3 ray_origin;
			glm::vec3 ray_direction;
				float ray_distance;
			void Generate_Ray_Mouse
			(	float 	W,	float	H,	int		X,	int		Y	//DOCUMENT
			) {												Y = H - Y;

				glm::mat4 I = glm::inverse(
					glm::perspective(glm::radians(90.0f), W/H, 0.01f, 200.0f)
				*	glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f * 200.0f))
				);
				glm::vec4 A(
					((float)X/(float)W - 0.5f) * 2.0f, 
					((float)Y/(float)H - 0.5f) * 2.0f,
					-1.0,
					1.0f
				);
				glm::vec4 B(A[0],A[1],0.0,1.0f);
				
				A=I*A; A/=A.w;	ray_origin = glm::vec3(A); ray_distance = 7777777.0f;
				B=I*B; B/=B.w;	ray_direction = glm::normalize(glm::vec3(B-A));
					
			//	cout << "ray_origin = ["<<ray_origin[0]<<","<<ray_origin[1]<<","<<ray_origin[2]<<"]";
			//	cout << "ray_direction = ["<<ray_direction[0]<<","<<ray_direction[1]<<","<<ray_direction[2]<<"]";
			};
			bool Intersect_Ray_OBB
			(	float	w,	float	h,	float	l,	float	x,	float	y,	float	z	//ELEMENT
			) {
				glm::vec3 aabb_min(w/-2.0f, h/-2.0f,l/-2.0f);	//w/-2, h/-2, l/-2
				glm::vec3 aabb_max(w/ 2.0f, h/ 2.0f,l/ 2.0f);	//w/2,  h/2,  l/2
				glm::mat4 ModelMatrix = 
					glm::translate(glm::mat4(1.0f), glm::vec3(x,y,z))
			//	*	glm::mat4_cast(glm::quat(glm::vec3(pitch, yaw, roll)))
				;
				
			//	cout << "Matrix[3] = ["<<ModelMatrix[3][0]<<","<<ModelMatrix[3][1]<<","<<ModelMatrix[3][2]<<"]";
			//	cout << "Matrix[0] = ["<<ModelMatrix[0][0]<<","<<ModelMatrix[0][1]<<","<<ModelMatrix[0][2]<<"]";
			//	cout << "Matrix[1] = ["<<ModelMatrix[1][0]<<","<<ModelMatrix[1][1]<<","<<ModelMatrix[1][2]<<"]";
			//	cout << "Matrix[2] = ["<<ModelMatrix[2][0]<<","<<ModelMatrix[2][1]<<","<<ModelMatrix[2][2]<<"]";
				
				float tMin = 0.0f;
				float tMax = 100000.0f;
				glm::vec3 delta = glm::vec3(ModelMatrix[3].x, ModelMatrix[3].y, ModelMatrix[3].z) - ray_origin;

				for(int i=0; i<3; i++)
				{
					glm::vec3 axis(ModelMatrix[i].x, ModelMatrix[i].y, ModelMatrix[i].z);
					float e = glm::dot(axis, delta);
					float f = glm::dot(ray_direction, axis);
					
					if ( fabs(f) > 0.001f ) {
						float t1 = (e+aabb_min[i])/f;
						float t2 = (e+aabb_max[i])/f;
						if (t1>t2){	float w=t1;t1=t2;t2=w; }
						if (t2 < tMax) tMax = t2;
						if (t1 > tMin) tMin = t1;
						if (tMax < tMin)
							return false;
					}else{
						if(-e+aabb_min[i] > 0.0f || -e+aabb_max[i] < 0.0f)
							return false;
					}
				}
				
				if (tMin <=ray_distance) {
					ray_distance = tMin;
				//	cout << "[RAY OBB] found element: true | " << tMin <<endl;
					return true;
				}else {
				//	cout << "[RAY OBB] found element: overridden | " << tMin <<endl;
					return false;
				}
			};
			node_p getElementByMouse(System::Window *W, int X, int Y) {	//[ray] do this in input thread
			
				Generate_Ray_Mouse(W->width,W->height, X,Y);
				
				node_p N = W->D->M.Root;
				queue<node_p> nodes; 
				nodes.push(N);
					
				while(!nodes.empty()) {		
					auto target = nodes.front();
								  nodes.pop();
					
					if(Intersect_Ray_OBB
					(	std::get<2>(target->V["width"].Assignment)
					,	std::get<2>(target->V["height"].Assignment)
					,	std::get<2>(target->V["length"].Assignment)
					,	std::get<2>(target->V["x"].Calculation)
					,	std::get<2>(target->V["y"].Calculation)
					,	std::get<2>(target->V["z"].Calculation)
					))	{
						N = target;
					//	cout <<"[getElementByMouse]: match: "<<N->attribs.s["id"]<<endl;
					}
					
					for(auto child: target->M.children) {
						if ("textbody"==child->M.Type)	//[textapi] use V.Flags
							continue;
						if ("disabled"==child->attribs.s["mousemovemode"])	//[modifiers]	//todo: support bool/int
							continue;
						nodes.push(child);
					}
				}
				
			//	cout <<"[getElementByMouse]: return: "<<N->attribs.s["id"]<<endl;
				return N;
			};
			struct getEntityByMouse__cursor_t {
				int i;		//character index
			//	float pitch;
			//	float yaw;
			//	float roll;
			}	   getEntityByMouse__cursor;
			node_p getEntityByMouse__recurse(System::Window::Text *B, node_p target) {
				if("textnode" == target->M.Type) {	//[textapi] use V.Flags
				
					auto &_ = getEntityByMouse__cursor;
					auto &value = target->attribs.s["value"];
					float size; for(auto temp=target;;temp=temp->M.parent){if(temp->V["fontsize"].Assignment.index()==2){size=std::get<2>(temp->V["fontsize"].Assignment);break;}}
					
					for(int i=0; i<value.length(); i++) {
						auto&C = B->Characters[++_.i];
						
						if(Intersect_Ray_OBB
						(	size	//width
						,	size	//height
						,	1.0f	//length
						,	C.model[3].x
						,	C.model[3].y
						,	C.model[3].z
						))	{
							return target;
						}
					}
					return NULL;
				}
				
				for(auto child : target->M.children) {
					auto found = getEntityByMouse__recurse(B, child);
					if ( found!= NULL )
						return found;
				}
				return NULL;
			};
			int getEntityByMouse(System::Window *W, node_p &target) { //[ray] returns texttype/char index
				
				//ray distance?
				//return: has textbody, textnode is hovered, character is hovered, => which textnode, which character index
				
				//char array: Texts[textbody->txtid]
				//char index: textnode->value
				
				//rotation: textbody roll,pitch,yaw
				//location: character model[3].x,y,z
				//scale:	texttype fontsize
				
				
				auto &_ = getEntityByMouse__cursor;
				_.i =-1;
			//	_.pitch
			//	_.yaw
			//	_.roll
				
				for(auto child : target->M.children) { if(child->M.Type=="textbody") {	//[textapi] use V.Flags
					auto found = getEntityByMouse__recurse(W->Texts[child->attribs.i["txtid"]], child);
					if ( found!= NULL ) {
						target=found;
						return _.i;	
					}
					return -1;	
				}}
				return -1;
			};
		}
	}
}
