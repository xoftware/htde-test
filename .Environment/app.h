namespace htde {
	namespace constructor {
		void Document_test( node_p target ) {
			for(auto& child : target->M.children) {
				cout << child->M.Type<<".id="<<child->attribs.s["id"];	//[!] node_proto //attribs
				cout <<" children= "<< child->M.children.size();
				cout <<" .value= "  << child->attribs.s["value"] << endl;	//[!] node_proto //attribs
				cout <<"PARENT.id = "<< child->M.parent->attribs.s["id"] <<endl<<endl;	//[!] node_proto //attribs
				
				Document_test( child );
			}
		};
	/*	
		void Document__draw( Document *D ) {	//[fps]
			auto W = D->V.Window;
			auto id= W->id;
			
			while(true) {	//[!]
				std::this_thread::sleep_for(std::chrono::milliseconds(16));
				
				if(!System::Sync::Dispatchable(id)) return; std::lock_guard<std::mutex> lock(*D->C.mutex);
				if(!System::Sync::Dispatchable(id)) return;
					
					constructor::Event Ev;	Ev.Frame = true;
					
				( D->C.Queue).push_back( Ev );
				(*D->C.ready).notify_one();
			}
		};
	*/
		void Document__init( Document *D  ) {
			
			htde::Thread.D = D; 
			cout <<(D->C.index = std::this_thread::get_id()) << endl;
			cout << D->Manifest << endl;
			//----------------------------------------------------------------------
		//	this->M.Root->attribs.s["id"]="root";	//[cursor]
			//----------------------------------------------------------------------
			D->V.Window->Init();
		//	D->V.Window->Font_Init("Host/share/font/mono/UbuntuMono.sdf");
		//	D->V.Window->Font_Init("Host/share/font/sans/Rendezvous.sdf");
			D->V.Window->Font_Init("Host/share/font/serif/Rendezvous.sdf");
			D->V.Window->Cursor.file="Host/share/cursor/htde/16/right_ptr";	//[cursor]
			D->V.Window->Cursor.Draw(1);
			//======================================================================
			D->Home="Host/http/ui.urthq.gg";
			D->Host="http://localhost:ui.urthq.gg";
			D->Parse(Hosts.Request(D->Manifest)->Data);
			//======================================================================
			Document_test( D->M.Root );
			//----------------------------------------------------------------------
		//	D->V.Window->Image_Init("Host/share/image/background/17.png");
		//	D->V.Window->Image_Init(".Environment/ref/gl/test/image/test.png");
		//	D->V.Window->Image_Init(".Environment/ref/gl/test/image/test.jpg");
		//	D->V.Window->Image_Init(".Environment/ref/gl/test/image/test.gif");
		//	D->V.Window->Image_Init(".Environment/ref/gl/test/image/test.bmp");
			new System::Window::Mesh(D->V.Window, "./Element/View/3D/2D/rectangle");
			D->V.Window->Draw();
		//	std::thread(Document__draw, D).detach();	//[fps]
			//----------------------------------------------------------------------
			D->C.Engine->Exec("console.log('[js0] hello world, it is '+new Date());");
			D->C.Engine->Exec("console.log('[js0] '+this.type);");
			D->C.Engine->Exec("console.log('[js0] '+this.children);");
			//----------------------------------------------------------------------
			
			while (true) {
				std::unique_lock<std::mutex> lock(*D->C.mutex);
				(*D->C.ready).wait(lock);
				
				while(D->C.Queue.size()) {
					constructor::Event Ev = D->C.Queue.front();		//[ ] Event	.Element .Request
											D->C.Queue.pop_front();

					if(Ev.Request != NULL) {
						D->C.Engine->Receive(Ev.Request);
					} else
					if(Ev.Element.s["type"] != "") {
						D->C.Chain(Ev.Element);
					}
				/*
					else 
					if(Ev.Frame) {	//[fps]
						if (D->V.Window->DrawFlag == true) {
							D->V.Window->DrawFlag = false;
							D->V.Window->Draw();
						}
					}
				*/
					//-------------------------
					if (D->Manifest == "") {
						delete D;
						return;
					}
					//-------------------------
				}
				
				if (D->V.Window->DrawFlag == true) {
					D->V.Window->DrawFlag = false;
					D->V.Window->Draw();
				}
			
			}
			
		};
		Document::Document( string Manifest ) {	
			Documents.push_back(this);
			this->Manifest = Manifest;
			
			this->M.D = 
			this->V.D = 
			this->C.D = this;
			
			this->M.Root = Element("root");
			
			this->V.Window = new System::Window(this);
			
			this->C.mutex = new std::mutex();
			this->C.ready = new std::condition_variable();
			this->C.Thread = std::thread(Document__init, this);
			this->C.Thread.detach();
		};
		Document::~Document() {
			
//			cout<<"[HTDE][DOCUMENT][DELETE] !!!!!!!"<<endl;		//assign Documents[me] = NULL
			
			//[?] for each node: this->C.Engine->Free(node);	//tempfix: JS GC bug
			
			//delete Window
			this->V.Window->Exit();	//[ ] - 
			delete this->V.Window;
			
			//close mutex/ready
			this->C.Engine->Exit();	//[!] - GC Elements BackEnd/FrontEnd
			delete this->C.Engine;	//[!] - delete in Exit, need virtual destructor
		};
		
		void Document::Parse( string code ) {
			
			int i = 0;
			string x;
			
			pugi::xml_document manifest;
			if(manifest.load_string(code.c_str())) {
				for (pugi::xml_node component : manifest.children()) {
					
					if(string("M") == component.name()) {
						for (pugi::xml_node asset : component.children()) {
							
							if((x=asset.attribute("file").value()) != "")
								this->M.Assets[x];
							
							if((x=asset.attribute("type").value()) != "") {
								Utils::xml_string_writer writer;
								for (pugi::xml_node child : asset.children()) child.print(writer);
								this->M.Assets[to_string(++i) +"."+ x] = writer.result;
							}
						}
					} else
						
					if(string("V") == component.name()) {
						for (pugi::xml_node asset : component.children()) {
							
							if((x=asset.attribute("file").value()) != "")
								this->V.Assets[x];
							
							if((x=asset.attribute("type").value()) != "")	//[?] - escape
								this->V.Assets[to_string(++i) +"."+ x] = asset.text().get();
							
						}
					} else
						
					if(string("C") == component.name()) {
						for (pugi::xml_node asset : component.children()) {
							
							if((x=asset.attribute("file").value()) != "")
								this->C.Assets[x];
							
							if((x=asset.attribute("type").value()) != "")	//[?] - escape
								this->C.Assets[to_string(++i) +"."+ x] = asset.text().get();
							
						}
						
						this->C.Engine = constructor::C::Engine("JS/SM");
						this->C.Engine->Init(this);
						//[!] - Default (Global Scope API)
					}
					
				}
			}
			
			Host::Transfer Tx = make_shared<Host::Transfer_Object>();
			for(auto& Asset : this->M.Assets) {
//				cout << "[M] =======> " << Asset.first <<endl;
				
				if(Asset.second == "") {
					Tx->URL = Asset.first;	Hosts.Request(Tx);
					Asset.second = Tx->Data;
				}else {
//					cout << Asset.second <<endl;
				}
			//	auto model = this->M.Parse(Asset.first, Asset.second);	//[parse]
				auto model = this->M.Parse(Asset.second);
				for (auto el : model->M.children)
					(el->M.parent = this->M.Root)->M.children.push_back(el);
			}
			
			for(auto& Asset : this->V.Assets) {
//				cout << "[V] =======> " << Asset.first <<endl;
				
				if(Asset.second == "") {
					Tx->URL = Asset.first; Hosts.Request(Tx);
					Asset.second = Tx->Data;
				}else {
//					cout << Asset.second <<endl;
				}
				
				this->V.Parse(Asset.second);
			}
			this->C.Engine->Root(this->M.Root);		//[prop]	inlines
			this->V.Assign(this->M.Root);			//[prop]	rules
			for(auto& Asset : this->C.Assets) {
//				cout << "[C] =======> " << Asset.first <<endl;
//				cout << "[C] =======> " << Asset.second <<endl;
				
				if(Asset.second == "") {
					Tx->URL = Asset.first;	Hosts.Request(Tx);
					Asset.second = Tx->Data;
				}
				
				this->C.Engine->Exec(Asset.second);
			}
			
		};
		void Document::Controller::Parse(string code) {
		
			//Transpile
			//Compile
			//return script
		
		};
		void Document::View::Parse(string code) {
			
			if( V::Parsers["JSS"] ) {
				V::Parsers["JSS"](D, code);
			}
			
		};
		/*	//[parse]
		node_p Document::Model::Parse(string file, string code) {
			
			node_p model;
			string type = Utils::split(file,'.').back(); 
						  Utils::uppercase(type);
			
			if( M::Parsers.find(type) != M::Parsers.end()) {
				model = M::Parsers[type](code);
			} else {
				model = Element("model");
			}
			
			return model;
		};	
		*/
		node_p Document::Model::Parse(string code) {
			
			node_p model;
			
			if( M::Parsers["XML"] ) {
				model = M::Parsers["XML"](code);
			} else {
				model = Element("model");
			}
			
			return model;
		};	
		void Document::Controller::Chain(data_generic &e) {
			auto D = this->D;
			auto &source = e.n["source"];
			auto &target = e.n["target"];
			
			if(e.s["type"] == "mouseoutro") { for(auto temp=target;temp!=NULL;temp=temp->M.parent){Utils::erase(temp->actions,"onmousemove");}
				D->V.Assign(D->M.Root);
			}else 
			if(e.s["type"] == "mouseintro") { for(auto temp=target;temp!=NULL;temp=temp->M.parent){temp->actions.push_back("onmousemove");}
				D->V.Assign(D->M.Root);
				D->V.Window->DrawFlag = true;	//todo: fps cap?
			}
		
			//propagate depth = 0 (primitive)
			for(//[!] todo: ignore nodes not in DOM
			;	target!=NULL
			;	target=target->M.parent
			)	{
				
				//Listener()
				D->C.Engine->Dispatch(target, e);
				//Callback()	//cancel flag, bubble flag
//				cout <<"[CHAIN] cancel = "<<e.i["cancel"]<<endl;
//				cout <<"[CHAIN] bubble = "<<e.i["bubble"]<<endl;
				
				if(!e.i["bubble"])
					break;
			}
			
			D->C.State = e;	//history
		
			//[chain]	propagate primitive only -> then trace hooks by depth
			//			propagate flag disables only event type on depth
			//			
			//
		
		
			//vector<node*>Elements;
			//-- from target
			/*
			string Level[10];
			Level[0] = EventObject.s["type"];
			
			bool b;
			for(int i =0; !b; i++) {
					
				cout <<endl<<endl<< "Level " << i <<endl<<endl;
				b = 1;
				
				for(auto& Element : Elements) {
					
					for(int j=1; j<=i; j++) {
						if(Element.C[Level[j-1]] != NULL)
							Level[j] = Element.C[Level[j-1]]->chain;
					}
					
					if(Element.C[Level[i]] != NULL){    
						b = 0;
						
						EventObject.n["current"]    = &Element;
						EventObject.s["type"]       = Level[i];
						EventObject.i["propagation"]= 1;
						EventObject.i["cancel"]     = 0;
						
						//pass to manager
						Element.C[Level[i]]->callback(&Element);
					}
				}
			}
			*/
		};
		void Document::View::Assign(node_p target) {
			for(auto& Rules_ : D->V.Rules) {
				for(int order = 0
					; 	order < Rules_.second.size()
					;	order++
				)	
				{	int weight = Rules_.first;
					Rule &rule = Rules_.second[order];
					
					vector<node_p> Nodes; Utils::V::getElementsByRule(target, rule, Nodes);
					
					for(auto Node : Nodes) {	//[!] node_proto 
						for(auto& b:rule.properties.b){ if(Utils::V::supports(Node,b.first)){ Node->V[b.first].Assignment=b.second; D->C.Engine->Prop(Node,b.first,1); }}	//[prop]
						for(auto& f:rule.properties.f){ if(Utils::V::supports(Node,f.first)){ Node->V[f.first].Assignment=f.second; D->C.Engine->Prop(Node,f.first,1); }}	//
						for(auto& s:rule.properties.s){ if(Utils::V::supports(Node,s.first)){ Node->V[s.first].Assignment=s.second; D->C.Engine->Prop(Node,s.first,1); }}
					/*	
						//[?] JSS - rule order/weight should be ok
						//[prop] - todo: reverse loop
					*/
					}
				}
			}
		};
	}
}
