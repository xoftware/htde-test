namespace htde {
	namespace System {
		vector<string> exec(string cmd) {
			vector<string> data;
			FILE * stream;
			char buffer[256];
			cmd.append(" 2>&1");
			if (stream = popen(cmd.c_str(), "r")) {
				while (fgets(buffer, sizeof(buffer)-1, stream) != NULL) 
					data.push_back(std::strtok(buffer,"\n"));	
				pclose(stream);
			}
			return data;
		};
		
		int GL_MUBS = 65536;	//GL_MAX_UNIFORM_BLOCK_SIZE		//64 KB	
		int GL_MVUB = 14;		//GL_MAX_VERTEX_UNIFORM_BLOCKS	//14
		
		static int GL_Context_Attrs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
			GLX_CONTEXT_MINOR_VERSION_ARB, 3,
			None
		};
		static int GL_Visual_Attrs[] = {
			GLX_X_RENDERABLE, True,
			GLX_DOUBLEBUFFER, True,
			GLX_RENDER_TYPE, GLX_RGBA_BIT,
			GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
			GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
			GLX_RED_SIZE, 8,
			GLX_GREEN_SIZE, 8,
			GLX_BLUE_SIZE, 8,
			GLX_ALPHA_SIZE, 8,
			GLX_DEPTH_SIZE, 24,
			None
		};					  std::mutex WM_Mutex;
		Display 						*WM_Display;	//[gl]
		xcb_connection_t 				*WM_Connection;
		xcb_screen_t 					*WM_Screen = 0;
		unordered_map
			<uint32_t, System::Window*>  Windows;
		
		System::Window
			  ::Window(constructor::Document *D) {
			this->D = D;
			this->Cursor.W = this;
		};
		void 
		System::Window::Cursor_t::Draw(int mode) { auto X = WM_Display;	//[cursor]
			if(mode==0){ Draw(1); return; }	//INIT
			if(mode& 1){ 
				if(!std::fs::exists(file)) {
				if(!std::fs::exists(file="Host/share/cursor/"+file)) {
				//	return;
				}}
				XDefineCursor(X, W->id, XcursorFilenameLoadCursor(X, file.c_str())); 
			}	//FILE
			if(mode& 2){}	//SIZE
			if(mode& 4){}	//SPOT	//wrap to size
			if(mode& 8){ XWarpPointer(X, W->id,W->id, 0,0,0,0, (int)(x+W->width/2),(int)(W->height/2-y)); }	//WARP
		};
		System::Window
			  ::Mesh
			  ::Mesh(Window *W, string file) {
				  
			this->W = W;
			this->file = file;
			W->Meshes.push_back(this);
			
//			cout << "new Mesh: this->file == " << this->file <<endl;	//global handles?	//view meta
			//----------------------------------------------------------------------------------
			
			static const GLfloat VBO_data[] = {		//load .obj -> vertices
				 1.0f,  1.0f, 0.0f,	//top right
				 1.0f, -1.0f, 0.0f,	//bot right
				-1.0f, -1.0f, 0.0f,	//bot left
				-1.0f,  1.0f, 0.0f,	//top left
			};			   this->VBO_size = 12;
			
			static const GLuint EBO_data[] = {		//load .obj -> indices
				0, 1, 3,   // first triangle	//	top right, bot right, top left
				1, 2, 3    // second triangle	//	bot right, bot left, top left
			};			  this->EBO_size = 6;
			
			glGenVertexArrays		(1, &this->VAO);
			glGenBuffers			(1, &this->VBO);
			glGenBuffers			(1, &this->EBO);
			//	
			this->SP =	 LoadShaders("Element/View/3D/template.glvs", "Element/View/3D/template.glfs");
			//----------------------------------------------------------------------------------
			Property 
				MATRIX;
				MATRIX.type=5;
				MATRIX.size=sizeof(glm::mat4);
			//	MATRIX.rpad=sizeof(GLfloat)*0;
				MATRIX.name="matrix";
			Properties.push_back(MATRIX);
			
			Property
				COLOR;
				COLOR.type=4;
				COLOR.size=sizeof(glm::vec4);
			//	COLOR.rpad=sizeof(GLfloat)*0;
				COLOR.name="color";
			Properties.push_back(COLOR);
			
			Property
				IMAGE;
				IMAGE.type=1;
				IMAGE.size=4;
			//	IMAGE.rpad=sizeof(GLint)*3;
				IMAGE.name="image";
			Properties.push_back(IMAGE);
			
			//----------------------------------------------------------------------------------
			glBindVertexArray(this->VAO);
			 
				glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
				glBufferData(GL_ARRAY_BUFFER, sizeof(VBO_data), VBO_data, GL_STATIC_DRAW);
				
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(EBO_data), EBO_data, GL_STATIC_DRAW);
				
				glEnableVertexAttribArray(0);	//layout (location = 0) in vec3 position;
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);
			
			glBindVertexArray(0);
			//----------------------------------------------------------------------------------
			glUseProgram(this->SP);
				
				glUniformMatrix4fv(glGetUniformLocation(this->SP, "viewprojection"), 1, GL_FALSE, glm::value_ptr(W->viewprojection));
				glUniformBlockBinding(this->SP, glGetUniformBlockIndex(this->SP, "block0"), 0);
				glUniformBlockBinding(this->SP, glGetUniformBlockIndex(this->SP, "block1"), 1);	
				
			glUseProgram(0);
		};
		void System::Window::Mesh::Data(node_p &target) {
			
		//	if(target->V.Type == this->type)
			
//				cout << "found element: " << target->V.Type <<endl;
		
				for(auto &property : Properties) { //
				
//					cout << "found property: " << property.type << " " << property.size << " " << property.name <<endl;
				
					//check if property exists
					//check property type (for variant)
				
					if(property.type == 1) {	//1 = int
						
//						cout << "trying memcpy, bytecount = " << UBO_bytecount <<endl;
						
						memcpy
						(	UBO_bytes+UBO_bytecount
						,	(const char*)&std::get<1>( target->V[property.name].Assignment )	//Calculation
						,	property.size	//size from type	//vec4 = 16
						);	UBO_bytecount+=property.size+12;	//pad from type	+property.rpad
						
//						cout << "done memcpy, bytecount = " << UBO_bytecount <<endl;
					}else
				
					if(property.type == 4) {	//4 = vec4
						
//						cout << "trying memcpy, bytecount = " << UBO_bytecount <<endl;
						
						memcpy
						(	UBO_bytes+UBO_bytecount
						,	(const char*)&std::get<4>( target->V[property.name].Assignment )	//Calculation
						,	property.size	//size from type	//vec4 = 16
						);	UBO_bytecount+=property.size;		//pad from type	+property.rpad
						
//						cout << "done memcpy, bytecount = " << UBO_bytecount <<endl;
					}else
					
					if(property.type == 5) {	//5 = mat4
						
//						cout << "trying memcpy, bytecount = " << UBO_bytecount <<endl;
						
						memcpy
						(	UBO_bytes+UBO_bytecount
						,	(const char*)&std::get<5>( target->V[property.name].Calculation )	//Calculation
						,	property.size	//size from type	//mat4 = 64
						);	UBO_bytecount+=property.size;		//pad from type	+property.rpad
						
//						cout << "done memcpy, bytecount = " << UBO_bytecount <<endl;
					}
				}
				UBO_instancecount++;
				
//				cout << "done element: " << UBO_instancecount <<endl;
			
//			for(auto &child: target->M.children)
//				Data( child);	//recurse
		};
		void System::Window::Mesh::Draw() {
			
			glBindBuffer(GL_UNIFORM_BUFFER, this->W->instancesblock);
			glBufferSubData(GL_UNIFORM_BUFFER, 0, this->UBO_bytecount, this->UBO_bytes);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
			
			glUseProgram(this->SP);
			
			glBindVertexArray(this->VAO);
			glDrawElementsInstanced(GL_TRIANGLES, this->EBO_size, GL_UNSIGNED_INT, 0, this->UBO_instancecount);
			glBindVertexArray(0);
		};
		
		void System::Window::Init() {
			auto W = this; 
			auto C = WM_Connection;
			auto X = WM_Display;
			//---------------------
			std::lock_guard<std::mutex> guard(WM_Mutex);
			//================================================ VISUAL ==============
			XVisualInfo       *vi;
			XRenderPictFormat *pf;
			int			 fbcs_size;
			GLXFBConfig  fbc; 	
			GLXFBConfig *fbcs = glXChooseFBConfig(X, DefaultScreen(X), GL_Visual_Attrs, &fbcs_size);
			for(int i=0; i<fbcs_size; i++) {
				vi = (XVisualInfo*) glXGetVisualFromFBConfig(X, fbcs[i]);
				if(!vi)
					continue;
				pf = XRenderFindVisualFormat(X, vi->visual);
				if(!pf)
					continue;
				if(pf->direct.alphaMask > 0) {
					fbc = fbcs[i];
					break;
				}
			}
			//================================================ CONTEXT =============
			GLXContext ctx_old = 		   glXCreateNewContext(X, fbc, GLX_RGBA_TYPE, 0, True);  if(!ctx_old) {fprintf(stderr, "glXCreateNewContext failed\n"); return; }
			glXCreateContextAttribsARBProc glXCreateContextAttribsARB = 0;
			glXCreateContextAttribsARB =  (glXCreateContextAttribsARBProc) glXGetProcAddress((const GLubyte*)"glXCreateContextAttribsARB");
			glXMakeCurrent(X, 0, 0);	   glXDestroyContext(X, ctx_old); if (!glXCreateContextAttribsARB) {printf("glXCreateContextAttribsARB() not found\n");return;}
			W->ctx = (GLXContext)		   glXCreateContextAttribsARB(X, fbc, NULL, true, GL_Context_Attrs); if (!(GLXContext)W->ctx) {printf("Failed to create OpenGL context. Exiting.\n"); return;}
			//================================================ WINDOW ==============
			XSetWindowAttributes 
				attr;
				attr.colormap = XCreateColormap(X, DefaultRootWindow(X), vi->visual, AllocNone);
				attr.border_pixel=0; attr.background_pixel=0;
				attr.event_mask =
					StructureNotifyMask |	EnterWindowMask |	LeaveWindowMask   |	PointerMotionMask |
					ExposureMask 		|	ButtonPressMask |	ButtonReleaseMask |
					OwnerGrabButtonMask |	KeyPressMask 	|	KeyReleaseMask;
			W->id = 
				XCreateWindow
				(	X, DefaultRootWindow(X), 
					W->x, W->y, 		//x,y
					W->width, W->height,//w,h
					0, vi->depth, InputOutput, vi->visual, 
					CWColormap | CWBorderPixel | CWBackPixel | CWEventMask, &attr
				)
			;	Windows[W->id] = W;	
			xcb_map_window(C, W->id); 
			W->gl = glXCreateWindow(X, fbc, W->id, 0);
			//================================================ VERSION =============
			if(!W->id)															{fprintf(stderr, "glXDestroyContext failed\n"	 ); xcb_destroy_window(C, W->id); glXDestroyContext(X, (GLXContext)W->ctx); return; }
			if(!glXMakeContextCurrent(X, W->gl, W->gl, (GLXContext)W->ctx))		{fprintf(stderr, "glXMakeContextCurrent failed\n"); xcb_destroy_window(C, W->id); glXDestroyContext(X, (GLXContext)W->ctx); return; }
			int major = 0, minor = 0;
			glGetIntegerv(GL_MAJOR_VERSION, &major);
			glGetIntegerv(GL_MINOR_VERSION, &minor);							  printf("OpenGL context created.\nVersion %d.%d\nVendor %s\nRenderer %s\n", major,minor, glGetString(GL_VENDOR),glGetString(GL_RENDERER));
			
			int mubb=0,mubs=0,mvub=0,mfub=0;
			glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS,	&mubb);				  printf("GL_MAX_UNIFORM_BUFFER_BINDINGS = %d\n", 	mubb);
			glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, 		&mubs);				  printf("GL_MAX_UNIFORM_BLOCK_SIZE = %d\n", 		mubs);
			glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS,		&mvub);				  printf("GL_MAX_VERTEX_UNIFORM_BLOCKS = %d\n", 	mvub);
			glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS,	&mfub);				  printf("GL_MAX_FRAGMENT_UNIFORM_BLOCKS = %d\n", 	mfub);
			
			int mvuc=0,mfuc=0;
			glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS,	&mvuc);				  printf("GL_MAX_VERTEX_UNIFORM_COMPONENTS = %d\n", mvuc);
			glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,&mfuc);			  printf("GL_MAX_FRAGMENT_UNIFORM_COMPONENTS = %d\n",mfuc);
			
		//	GL_MUBS = mubs;
			
			glewExperimental = true; if (glewInit() != GLEW_OK)					{fprintf(stderr, "Failed to initialize GLEW\n");	return;}
			
			const char* extensions = glXQueryExtensionsString(X,DefaultScreen(X));
			printf("EXTENSIONS = \n\n%s\n\n", extensions);
			
		/*	
			const char* extensions = glXQueryExtensionsString(X,DefaultScreen(X));
			typedef int (*glXSIM)(unsigned int); glXSIM glXSwapIntervalMESA = NULL;
			typedef int (*glXGSIM)(void); glXGSIM glXGetSwapIntervalMESA = NULL;
			if (strstr(extensions,"MESA_swap_control")) {
				glXSwapIntervalMESA = (glXSIM)glXGetProcAddress((const GLubyte*)"glXSwapIntervalMESA");
				glXGetSwapIntervalMESA = (glXGSIM)glXGetProcAddress((const GLubyte*)"glXGetSwapIntervalMESA");
			}
			if (glXSwapIntervalMESA) {
				printf("ATTEMPTING SWAP INTERVAL = %d\n",glXSwapIntervalMESA(1));
				printf("SWAP INTERVAL = %d\n", glXGetSwapIntervalMESA());
			}
		*/	
		/*
			const char* extensions = glXQueryExtensionsString(X,DefaultScreen(X));
			typedef int (*glXSIS)(unsigned int); glXSIS glXSwapIntervalSGI = NULL;
			if (strstr(extensions,"MESA_swap_control")) {
				glXSwapIntervalSGI = (glXSIS)glXGetProcAddress((const GLubyte*)"glXSwapIntervalSGI");
			}
			if (glXSwapIntervalSGI) {
				printf("ATTEMPTING SWAP INTERVAL = %d\n",glXSwapIntervalSGI(1));
			}
		*/
			//================================================ RENDER ==============
			glClearColor(0.0, 0.0, 0.0, 0.0);
			//----------------------------------------------------------------------------------
			float aspect = width/height;
			
			W->viewprojection =
		//		glm::ortho(width/-2, width/2, height/-2, height/2, -1.0f, 1.0f)
				glm::perspective(glm::radians(90.0f), W->width / W->height, 100.0f, -100.0f)
			*	glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -200.0f))
			;
				glGenBuffers(1, &W->instancesblock);
				glBindBuffer(GL_UNIFORM_BUFFER, W->instancesblock);
				glBufferData(GL_UNIFORM_BUFFER, GL_MUBS, NULL, GL_STATIC_DRAW); // allocate 2 mat4 (128 bytes)
				glBindBuffer(GL_UNIFORM_BUFFER, 0);
				glBindBufferRange(GL_UNIFORM_BUFFER, 0, W->instancesblock, 0, GL_MUBS);
			
				glGenBuffers(1, &W->SAAimagesblock);
				glBindBuffer(GL_UNIFORM_BUFFER, W->SAAimagesblock);
				glBufferData(GL_UNIFORM_BUFFER, GL_MUBS, NULL, GL_STATIC_DRAW);
				glBindBuffer(GL_UNIFORM_BUFFER, 0);
				glBindBufferRange(GL_UNIFORM_BUFFER, 1, W->SAAimagesblock, 0, GL_MUBS);
			//----------------------------------------------------------------------------------
			static const GLfloat VBO_data[] = {		//quad
				 1.0f,  1.0f, 0.0f,
				 1.0f, -1.0f, 0.0f,
				-1.0f, -1.0f, 0.0f,
				-1.0f,  1.0f, 0.0f,
			};
			static const GLuint EBO_data[] = {		//quad
				0, 1, 3,   // first triangle
				1, 2, 3    // second triangle
			};
			glGenBuffers(1, &Texts_VBO);
			glGenBuffers(1, &Texts_EBO);
				glBindBuffer(GL_ARRAY_BUFFER, Texts_VBO);
				glBufferData(GL_ARRAY_BUFFER, sizeof(VBO_data), VBO_data, GL_STATIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER, 0);	
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Texts_EBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(EBO_data), EBO_data, GL_STATIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glGenTextures(1, &W->FAA);
			glActiveTexture(GL_TEXTURE0+2);	//+2
			glBindTexture(GL_TEXTURE_2D_ARRAY, W->FAA);
				glTexImage3D   (GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, 512, 512, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);	//max size <-> GL_MUBS/Glyph = 32
				glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
			glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
			Fonts_SP = LoadShaders("Element/View/text/text.glvs", "Element/View/text/text.glfs");
			glUseProgram(Fonts_SP);
				glUniformMatrix4fv(glGetUniformLocation(Fonts_SP, "viewprojection"), 1, GL_FALSE, glm::value_ptr(W->viewprojection));
				glUniform1i(glGetUniformLocation(Fonts_SP, "font_textures"), 2);
				glUniformBlockBinding(Fonts_SP, glGetUniformBlockIndex(Fonts_SP, "block2"), 2);
			//	
				glGenBuffers(1, &W->FAAglyphsblock);
				glBindBuffer(GL_UNIFORM_BUFFER, W->FAAglyphsblock);
				glBufferData(GL_UNIFORM_BUFFER, GL_MUBS, NULL, GL_STATIC_DRAW);
				glBindBuffer(GL_UNIFORM_BUFFER, 0);
				glBindBufferRange(GL_UNIFORM_BUFFER, 2, W->FAAglyphsblock, 0, GL_MUBS);
			glUseProgram(0);
			//----------------------------------------------------------------------------------
				//gen texture - static array
				//memory map -> subbuffer			(how to get dimensions?)
				//gen uniform - image meta array	(shader)
				//upload uvs -> buffer
			
				glEnable(GL_DEPTH_TEST);
				glDepthFunc(GL_LEQUAL);
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				
				
			
				
			magic_load (Utils::V::gIR_H = magic_open(MAGIC_SYMLINK),NULL);
		};
		/*
		void System::Window::Init() {
			auto W = this;
			auto C = WM_Connection;
			
			auto title = (W->D->Manifest).c_str();
			xcb_request_check(C, xcb_change_property_checked(C,XCB_PROP_MODE_REPLACE,W->id,
				XCB_ATOM_WM_NAME,XCB_ATOM_STRING,8, strlen(title), title
			));
			
			const static uint32_t pos[] = { 200, 200 };
			xcb_request_check(C, xcb_configure_window_checked(C,W->id, 
				XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, pos
			));
			const static uint32_t size[] = { 400, 100 };
			xcb_request_check(C, xcb_configure_window_checked(C,W->id, 
				XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, size
			));
			
			//title, attributes
		};
		*/
		void System::Window::Exit() {
			auto W = this;
			auto C = WM_Connection;
			auto X = WM_Display;
			
			W->exists = false;
			glXDestroyWindow(X, W->gl);
			xcb_request_check(C, xcb_destroy_window_checked(C, W->id));
			glXDestroyContext(X, (GLXContext)W->ctx);
			Windows.erase(W->id);
			magic_close(Utils::V::gIR_H);	
			
			for(Mesh * mesh: W->Meshes)
				delete mesh;
			
			for(Image *image: W->Images_Static) {
				munmap(image->data, image->data_sz);
				close (image->data_fd);
				delete image;
			}
			
			for(Text * text: W->Texts)
				delete text;
			
			for(Font *font: W->Fonts) {
				munmap(font->data, font->data_sz);
				close (font->data_fd);
				delete font;
			}
		};
		
		namespace Async {
			bool Dispatchable(uint32_t id) {
				if(Windows.find(id) == Windows.end()) return false;
				if(!((System::Window*)Windows[id])->exists) return false;
				return true;
			};
			void Dispatch_Load(uint32_t id, Window::Image *image) {
				if(!Dispatchable(id)) return;
				auto W = Windows[id];
				std::lock_guard<std::mutex> lock(*W->D->C.mutex);
				if(!Dispatchable(id)) return;
				
					constructor::Event Ev;
					
						Ev.Element.i["bubble"] = 0;
						Ev.Element.i["cancel"] = 0;
						Ev.Element.n["source"] = 
						Ev.Element.n["target"] = W->D->M.Root;
					
						Ev.Element.s["type"] = "load";
						Ev.Element.s["file"] = image->file;
				
				  W->DrawFlag = true;
				( W->D->C.Queue).push_back( Ev );
				(*W->D->C.ready).notify_one();
			}
		}
		bool Image_Load(Window::Image *image) {	//	cout<< "%%%%%%% Image_Load: I am called." <<endl;
			
			int fd = open((".Environment/.cache/htde/"+image->file+"/.pix").c_str(), O_RDONLY, S_IRUSR | S_IWUSR);
			
			struct  stat sb; 
			if(fstat(fd,&sb) == -1) { printf("%%%%%%% Image_Load: couldn't get filesize");
				close(fd); return false;
			}
				image->data_fd = fd;	//
				image->data_sz = sb.st_size;
				image->data = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
			if (image->data == MAP_FAILED) { printf("%%%%%%% Image_Load: couldn't mmap file");
				close(fd); return false;
			}
			
			image->STATE_LOADED = true;	 	// cout<< "%%%%%%% Image_Load: loaded cache for "<<image->file<<endl;
			return true;
		};
		std::mutex 
			 Image_Load__mutex;
		void Image_Load__thread(Window::Image *image, Window *W) {
			std::lock_guard<std::mutex> guard(Image_Load__mutex);
			
			string&file = image->file;
			string path = ".Environment/.cache/htde/"+file;
			
			if(std::fs::create_directories(path)) {	//	cout<< "%%%%%%% Image_Load__thread: creating cache for "<<image->file<<endl;
				unsigned char* data; int data_sz,w,h,ch;
				std::ofstream fo;
				
				if (data 	= stbi_load(file.c_str(),&w,&h,&ch,STBI_rgb_alpha))
				{ 	data_sz =	 					 (w *h 	  *STBI_rgb_alpha);
			
					fo.open((path+"/.pix").c_str(), std::ios::binary );
					fo.write((char*)data, data_sz);
					fo.close();
				
						cout <<"img_data success"<<endl; stbi_image_free(data);
				}else { cout <<"img_data failed" <<endl; return; }
			}
			if(!Image_Load(image)) {
				return;
			}
			std::thread(Async::Dispatch_Load, W->id, image).detach();
		};
		int System::Window::Image_Init(string url) {
			
			for(Image *img: Images_Static) {
				if (img->url==url) {
					img->ref++;
					return img->id;
				}
			}
			auto image = new Image();	//
				 image->url  = url;
				 image->ref	 = 1;
			
			if(!std::fs::exists(image->file=Hosts.Request(url)->Data)) {
			if(!std::fs::exists(image->file="Host/share/image/"+url)) {
			if(!std::fs::exists(image->file=url)) {
				delete image; return -1;
			}}}
			if(!Utils::V::getImageResolution(image->file, image->size[0],image->size[1],image->size[2])) {
				delete image; return -1;
			}
			if(!Image_Load(image)) {
				std::thread(Image_Load__thread, image, this).detach();
			}
			
			STATE_DOPACK = true;
			image->id = Images_Static.size();
			Images_Static.push_back(image);
			return image->id;
		};
		
		int Image_Sort(const void* _a, const void* _b) {
			System::Window::Image
				*a = *(System::Window::Image**)_a
			,	*b = *(System::Window::Image**)_b;
			if (a->size[2] < b->size[2])	return  1;	else 
			if (a->size[2] > b->size[2])	return -1;	else
											return  0;
		};
		int System::Window::Image_Pack() {
			
		//=============================================================================
			auto size = Images_Static.size();
			auto list = new Image*[size]; 
			
			std::copy(Images_Static.begin(), Images_Static.end(), list);
			qsort(list, size, sizeof(Image*), &Image_Sort);
		//-----------------------------------------------------------------------------
			int  res 	= 2048;			//size of texture slot in pixels
			int	 unit 	= 128; 			//256 units per 2048x2048 image
			int	 units 	= 0;	 		//unit count/location
			auto bits 	= bitset<8>(0);	//unit count/location in binary		//8 bits = 256 units @ 128x128 pixels/unit
			int  pix[] 	= {0,0,0};		//unit count/location in pixels		//slot = [x,y,layer]
				
			for(int i=0; i<size; i++) {	//meta objects size descending	//size = [w,h,square size]
				Image* image = list[i];
				image->STATE_PACKED=true;
				image->STATE_PUSHED=0;
										//	cout << "%%%%%%% image->size[2]: "<< image->size[2] <<endl;
				image->slot[0] = pix[0];//	cout << "%%%%%%% image->slot[0]: "<< image->slot[0] <<endl;
				image->slot[1] = pix[1];//	cout << "%%%%%%% image->slot[1]: "<< image->slot[1] <<endl;
				image->slot[2] = pix[2];//	cout << "%%%%%%% image->slot[1]: "<< image->slot[2] <<endl;
				
				units += (image->size[2]/unit) * (image->size[2]/unit);
				bits = units;
					
				pix[0] = bits[7]*8*unit + bits[5]*4*unit + bits[3]*2*unit + bits[1]*1*unit;
				pix[1] = bits[6]*8*unit + bits[4]*4*unit + bits[2]*2*unit + bits[0]*1*unit;
					
				if((pix[0]==0) && (pix[1]==0)) {
					pix[2]++;
				}
			};
		//-----------------------------------------------------------------------------
			delete[] list;
		//=============================================================================
		//	STATE_DOPACK=false;
		//	STATE_DOPUSH=true;
			
			if (pix[2]+1 > Images_Static_Slots) {
				Images_Static_Slots = pix[2]+1;
				
					STATE_DOSIZE=true;	//cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<STATE_DOSIZE=true<< "<< Images_Static_Slots <<endl;
			}else {	STATE_DOSIZE=false;	//cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>STATE_DOSIZE=false<< "<< Images_Static_Slots <<endl;
			}
		};
		
		std::mutex 
			 Font_Load__mutex;
		bool Font_Load(Window::Font *font) {
			std::lock_guard<std::mutex> guard(Font_Load__mutex);
			
			string&file = font->file;
			string path = ".Environment/.cache/htde/"+file;
			
			if(std::fs::create_directories(path)) {	//	cout<< "%%%%%%% Font_Load__cache: creating cache for "<<file<<endl;
				//untar to "~/.cache/htde/fontname.sdf"
				//rename "*.png" -> ".png", "*.fnt" -> ".fnt"
				system(("tar -xf ./"+file+" -C "+path
				+	" && cd "+path
				+	" && mv *.png .png"
				+	" && mv *.fnt .fnt"
				).c_str());
				
				//load .png -> .pix
				//load .fnt -> .lay1, .lay2
				Window::Font F;
				std::ofstream fo;
				std::ifstream fi;
				
				unsigned char *data; int w,h,ch;
				if (data = stbi_load((path+"/.png").c_str(),&w,&h,&ch,STBI_rgb_alpha))
				{ 	
					fo.open((path+"/.pix").c_str(), std::ios::binary );
					fo.write((char*)data, sizeof(char)*512*512*4);
					fo.close(); cout<<".pix success"<<endl;
					
						cout <<"img_data success"<<endl; stbi_image_free(data);
				}else { cout <<"img_data failure"<<endl; return false; }
			//
				fi.open(path+"/.fnt"); string line;
				if (fi.is_open())
				{	RE2 RE_META1(R"(info\s+face=\"([^"]+)\"\s+size=([0-9-]+)\s+bold=(0|1)\s+italic=(0|1))");
					string f; int s,b,i;
					RE2 RE_META2(R"(common\s+lineHeight=([0-9-]+)\s+base=([0-9-]+))");
					int lineheight,basewidth;
					RE2 RE_GLYPH(R"(char\s+id=([0-9-]+)\s+x=([0-9-]+)\s+y=([0-9-]+)\s+width=([0-9-]+)\s+height=([0-9-]+)\s+xoffset=([0-9-]+)\s+yoffset=([0-9-]+)\s+xadvance=([0-9-]+))");
					int c,x,y,w,h,xO,yO,xA,yA, l=0;
					while (getline(fi, line)){ l++;
						if(l==1){ if(RE2::PartialMatch(line, RE_META1, &f,&s,&b,&i)) {
							F.Meta.bold =(b==1); F.Meta.size = s;
							F.Meta.ital =(i==1); strcpy(F.Meta.face, (f.substr(0,31)).c_str());
						}} else
						if(l==2){ if(RE2::PartialMatch(line, RE_META2, &lineheight,&basewidth)) {
							F.Meta.line = lineheight;
							F.Meta.base = basewidth;
						}} else { if(RE2::PartialMatch(line, RE_GLYPH, &c,&x,&y,&w,&h,&xO,&yO,&xA)) {
							F.Glyphs[c] = {{x ,y },{w ,h}};
							F.glyphs[c] = {{xO,yO},{xA,0}};
						}}
					}
					for(int c=1; c<32; c++) {
						F.Glyphs[c] = F.Glyphs[0];
						F.glyphs[c] = F.glyphs[0];
					}
					fo.open((path+"/.lay0").c_str(), std::ios::binary );
					fo.write((char*)&F.Meta, sizeof(F.Meta));
					fo.close(); cout<<".lay0 success"<<endl;
					fo.open((path+"/.lay1").c_str(), std::ios::binary );
					fo.write((char*)&F.glyphs[0], sizeof(F.glyphs));
					fo.close(); cout<<".lay1 success"<<endl;
					fo.open((path+"/.lay2").c_str(), std::ios::binary );
					fo.write((char*)&F.Glyphs[0], sizeof(F.Glyphs));
					fo.close(); cout<<".lay2 success"<<endl;

						cout <<"fnt_data success"<<endl; fi.close();
				}else { cout <<"fnt_data failure"<<endl; return false; }
				
				//delete .png, .fnt
				system(("cd "+path
				+	" && rm .png"
				+	" && rm .fnt"
				).c_str());
			}
			
			int fd1 = open((path+"/.pix").c_str(), O_RDONLY, S_IRUSR | S_IWUSR);	//open from cache
			
			struct  stat sb; 
			if(fstat(fd1,&sb) == -1) { printf("%%%%%%%????????????????????????????? Font_Load: couldn't get filesize");
				close(fd1); return false;
			}
				font->data_fd = fd1;
				font->data_sz = sb.st_size;
				font->data = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd1, 0);
			if (font->data == MAP_FAILED) { printf("%%%%%%%????????????????????????????????? Font_Load: couldn't mmap file");
				close(fd1); return false;
			}
			
			std::ifstream fi;
			fi.open((path+"/.lay0").c_str(), std::ios::binary);	//open from cache
			fi.read((char*)&font->Meta, sizeof(font->Meta));
			fi.close(); cout <<".lay0 meta loaded"<<endl;
			fi.open((path+"/.lay1").c_str(), std::ios::binary);	//open from cache
			fi.read((char*)&font->glyphs, sizeof(font->glyphs));
			fi.close(); cout <<".lay1 glyphs loaded"<<endl;
			fi.open((path+"/.lay2").c_str(), std::ios::binary);	//open from cache
			fi.read((char*)&font->Glyphs, sizeof(font->Glyphs));
			fi.close(); cout <<".lay2 Glyphs loaded"<<endl;
			
			return true;
		};
		int System::Window::Font_Init(string url) {
		
			for(Font *fnt: Fonts) {
				if (fnt->url == url) {
					return fnt->id;
				}
			}
			auto font = new Font();	//
				 font->url = url;
			
			if(!std::fs::exists(font->file=Hosts.Request(url)->Data)) {
			if(!std::fs::exists(font->file="Host/share/font/"+url)) {
			if(!std::fs::exists(font->file=url)) {
				delete font; return -1;
			}}}
			if(!Font_Load(font)) {
				delete font; return -1;
			}
			
			font->STATE_DOINIT = true;
			font->id = Fonts.size();
			Fonts.push_back(font);
			return font->id;
		
		};
		int System::Window::Text_Init(node_p target) {
		//	//check if new text		->	new text, set id, state flag || return id
			if(target->attribs.i.end() != target->attribs.i.find("txtid")){ 
				int &B_id = target->attribs.i["txtid"];
				auto B= Texts[B_id];
					 B->STATE_DOPACK = true;
				return B_id;
			}
			auto B = new Text();
			B->STATE_DOINIT = true;
			B->STATE_DOPACK = true;
			B->id = Texts.size();
			Texts.push_back(B);
			return B->id;	//N->attribs.i["txtid"] =
		};
		
		struct Text__cursor_t {
			
			float w;	//body w
			float h;	//body h
			float x;	//body x
			float y;	//body y
			float z;	//body z
			
		//	float W;	//char W	//line_rat
		//	float H;	//line H	//line_new
			float X;	//cursor X
			float Y;	//cursor Y
			
			int last;	//last element
		/*
			0:	NONE, 
			1:	DEFAULT, 
			2:	RECTANGLE, 
			4:	TEXTBODY, 
			256:TEXTNODE
			8:	TITLE, 		//header,footer,break
			16:	ITEM, 		//bullet,number,expand,radio,check
			32:	STRING, 	//link
			64:	BASIC, 		//font family, size, color fg, bg
			128:BLOCK		//code, note, quote
			?:	MEDIA		//media
			?:	NODE		//element, class, id
		*/
		}	thread_local 
			 Layout_text__cursor;
		void Layout_text__recurse(node_p target, Window::Text *B) {
			if("textnode" == target->M.Type) {	//[textapi] use V.Flags
				
				auto W = Thread.D->V.Window;
				auto&_ = Layout_text__cursor;
				
				auto &type = target->M.parent->V.Flags[0];	//[textapi] target = class view type
				auto &value= target->attribs.s["value"];
				
				Window::Font *F;	//[!]inherit loop up until value
				for(auto temp=target;;temp=temp->M.parent) {		//[!]
					 if (temp->V["font"].Assignment.index()==1) {	//[!]
						F=W->Fonts[std::get<1>(temp->V["font"].Assignment)];
						break;
					}
				}
				glm::vec4 rgba;
				for(auto temp=target;;temp=temp->M.parent) {
					 if (temp->V["fontcolor"].Assignment.index()==4) {
						rgba=std::get<4>(temp->V["fontcolor"].Assignment);
						break;
					 }
				}
				float size;
				for(auto temp=target;;temp=temp->M.parent) {
					 if (temp->V["fontsize"].Assignment.index()==2) {
						size=std::get<2>(temp->V["fontsize"].Assignment);
						break;
					 }
				}
				
				switch(type) {
					case   8:	break;
					case  16:	break;	//bullet - begin: depth + symbol, end: newline
					case  32:	break;	//link -
					case  64:	break;	//font -
					case 128:	break;	//block - set font to monospace, color highlight, create bg node
					default :	break;	//default
				}
				//[!] font - need to modularize all of this
				//cannot hardcode: Data method, Character Struct | load from layout profile, assemble from shader
				
				
				float line_old = F->Meta.line;	//70.0f;	//font-size in atlas
				float line_new = size;			//10.0f;	//font-size in body
				float rat = line_new / line_old;
				float pad = 0.0f;
				
				auto &space = F->glyphs[32].advance[0];
				auto &depth = target->M.parent->attribs.i["depth"];
				bool mono = (type==128);
				
				float L = _.w/-2.0f;
				float R = _.w/ 2.0f;
				
				if( (type != 16)
				&&(_.last != 16)); else
					_.X = R;	//new line (wrap)
				
				//-------------------------------------------
				Window::Font::Glyph G; Window::Font::glyph g;
				float x, y, w, h; 
				
				int i=0;
				for(const char *c = value.c_str(); *c; c++) {
					if((mono) && (*c == 10)) {	//if block/code
						_.X = R; //new line (wrap)
						continue;
					}
					G = F->Glyphs[*c];
					g = F->glyphs[*c];
					w = G.size[0]*rat; x = g.offset[0]*rat;
					h = G.size[1]*rat; y = g.offset[1]*rat;
					if (_.X >= R - g.advance[0]*rat/2.0f) {	//width - pad*2
						_.X  = L + g.advance[0]*rat/2.0f + (space*rat*0.75)*(4*depth);
						if (_.last<0)
							_.last=0;
						else
							_.Y -= line_new;			//line-height
						if (_.Y <= _.h/-2.0f)
							return;
						i = 0;
					}
					B->Characters.push_back({
						glm::scale(glm::translate(glm::mat4(1.0f)	
						,	glm::vec3
							(	_.x + _.X + x
							,	_.y + _.Y - h/2.0f - y
							,	_.z
							)
						),	glm::vec3
							(	w/2.0f
							,	h/2.0f
							,	1.0f
							)
						)					//model
					,	rgba				//color
					,	(F->id*128) + (int)*c	//glyph	//offset
					});
					if((mono) && (*c == 9)) {	//if block/code
						int tab = 4-(i%4); i += tab;
						_.X +=(space*rat*0.75f)*tab;
					}else {
						i++;
						_.X += g.advance[0]*rat*0.75f;
					}
				}
				//-------------------------------------------
				_.last = type;
			}
			
			for(auto child:target->M.children)
				Layout_text__recurse(child, B);
		};
		void Layout_text(node_p target) {	//[?] - layout profile entry point
			auto W = Thread.D->V.Window;
			auto B = W->Texts[target->attribs.i["txtid"]];
			auto&_ = Layout_text__cursor;
			
		//	if(!B->STATE_DOPACK)	//[?] - set during assignment
		//		return;
			
			B->Characters.clear();
			
			_.x = std::get<2>(target->M.parent->V["x"].Calculation);
			_.y = std::get<2>(target->M.parent->V["y"].Calculation);
			_.z = std::get<2>(target->M.parent->V["z"].Calculation);
			_.w = std::get<2>(target->M.parent->V["width"].Assignment);
			_.h = std::get<2>(target->M.parent->V["height"].Assignment);
			_.X = _.w/2.0f;		//start at right (auto-wrap)
			_.Y = _.h/2.0f;		//start at top
			_.last = -1;
			
			for(auto child:target->M.children)
				Layout_text__recurse(child, B);
		};
		void Layout_temp(node_p target) {
			//matrix offset
			if(!target->M.parent) {
				target->V["color"].Assignment = glm::vec4(0.0f,0.0f,0.0f,0.0f);
				target->V["image"].Assignment = (int)-1;
				target->V["x"].Calculation	= (double)0.0;
				target->V["y"].Calculation	= (double)0.0;
				target->V["z"].Calculation	= (double)0.0;
				target->V["matrix"].Calculation	= glm::mat4(1.0f);
			}else {
				target->V["matrix"].Calculation =
				
				glm::scale
				(	glm::translate
					(	glm::mat4(1.0f)
					,	glm::vec3	//translation
						(	(float)std::get<2>(target->V["x"].Calculation = std::get<2>(target->M.parent->V["x"].Calculation) + std::get<2>(target->V["x"].Assignment))
						,	(float)std::get<2>(target->V["y"].Calculation = std::get<2>(target->M.parent->V["y"].Calculation) + std::get<2>(target->V["y"].Assignment))
						,	(float)std::get<2>(target->V["z"].Calculation = std::get<2>(target->M.parent->V["z"].Calculation) + std::get<2>(target->V["z"].Assignment))
						)
					)
				,	glm::vec3		//scale
					(	(float)(std::get<2>(target->V["width"].Assignment)/2)
					,	(float)(std::get<2>(target->V["height"].Assignment)/2)
					,	(float)(std::get<2>(target->V["length"].Assignment)/2)
					)
				);
			}
			for(auto child: target->M.children) {
				if ("textbody"==child->M.Type)	//[textapi] use V.Flags
					Layout_text(child);
				else
					Layout_temp(child);	
			}
		};
		
	//	thread_local std::chrono::steady_clock::time_point frames_record[620];
	//	thread_local int frames_index=0;
		
		void System::Window::Draw() {
			auto W = this;
			auto C = WM_Connection;
			auto X = WM_Display;
			//----------------------
//			cout << "draw requested,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,," <<endl;

		//	auto frametime = std::chrono::steady_clock::now();
		//	auto framediff = std::chrono::duration_cast<std::chrono::milliseconds>(frametime - framelast).count();
		//		 framelast = frametime;	cout << "[frame] ms:"<<framediff<<endl;
		/*	
			auto frametime = std::chrono::steady_clock::now();
			auto framediff = std::chrono::duration_cast<std::chrono::milliseconds>(frametime - framelast).count();
			if ( framediff>= 15 ) {		//timeout has 0-1ms delay
				 framelast = frametime;	//cout << "[frame] ms:"<<framediff<<endl;
			} else {
				return;
			}
		*/
		/*
			frames_record[frames_index++] = std::chrono::steady_clock::now();//
			if(frames_index==620){frames_index=0; for(int i=1;i<620;i++) {
				cout
				<<	"[frame] ms:"
				<<	std::chrono::duration_cast<std::chrono::milliseconds>(frames_record[i] - frames_record[i-1]).count()<<endl;
			}}	
		*/
		/*	
			if (framediff > 20) {
				framelast = frametime;
				frameskip = 1;	cout<<"[frame] pass "<<framediff<<endl;
			} else {
				frameskip = 0;	cout<<"[frame] skip "<<framediff<<endl;
				DrawFlag = true;
				return;
			}
		*/
			//----------------------------------------------------------------------------------	PACK ASSET
			
				if (STATE_DOPACK) {		//only if we have new image metas
					Image_Pack();
				}
				
			//==================================================================================	CALC META
				
				for(auto B: Texts) {
					B->Characters.clear();	//[textapi]	
				}
				Layout_temp(W->D->M.Root);
				
			//----------------------------------------------------------------------------------	PREP META
				for(Mesh *mesh: Meshes) {	//[!] - test for mesh type
					mesh->UBO_instancecount = 0;
					mesh->UBO_bytecount = 0;
				//	mesh->Data(W->D->M.Root);
				
					queue<node_p> nodes; 
					nodes.push(W->D->M.Root);
					
					while(!nodes.empty()) {		
							  auto target = nodes.front();
						mesh->Data(target); nodes.pop();
						for(auto child: target->M.children) {
						//if text body -> skip
							if ("textbody"==child->M.Type)	//[textapi] use V.Flags
								continue;
							
							nodes.push(child);
						}
					}
				}
			//==================================================================================	LOCK CONTEXT
//			cout << "draw queued~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" <<endl;
			std::lock_guard<std::mutex> guard(WM_Mutex);
			//----------------------------------------------------------------------------------	PUSH META
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				
				if (STATE_DOPACK) {
					STATE_DOPACK=false;
					int	SAA_offset = -32;	//32
					
					glBindBuffer(GL_UNIFORM_BUFFER, W->SAAimagesblock);
					for(auto image: Images_Static) {
						
						glm::mat2x3 SAA_bytes
						(	image->slot[0]/2048.0f,	image->slot[1]/2048.0f, image->slot[2]
						,	image->size[0]/2048.0f,	image->size[1]/2048.0f, 0.0f
						);
						glBufferSubData(GL_UNIFORM_BUFFER, SAA_offset+=32, 24, (const GLfloat*)&SAA_bytes);	//32, 24
					}					
					glBindBuffer(GL_UNIFORM_BUFFER, 0);
				}
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	PUSH ASSET
//			auto start = std::chrono::steady_clock::now();
			
				if (STATE_DOSIZE) {		//only if repacked to new size
					STATE_DOSIZE=false;
				
					glDeleteTextures(1, &W->SAA);
					glGenTextures					(1, &W->SAA);
					glBindTexture  (GL_TEXTURE_2D_ARRAY, W->SAA);
					glTexImage3D   (GL_TEXTURE_2D_ARRAY, 0, GL_RGBA, 2048, 2048, Images_Static_Slots, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);			
					glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
					glTexParameteri(GL_TEXTURE_2D_ARRAY,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);
					glBindTexture  (GL_TEXTURE_2D_ARRAY, 0);
				}
				
				for(auto image: Images_Static) {
					if (image->STATE_LOADED) {				//cout << "%%%%%%%%%%%%%% image->STATE_LOADED = TRUE" <<endl;
						if (image->STATE_PUSHED!= 2) {
							image->STATE_PUSHED = 2;
							
							glActiveTexture(GL_TEXTURE0);					//
							glBindTexture  (GL_TEXTURE_2D_ARRAY, W->SAA);	//
							
							glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 
								image->slot[0], image->slot[1], image->slot[2], 
								image->size[0], image->size[1], 1, 
								GL_RGBA, GL_UNSIGNED_BYTE, image->data
							);
						}
					}else {
						if (image->STATE_PUSHED!= 1) {
							image->STATE_PUSHED = 1;
							//push default
						}
					}
				}
			
//			auto stop = std::chrono::steady_clock::now();
//			cout << std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count() << " ns !@#$%&#^@%$#$!@#%$@!$#$^$%!$@#$%$&^^%$!@#^$#$@"<<endl;
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~	DRAW SCENE
				for(Mesh *mesh: Meshes) {
					mesh->Draw();
				}
				
				for(auto font: Fonts) {
					if (font->STATE_DOINIT) {
						font->STATE_DOINIT=false;
						//push uniform
						glBindBuffer(GL_UNIFORM_BUFFER, W->FAAglyphsblock);
						glBufferSubData(GL_UNIFORM_BUFFER, font->id * 128 * sizeof(Font::Glyph), 128 * sizeof(Font::Glyph), &font->Glyphs[0]);		//offset
						glBindBuffer(GL_UNIFORM_BUFFER, 0);
						//push texture
						glActiveTexture(GL_TEXTURE0+2);
						glBindTexture  (GL_TEXTURE_2D_ARRAY, W->FAA);
						glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 
							0, 0, font->id, //offset
							512, 512, 1, 	//
							GL_RGBA, GL_UNSIGNED_BYTE, font->data
						);	
					}
				}
				
				glUseProgram(Fonts_SP);
				for(Text *text: Texts) {
				
					if (text->STATE_DOINIT) {
						text->STATE_DOINIT=false;
						
						glGenVertexArrays(1, &text->VAO);
						glBindVertexArray(text->VAO);
						
							glBindBuffer(GL_ARRAY_BUFFER, Texts_VBO);
							glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Texts_EBO);
							glEnableVertexAttribArray(0);	//layout (location = 0) in vec3 position;
							glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);
						
							glGenBuffers(1, &text->VBO);
							glBindBuffer(GL_ARRAY_BUFFER, text->VBO); //layout (location = 1) in mat4 model; //layout (location = 5) in vec4 color;
							glEnableVertexAttribArray(1); glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(0 * sizeof(glm::vec4))); glVertexAttribDivisor(1, 1);
							glEnableVertexAttribArray(2); glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(1 * sizeof(glm::vec4))); glVertexAttribDivisor(2, 1);
							glEnableVertexAttribArray(3); glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(2 * sizeof(glm::vec4))); glVertexAttribDivisor(3, 1);
							glEnableVertexAttribArray(4); glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(3 * sizeof(glm::vec4))); glVertexAttribDivisor(4, 1);
							glEnableVertexAttribArray(5); glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(4 * sizeof(glm::vec4))); glVertexAttribDivisor(5, 1);
							glEnableVertexAttribArray(6); glVertexAttribPointer(6, 1, GL_FLOAT, GL_FALSE, sizeof(Text::Character), (void*)(5 * sizeof(glm::vec4))); glVertexAttribDivisor(6, 1);
							
						glBindVertexArray(0);
					}
					
				//	if (text->STATE_DOPACK) {		//[!]font - set during assignment
				//		text->STATE_DOPACK=false;
						
						glBindBuffer(GL_ARRAY_BUFFER, text->VBO);
						glBufferData(GL_ARRAY_BUFFER, text->Characters.size() * sizeof(Text::Character), &text->Characters[0], GL_STATIC_DRAW);	//
						glBindBuffer(GL_ARRAY_BUFFER, 0);
				//	}
					
					glBindVertexArray(text->VAO);
					glDrawElementsInstanced(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, text->Characters.size());	//
					glBindVertexArray(0);
				}
				
			glXSwapBuffers(X, W->gl);
			//----------------------------------------------------------------------------------
//			cout << "draw frame.............................................." <<endl;
			
			//init mesh
				//make VAO
				//make VBOs
				//make SP
				//make UBO
					//color				//vec4
					//location			//mat4
			
			//------------------
			
				//in position			//vec3	//layout=0	//vertex pos		//per vertex
				//uniform instances		//array: mesh prop structs				//per instance
			
				//uniform view			//vec4									//per draw
				//uniform projection	//vec4									//per draw
			
			
			//------------------
			
			//lock active context
			
			//mesh management
			//texture management
			//property buffering	//layout?
			
			//instanced rendering
				//bind VAO
				//bind VBO [?]
				//bind SP
				//bind UBO
			
			
		};
		
		namespace Sync {
			string Read(string file) {
				string result;
				char buffer[1024];	//[?] - static
				uv_buf_t iov = uv_buf_init(buffer, sizeof(buffer));	//[?] - static
				uv_fs_t open_req;
				uv_fs_t read_req;
				uv_fs_t close_req;
				
				uv_fs_open(uv_default_loop(), &open_req, file.c_str(), O_RDONLY, 0, NULL);
				if (open_req.result < 0) {
					fprintf(stderr, "error opening file: %s\n", uv_strerror((int)open_req.result));
				} else { 
					for(int i=0; i<1024; i++) {
						uv_fs_read(uv_default_loop(), &read_req, open_req.result, &iov, 1, -1, NULL);
						if (read_req.result < 0) {
							fprintf(stderr, "Read error: %s\n", uv_strerror(read_req.result));
							break;
						} else if (read_req.result == 0) {
							uv_fs_close(uv_default_loop(), &close_req, open_req.result, NULL);
							uv_fs_req_cleanup(&close_req);
							break;
						} else if (read_req.result > 0) {
							iov.len = read_req.result;
							result += string(iov.base, iov.len);
						}
					}
					uv_fs_req_cleanup(&read_req);
				}
				uv_fs_req_cleanup(&open_req);
				return result;
			};
			string Mime(string file) {
				magic_t h = magic_open(MAGIC_MIME_ENCODING); magic_load(h,NULL);
				auto result = string(magic_file(h, file.c_str())); magic_close(h);
				return result;
			};
			
			void print_modifiers (uint32_t mask) {
			  const char **mod, *mods[] = {
				"Shift", "Lock", "Ctrl", "Alt",
				"Mod2", "Mod3", "Mod4", "Mod5",
				"Button1", "Button2", "Button3", "Button4", "Button5"
			  };
			  cout << "Modifier mask: ";
			  for (mod = mods ; mask; mask >>= 1, mod++)
				if (mask & 1)
				  cout << *mod;
			  cout << endl;
			};
			bool Dispatchable(uint32_t id) {
				if(Windows.find(id) == Windows.end()) return false;
				if(!((System::Window*)Windows[id])->exists) return false;
				return true;
			};
			void Dispatch_MouseDn( xcb_button_press_event_t *ev ) {
				if(!Dispatchable(ev->event)) return;
				auto W = Windows[ev->event];
				std::lock_guard<std::mutex> lock(*W->D->C.mutex);
				if(!Dispatchable(ev->event)) return;
				
					constructor::Event Ev;
					
						Ev.Element.i["bubble"] = 1;
						Ev.Element.i["cancel"] = 0;
						Ev.Element.n["source"] = 
						Ev.Element.n["target"] = W->D->C.State.n["source"];
					
						Ev.Element.s["type"] = "mousedn";
						Ev.Element.i["x"] = ev->event_x - W->width/2;
						Ev.Element.i["y"] = W->height/2 - ev->event_y;
						
				( W->D->C.Queue).push_back( Ev );
				(*W->D->C.ready).notify_one();
			};
			void Dispatch_MouseUp( xcb_button_release_event_t *ev ) {
				if(!Dispatchable(ev->event)) return;
				auto W = Windows[ev->event];
				std::lock_guard<std::mutex> lock(*W->D->C.mutex);
				if(!Dispatchable(ev->event)) return;
				
					constructor::Event Ev;
					
						Ev.Element.i["bubble"] = 1;
						Ev.Element.i["cancel"] = 0;
						Ev.Element.n["source"] = 
						Ev.Element.n["target"] = W->D->C.State.n["source"];
					
						Ev.Element.s["type"] = "mouseup";
						Ev.Element.i["x"] = ev->event_x - W->width/2;
						Ev.Element.i["y"] = W->height/2 - ev->event_y;
				
				( W->D->C.Queue).push_back( Ev );
				(*W->D->C.ready).notify_one();
			};
			void Dispatch_MouseMove( xcb_motion_notify_event_t *ev ) {
				if(!Dispatchable(ev->event)) return;
				auto W = Windows[ev->event];
				std::lock_guard<std::mutex> lock(*W->D->C.mutex);
				if(!Dispatchable(ev->event)) return;
				
					auto newsrc = Utils::V::getElementByMouse(W, ev->event_x,ev->event_y);
					auto oldsrc = W->D->C.State.n["source"];	//[history]
					auto newent = Utils::V::getEntityByMouse(W, newsrc);	//will update newsrc
					auto oldent = W->D->C.State.i["entity"];
					
					constructor::Event Ev;
					
						Ev.Element.i["bubble"] = 1;
						Ev.Element.i["cancel"] = 0;
						Ev.Element.n["source"] = 
						Ev.Element.n["target"] = newsrc;	//[ray]
					
						Ev.Element.s["type"] = "mousemove";
						Ev.Element.i["x"] = W->Cursor.x = ev->event_x - W->width/2;	//[cursor]
						Ev.Element.i["y"] = W->Cursor.y = W->height/2 - ev->event_y;//[cursor]
						
					if(~newent) {
						Ev.Element.i["entity"] = newent;
					}
					if((oldsrc != NULL)
					&& (oldsrc != newsrc)) {
						constructor::Event E3 = Ev;
						E3.Element.s["type"] = "mouseintro";
						E3.Element.i["bubble"] = 0;
						constructor::Event E2 = E3;
						E2.Element.s["type"] = "mouseoutro";
						E2.Element.n["target"] =
						E2.Element.n["source"] = oldsrc;
						( W->D->C.Queue).push_back( E2 );
						( W->D->C.Queue).push_back( E3 );
					}
					
				( W->D->C.Queue).push_back( Ev );
				(*W->D->C.ready).notify_one();
			};
			void Dispatch_KeyDown( xcb_key_press_event_t *ev ) {
				if(!Dispatchable(ev->event)) return;
				auto W = Windows[ev->event];
				std::lock_guard<std::mutex> lock(*W->D->C.mutex);
				if(!Dispatchable(ev->event)) return;
				
					auto src = W->D->C.State.n["source"]? W->D->C.State.n["source"]: W->D->M.Root;
					auto sym = XkbKeycodeToKeysym(WM_Display, ev->detail, 0, ev->state & ShiftMask ?1:0);
				
					constructor::Event Ev;
					
						Ev.Element.i["bubble"] = 1;
						Ev.Element.i["cancel"] = 0;
						Ev.Element.n["source"] = 
						Ev.Element.n["target"] = src;

						Ev.Element.s["type"] = "keydn";
						Ev.Element.i["code"] = sym;
						Ev.Element.s["char"] =(sym > 127)?"":string(1,(char)sym);
						Ev.Element.i["mask"] = ev->state;
						
				( W->D->C.Queue).push_back( Ev );
				(*W->D->C.ready).notify_one();
			};

			void Input__Thread_gl() {
				auto C = WM_Connection;// = xcb_connect(NULL, NULL);
				xcb_generic_event_t *e;
				
			  while ((e = xcb_wait_for_event (C))) {
				switch (e->response_type & ~0x80) {
				case XCB_EXPOSE: {
				  xcb_expose_event_t *ev = (xcb_expose_event_t *)e;

//				  printf ("Window %ld exposed. Region to be redrawn at location (%d,%d), with dimension (%d,%d)\n",
//						  ev->window, ev->x, ev->y, ev->width, ev->height);
				  break;
				}
				case XCB_BUTTON_PRESS: {
				  xcb_button_press_event_t *ev = (xcb_button_press_event_t *)e;
//				  print_modifiers(ev->state);

				  switch (ev->detail) {
				  case 4:
//					printf ("Wheel Button up in window %ld, at coordinates (%d,%d)\n",
//							ev->event, ev->event_x, ev->event_y);
					break;
				  case 5:
//					printf ("Wheel Button down in window %ld, at coordinates (%d,%d)\n",
//							ev->event, ev->event_x, ev->event_y);
					break;
				  case 1:
						
						Dispatch_MouseDn(ev);
						
//						printf ("Button %d pressed in window %ld, at coordinates (%d,%d)\n",
//								ev->detail, ev->event, ev->event_x, ev->event_y);
					break;
				  default:
					printf ("Button %d pressed in window %ld, at coordinates (%d,%d)\n",
							ev->detail, ev->event, ev->event_x, ev->event_y);
				  }
				  break;
				}
				case XCB_BUTTON_RELEASE: {
				  xcb_button_release_event_t *ev = (xcb_button_release_event_t *)e;
//				  print_modifiers(ev->state);

				  switch (ev->detail) {
				  case 1:
						
						Dispatch_MouseUp(ev);
						
//						printf ("Button %d released in window %ld, at coordinates (%d,%d)\n",
//								ev->detail, ev->event, ev->event_x, ev->event_y);
					break;
				  default:
					printf ("Button %d released in window %ld, at coordinates (%d,%d)\n",
						  ev->detail, ev->event, ev->event_x, ev->event_y);
				  }
				  break;
				}
				case XCB_MOTION_NOTIFY: {
				  xcb_motion_notify_event_t *ev = (xcb_motion_notify_event_t *)e;
					
					Dispatch_MouseMove(ev);
					
//				  printf ("Mouse moved in window %ld, at coordinates (%d,%d)\n",
//						  ev->event, ev->event_x, ev->event_y);
				  break;
				}
				case XCB_ENTER_NOTIFY: {
				  xcb_enter_notify_event_t *ev = (xcb_enter_notify_event_t *)e;

//				  printf ("Mouse entered window %ld, at coordinates (%d,%d)\n",
//						  ev->event, ev->event_x, ev->event_y);
				  break;
				}
				case XCB_LEAVE_NOTIFY: {
				  xcb_leave_notify_event_t *ev = (xcb_leave_notify_event_t *)e;

//				  printf ("Mouse left window %ld, at coordinates (%d,%d)\n",
//						  ev->event, ev->event_x, ev->event_y);
				  break;
				}
				case XCB_KEY_PRESS: {
				  xcb_key_press_event_t *ev = (xcb_key_press_event_t *)e;
//				  print_modifiers(ev->state);
				  
					Dispatch_KeyDown(ev);
					
//					printf ("Key %d pressed in window %ld\n", 
//							ev->detail, ev->event);
				  break;
				}
				case XCB_KEY_RELEASE: {
				  xcb_key_release_event_t *ev = (xcb_key_release_event_t *)e;
//				  print_modifiers(ev->state);

//				  printf ("Key released in window %ld\n",
//						  ev->event);
				  break;
				}
				default:
				  // Unknown event type, ignore it 
//				  printf("Unknown event: %d\n", e->response_type);
				  break;
				}
				// Free the Generic Event
				free (e);
			  }
			};
		/*	
			void Document__draw() {	//[fps]
			
				while(true) {	//[!]
					std::this_thread::sleep_for(std::chrono::milliseconds(7));
					
					for(auto&each: Windows) {
						auto id=each.first;
						if(!Dispatchable(id)) continue;	
						auto W = Windows[id]; std::lock_guard<std::mutex> lock(*W->D->C.mutex);
						if(!Dispatchable(id)) continue;
						
							constructor::Event Ev;	Ev.Frame = true;
								
						( W->D->C.Queue).push_back( Ev );
						(*W->D->C.ready).notify_one();
					}
				}
			};
		*/	
			void Input( bool toggle ) {
				if(!toggle) {
					xcb_disconnect(WM_Connection);
					return;
				}					if (!XInitThreads())			  {fprintf(stderr, "Can't multithread X\n"); return;}
				auto X = WM_Display    = XOpenDisplay(0);		if(!X){fprintf(stderr, "Can't open display\n");	return; }
				auto C = WM_Connection = XGetXCBConnection(X);	if(!C){fprintf(stderr, "Can't get xcb connection from display\n"); XCloseDisplay(X); return; }
				XSetEventQueueOwner (X , XCBOwnsEventQueue);
				std::thread(Input__Thread_gl).detach();	//[gl]
		//		std::thread(Document__draw).detach(); //[fps]
			};
		}
	}
}
