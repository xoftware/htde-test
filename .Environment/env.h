namespace htde {
	void Host::Request( Transfer Tx ) {
		Tx->Data = "";
		if (Tx->URL[0] == '/') {
			if((Tx->Mime = System::Sync::Mime(Tx->URL)) != "binary") {
				Tx->Data = System::Sync::Read(Tx->URL);
			}else {
				Tx->Data = Tx->URL;
			}
			Hosts.Receive(Tx);
			return;
		}
		if (Tx->URL.find("://") == string::npos) {
			Tx->URL= Thread.D->Host +"/"+ Tx->URL;
		}
		
		Protocols["http"].Request(Tx);
	};
	void Host::Receive( Transfer Tx ) {
		
	//	if( Tx->callback != NULL )
	//		Tx->callback(Tx);				//add to Queue
		
		if( Tx->frontend != NULL) {
			if(!Tx->Async) {					//do not Queue (Sync)
				Tx->D->C.Engine->Receive(Tx);
			}else {								//add to Queue (Async)
				constructor::Event Ev; Ev.Request = Tx;
				std::lock_guard<std::mutex> lock(*Tx->D->C.mutex);
				( Tx->D->C.Queue).push_back( Ev );
				(*Tx->D->C.ready).notify_one();
			}
		}
	};
	Host::Transfer Host::Request( string URL ) {
		auto Tx=make_shared<Host::Transfer_Object>();
		Tx->URL=URL; Hosts.Request(Tx);
		return Tx;
	};	
	Host 								Hosts;		//\ put in Host.so

	thread_local THREAD_LOCAL_T			Thread;
	vector<constructor::Document*> 		Documents;
	namespace constructor {
		unordered_map<string, node>		Elements;
		unordered_map<string, Member>	Members;		//member, Members	
		
		unordered_map<string, View>			Views;
		unordered_map<string, Controller>	Controllers;	//[!] node_proto
		
		node_p Element(string type) {
			if(Elements.find( type) == Elements.end())
				type = "element";
			
			return make_shared<node>(Elements[type]);
		};
	}
	namespace constructor {
		namespace M {
			unordered_map<string, parser>	Parsers;
		}
		namespace V {
			unordered_map<string, vparser>	Parsers;
		}
		namespace C {
			unordered_map<string, 
			 unordered_map<string, 
			  script_engine*(*)()>>			Engines;
			unordered_map<string, 
			 unordered_map<string, 
			  script_engine*(*)()>>			Xpilers;
			 
			 script_engine* Engine(string type) {
				
				//split type by '/'
				//use default if necessary
				
				return Engines["JS"]["SM"]();
			 };
		}
	}
}
/*
	TODO:
	- test.* 	=>	htde.*
	- _build.* 	=>	.Environment/_build.*
	- rayobb.h 	+>	.Environment/lib.h		(Utils::V)
	- ~/.cache	=>	/home/x3/.cache
	- ref/[lib] =>	.Environment/lib/[lib]
	- ref		=>	.Environment/ref
	- *.c		=>	*.cpp17
	


*/
