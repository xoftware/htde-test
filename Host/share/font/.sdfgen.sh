L="\033[1;30m";R="\033[0m"

echo -e \
"$L""\n""1. Generate .png & .fnt with Hiero"\
"$L""\n""  a. Glyph Cache: $R"" 512x512"\
"$L""\n""  b. Size:        $R""*so big to fit on 1 page*"\
"$L""\n""  c. Render:      $R"" Java"\
"$L""\n""  d. Effect:      $R"" Signed Distance Field"\
"$L""\n""  e. Spread:      $R""~10"\
"$L""\n""  f. Scale:       $R""~15"\
"$L""\n""  g. Padding:     $R""~8"\
"$L""\n""  h. Offset:      $R"" 0"\
"$L""\n""2. Generate .sdf with this script"\
"$L""\n""3. Move it to appropriate directory"\
"$L""\n"

for i in ./*.fnt; do
	if [ -f "$i" ]; then

		NAME="$(basename $i)"
		NAME=${NAME%.fnt}
		
		tar cf $NAME.sdf $NAME.fnt $NAME.png
				  rm -rf $NAME.fnt $NAME.png
	fi
done
