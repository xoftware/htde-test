DIR=$1	#default

curgen_color() {
	ASSET=$DIR/$2/$1	#dir/color/cur
	
	echo `cat $DIR/$1.cfg`" $ASSET.png 1000" > $ASSET.cfg	#cfg
	xcursorgen $ASSET.cfg $ASSET;		rm -rf $ASSET.cfg	#cur
}

for i in "$DIR"/*.cfg; do
	if [ -f "$i" ]; then

		NAME="$(basename $i)"
		NAME=${NAME%.cfg}
			
		curgen_color $NAME black
		curgen_color $NAME red
		curgen_color $NAME orange
		curgen_color $NAME yellow
		curgen_color $NAME green
		curgen_color $NAME blue
		curgen_color $NAME purple
		curgen_color $NAME pink			
	fi
done
