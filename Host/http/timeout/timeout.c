#include "../../../htde.h"
#include "../http.h"
using namespace htde;

extern "C" {
	
	Host::Transfer Bridge( Host::Transfer Tx ){
		auto* http = (local::http *)(Tx->local.get());
		
	//	cout << http->URL <<endl;
		
		return Tx;
	};
	
	void Timeout( Host::Transfer Tx ) {
		
		auto* http = (local::http *)(Tx->local.get());

		int ms = std::stoi(http->url_route);	//cout << "!!!!!!!!!!!!!!!!!!!! trying timeout: "<<ms<<" !!!!!!!!!!!!" <<endl;
		Tx->Data = http->url_route;

		std::this_thread::sleep_for(std::chrono::milliseconds(ms));
		Hosts["http"].Receive(Tx);
	};
	
	void Router( Host::Transfer Tx ) {
		
		Tx->Async = true;		
		
		if(!Tx->Async) {
			Timeout(Tx);
		} else {
			std::thread(Timeout,Tx).detach();
		}
		
	//	cout << "[!] ------->" << "Router : timeout.so" <<endl;
	};
}
