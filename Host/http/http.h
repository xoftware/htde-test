#include "re2/re2.h"
namespace htde::local {
		
	struct http {
		
		string URL; 		 //protocol://domainname:portalias/route?params=values
		RE2 RE_URL = RE2(R"(([^:]+:\/\/)([^:]+:)?([^\/\\?]+)(\/[^\\?]*)?(\\?[^\n]*))");
		
		//Hosts["http"]["ui.urthq.gg"]["Multiplayer"](Tx);
		//Hosts[Protocol][Port][Route](Tx);
		
		string url_protocol;
		string url_domainname;
		string url_portalias;
		string url_domain;
		string url_route;
		string url_params;
		
		strmap HEAD;
		strmap POST;
		strmap GET;
		
		string url_file;
		
	};		
}