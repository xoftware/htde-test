#include "../../../htde.h"
#include "../http.h"
using namespace htde;

extern "C" {

	Host::Transfer Bridge( Host::Transfer Tx ){
		cout << "[!] -------> " << "Bridge : ui.urthq.gg.so" <<endl;
		auto* http = (local::http *)(Tx->local.get());
		
		cout << http->URL <<endl;
		
		for(std::pair<string,string> url_param : http->GET)
			cout << url_param.first << " : " << url_param.second << "; ";
		cout << endl;
		
		cout << "[!] <------- " << "Bridge : ui.urthq.gg.so" <<endl;
		return Tx;
	};

	void Router( Host::Transfer Tx ) {
		
		auto* http = (local::http *)(Tx->local.get());
		Tx->Async = false;
		
		bool bridge = false;
		http->url_file = "Host/http/"+ http->url_portalias +"/"+ http->url_route;
		
		if(!Tx->Async) {
			if(bridge) {
				//Call Bridge
				//Callback Receive
			} else {
				if((Tx->Mime = System::Sync::Mime(http->url_file)) != "binary") {
					Tx->Data = System::Sync::Read(http->url_file);
				}else {
					Tx->Data = http->url_file;
				}
				Hosts["http"].Receive(Tx);
			}
		} else {
			if(bridge) {
				//Call Bridge -> libuv / work
				//Callback Receive <-
			} else {
				//Call Default -> libuv / fs
				//Callback Receive <-
			}
		}
		
		cout << "[!] ------->" << "Router : ui.urthq.gg.so" <<endl;
	};
}
