#include "../../../../htde.h"
#include "../../http.h"
using namespace htde;
extern "C" {

	Host::Transfer Bridge( Host::Transfer Tx ){
		cout << "[!] -------> " << "Bridge : Multiplayer.so" <<endl;
		auto* http = (local::http *)(Tx->local.get());
		
		for(std::pair<string,string> url_param : http->GET)
			cout << url_param.first << " : " << url_param.second << "; ";
		cout << endl;
		
		cout << "[!] <------- " << "Bridge : Multiplayer.so" <<endl;
		return Tx;
	};
	
}
