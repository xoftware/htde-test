#include "../../htde.h"
#include "http.h"
using namespace htde;

extern "C" {
	
	void Receive( Host::Transfer Tx ) {
		//Process Tx
		//Hosts.Receive(Tx);
		
//		cout << Tx->Data <<endl;
		
		auto* http = (local::http *)(Tx->local.get());
//		cout << "[!] -------> " << http->URL <<endl;
		
		Hosts.Receive(Tx);
	};
	
	void Router( Host::Transfer Tx ) {
		
		auto* http = (local::http *)(Tx->local.get());
		http->url_file = "Host/http/"+ http->url_portalias +"/"+ http->url_route;
		
		if(!Tx->Async) {
			if((Tx->Mime = System::Sync::Mime(http->url_file)) != "binary") {
				Tx->Data = System::Sync::Read(http->url_file);
			}else {
				Tx->Data = http->url_file;
			}
			Receive(Tx);
		} else {
			//Call Default -> libuv / fs
			//Callback Receive <-
		}
		
	};
	
	void Request( Host::Transfer Tx ) {
		
		auto* http = (local::http *)
					 (Tx->local = make_shared<local::http>()).get();
					 
		if(RE2::FullMatch(Tx->URL, http->RE_URL
		,  &http->url_protocol
		,  &http->url_domainname
		,  &http->url_portalias
		,  &http->url_route
		,  &http->url_params)
		) {	
			http->URL			= Tx->URL;
			http->url_protocol	= Utils::undelimit(http->url_protocol, "://");
			http->url_domainname= Utils::undelimit(http->url_domainname, ":");
			http->url_domain	= http->url_domainname +":"+ http->url_portalias;
			http->url_route		= Utils::undelimit(http->url_route, "/");
			http->GET			= Utils::parse_url_params(http->url_params);         //route?GET
			http->HEAD;
			http->POST;
			
            if(("localhost" == http->url_domainname)		//Localhost:
			&& (Hosts["http"].Ports.find(http->url_domain)
			!=	Hosts["http"].Ports.end())) {
			
				Tx->Async = false;		//
				
				if(Hosts["http"][http->url_domain].Routes.find(http->url_route)
				!= Hosts["http"][http->url_domain].Routes.end()) {
					if(!Tx->Async) {
						Hosts["http"][http->url_domain][http->url_route](Tx);
						Receive(Tx);
					} else {
						//Call Bridge -> libuv / work
						//Callback Receive <-
					}
				} else {
					if (Hosts["http"][http->url_domain].Router) {
						Hosts["http"][http->url_domain].Router(Tx);
					} else {
						Router(Tx);
					}
				}
				
            } else {							//Network:
				if(!Tx->Async) {
					//Fetch -- curl
					//Callback Receive
				} else {
					//Fetch <- libuv / curl
					//Callback -> Receive
				}
            }
			
		}
	};
}

/*
	//http.h [?]
	//DLL_PUBLIC __atribute__
	extern int HTTP_HEAD	= 0;
	extern int HTTP_GET		= 1;
	extern int HTTP_POST	= 2;
	

	//htde.h
	Tx->local.s = vector<string>;
	
	
	//http.c
	Tx->local.s[HTTP_GET]	= "";
	Tx->local.s[HTTP_POST]	= "";
	Tx->local.s[HTTP_HEAD]	= "";

	




*/
